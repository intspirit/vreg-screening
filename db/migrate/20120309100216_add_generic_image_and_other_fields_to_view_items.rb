class AddGenericImageAndOtherFieldsToViewItems < ActiveRecord::Migration
  def change
    # for single table inheritance
    add_column :view_items, :type, :string
    # generic image attachment
    add_column :view_items, :image_uid, :string
    # generic thumb attachment
    add_column :view_items, :thumbnail_uid, :string
    # generic thumb hover attachment
    add_column :view_items, :thumbnail_hover_uid, :string
    # generic title string
    add_column :view_items, :title, :string
    # generic file 
    add_column :view_items, :file_uid, :string
    
    # VIMEO ID
    add_column :view_items, :vimeo_id, :string
    
    # YOUTUBE ID
    add_column :view_items, :youtube_id, :string
  end
end
