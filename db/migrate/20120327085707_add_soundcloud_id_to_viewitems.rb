class AddSoundcloudIdToViewitems < ActiveRecord::Migration
  def change
    add_column :view_items, :soundcloud_id, :string
  end
end
