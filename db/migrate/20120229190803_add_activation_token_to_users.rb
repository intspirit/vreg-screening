class AddActivationTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :activation_token, :string
    add_column :users, :after_activation_path, :string
    add_column :users, :activated, :boolean, :default=>false
  end
end
