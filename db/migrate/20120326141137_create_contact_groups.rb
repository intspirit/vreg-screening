class CreateContactGroups < ActiveRecord::Migration
  def change
    create_table :contact_groups do |t|
      t.string :name
      t.references :user
      t.string :description
      t.timestamps
    end
    
    create_table :contact_groups_contacts, :id => false, :force => true do |t|
      t.references :contact_group 
      t.references :contact
      # t.timestamps
    end
  end
end
