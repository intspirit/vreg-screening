class AddShowLinkToViewItems < ActiveRecord::Migration
  def change
    add_column :view_items, :show_link, :boolean
  end
end
