class FixTypoInUserActions < ActiveRecord::Migration
  def up
    rename_column :user_actions, :stared_at, :started_at
  end

  def down
  end
end
