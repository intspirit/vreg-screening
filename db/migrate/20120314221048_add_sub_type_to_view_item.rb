class AddSubTypeToViewItem < ActiveRecord::Migration
  def change
    add_column :view_items, :sub_type, :string
  end
end
