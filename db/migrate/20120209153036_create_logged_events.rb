class CreateLoggedEvents < ActiveRecord::Migration
  def change
    create_table :logged_events do |t|
      t.integer :user_id
      t.integer :project_id
      t.string :event_type
      t.datetime :created_at
    end
    
    add_index :logged_events, :project_id
    
  end
end
