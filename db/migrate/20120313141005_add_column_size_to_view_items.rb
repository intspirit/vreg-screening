class AddColumnSizeToViewItems < ActiveRecord::Migration
  def change
    add_column :view_items, :column_size, :integer
  end
end
