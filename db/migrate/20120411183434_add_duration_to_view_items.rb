class AddDurationToViewItems < ActiveRecord::Migration
  def change
    add_column :view_items, :duration, :integer
  end
end
