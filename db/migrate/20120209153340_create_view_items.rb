class CreateViewItems < ActiveRecord::Migration
  def change
    create_table :view_items do |t|
      t.string :ancestry
      t.integer :position, :default => 0
      t.string :item_type
      t.text :html, :default => ''
    end
    add_index :view_items, :ancestry
  end
end
