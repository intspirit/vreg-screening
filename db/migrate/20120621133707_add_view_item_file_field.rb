class AddViewItemFileField < ActiveRecord::Migration
  def change
    add_column :view_items, :video_file, :string
  end
end
