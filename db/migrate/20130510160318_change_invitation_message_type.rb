class ChangeInvitationMessageType < ActiveRecord::Migration
  def up
    change_column :invitations, :message, :text, :limit => nil
  end

  def down
    change_column :invitations, :message, :string, :limit => 255
  end
end
