class AddDraftToProject < ActiveRecord::Migration
  def change
    add_column :projects, :draft_id, :integer
  end
end
