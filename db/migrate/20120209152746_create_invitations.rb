class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string :message
      t.string :token
      t.datetime :viewed_at
      t.integer :creator_id
      t.integer :project_id
      t.integer :user_id

      t.timestamps
    end
  end
end
