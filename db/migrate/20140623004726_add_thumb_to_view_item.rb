class AddThumbToViewItem < ActiveRecord::Migration
  def change
    add_column :view_items, :thumb_url, :string
  end
end
