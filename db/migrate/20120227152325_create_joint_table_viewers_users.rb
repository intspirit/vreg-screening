class CreateJointTableViewersUsers < ActiveRecord::Migration
  def up
    create_table :projects_viewers, :id => false do |t|
      t.integer :viewer_id
      t.integer :project_id
    end
    add_index :projects_viewers, [:project_id, :viewer_id]
    add_index :projects_viewers, [:viewer_id, :project_id]
  end

  def down
    drop_table :projects_viewers
  end
end
