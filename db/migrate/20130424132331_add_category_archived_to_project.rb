class AddCategoryArchivedToProject < ActiveRecord::Migration
  def change
    add_column :projects, :category, :string
    add_column :projects, :archived, :boolean
  end
end
