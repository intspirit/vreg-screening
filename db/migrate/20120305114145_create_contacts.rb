class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :email
      t.string :full_name
      t.integer :user_id
      t.integer :connected_id

      t.timestamps
    end
  end
end
