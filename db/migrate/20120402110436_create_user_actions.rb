class CreateUserActions < ActiveRecord::Migration
  def change
    create_table :user_actions do |t|
      t.integer :user_id
      t.integer :project_id
      t.string :action_type
      t.integer :view_item_id
      t.text :description
      t.datetime :stared_at
      t.datetime :ended_at

      t.timestamps
    end
  end
end
