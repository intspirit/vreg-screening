class CreateUsersContacts < ActiveRecord::Migration
  def up
    create_table :users_contacts, :id => false do |t|
      t.integer :user_id
      t.integer :contact_id
    end
    add_index :users_contacts, [:contact_id, :user_id]
    add_index :users_contacts, [:user_id, :contact_id]
  end

  def down
    
  end
end
