# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# creating the main admin user
u = User.where(super_user: true)
u.create!([
	{
		email: 'example@exmaple.com',
		name: 'James',
		password: '123456',
		password_confirmation: '123456'
	}
])

# creating normal user 
