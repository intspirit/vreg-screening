require "test_helper"

class UserTest < ActiveSupport::TestCase
  include UsersFactory
  
  test 'Add contact -> create user' do
    
    mails_before = ActionMailer::Base.deliveries.count
    
    user = valid_user
    contact = user.add_contact({email:'contact@email.net'})
    assert User.all.count == 2, 'Contact was NOT created'
    
    assert mails_before == ActionMailer::Base.deliveries.count, 'When creating a contact an email was sent'
  end

  test 'Do not duplicate users when adding contacts' do
    user = valid_user
    user.add_contact({email:'contact@email.net'})
    user.add_contact({email:'contact@email.net'})
    assert User.all.count == 2, 'Contact was duplicated'
    assert user.contacts.count > 0, 'Contact was not aded to user.contacts'
    assert user.contacts.count == 1, 'User has duplicated contacts.'
  end
  
  test 'When user with email exists just add to contacts' do
    user1 = valid_user
    user2 = valid_user
    user3 = user_with_activation
    user1.add_contact({email:user2.email})
    assert user1.contacts.count == 1, 'Confirmed user was not aded to user.contacts'
    user1.add_contact({email:user3.email})
    assert user1.contacts.count == 2, 'Uconfirmed user was not aded to user.contacts'
  end
  
end


