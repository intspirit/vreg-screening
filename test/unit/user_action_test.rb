require 'test_helper'

class UserActionTest < ActiveSupport::TestCase
  include ViewItemsFactory
  include ProjectsFactory
  test "download event with view_item sets description" do
    project = valid_project
    attachment  = create_attachment({parent_id:project.view_item.id})
    user_action = UserAction.new({user:project.creator,view_item:attachment,action_type:'download'})
    assert user_action.save, "User action wasn't saved."
    assert user_action.description, "Descripton wasn't set."
  end
end
