require "test_helper"

class ProjectTest < ActiveSupport::TestCase
  include UsersFactory
  include ProjectsFactory

  test 'On create initializes the default template' do
    project = valid_project
    assert not(project.view_item.nil?)
    
    assert not(SCREENING_TEMPLATES.nil?), 'SCREENING_TEMPLATES is not defined'
    
    assert SCREENING_TEMPLATES['templates'].first['name']=='Default', 'Put a default template into SCREENING_TEMPLATES'
    
    template = SCREENING_TEMPLATES['templates'].first['children']
    template.each_with_index do |view_item_template, index|
      assert project.view_item.subtree.where({:item_type=>view_item_template['item_type'],:position=>index}).order(:position).first, "#{view_item_template['item_type']} wasnt created"
    end
    
  end
  
  test 'Add and delete viewers' do
    project = valid_project
    creator = valid_project.creator
    add_contacts_to(creator)
    contact = creator.contacts.first
    
    project.viewers << contact
    
    assert_equal 1, project.viewers.count, 'Contact was not added to project viewers.'
    #assert_equal 1, contact.viewable_projects.count, 'Contact is not present in viewable_projects.'
    
    project.viewers << creator.contacts.first
    
    assert not(project.viewers.count > 1), 'Contact was not added to project viewers more than once.'
    #assert_equal 1, user.viewable_projects.count, 'Contant sees project as viewable only once'
    
    project.viewers.delete creator.contacts.first
    
    assert_equal 0, project.viewers.count, 'Contact was not removed as viewer.'
    #assert_equal 0, user.viewable_projects.count, 'Contact sees project as viewer even if it was removed.'
  end
  
end