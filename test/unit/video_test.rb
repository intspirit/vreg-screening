require 'test_helper'

class VideoTest < ActiveSupport::TestCase
  valid_vimeo_id = '32658495'
  unknown_vimeo_id = '30000005'
  another_valid_vimeo_id = '6312563'
  
  test 'Assert vimeo thumbs get pulled.' do
    video = Video.new({item_type:'video',sub_type:'vimeo'})
    video.vimeo_id = unknown_vimeo_id
    assert !video.save, 'Video saved with unknown vimeo_id'
    
    video.vimeo_id = valid_vimeo_id
    assert video.save, 'Video wasnt saved, even if it has a valid vimeo_id'

    assert video.duration, 'Duration wasnt set.'
    assert video.image_uid, 'Main image wasnt set.'
    assert video.thumbnail_uid, 'Thumbnail image wasnt set.'
    assert video.thumbnail_hover_uid, 'Thumbnail hover image wasnt set.'
    
  end
  
  test 'Assert vimeo relatade attrubutes get updated if video gets updated.' do
    
    video = Video.new({item_type:'video',sub_type:'vimeo'})
    video.vimeo_id = valid_vimeo_id
    video.save
    
    old_image_uid           = video.image_uid
    old_thumbnail_uid       = video.thumbnail_uid
    old_thumbnail_hover_uid = video.thumbnail_hover_uid
    
    video.vimeo_id = another_valid_vimeo_id
    video.save
    
    assert old_image_uid           != video.image_uid, 'Main image wasnt updated.'
    assert old_thumbnail_uid       != video.thumbnail_uid, 'Thumbnail image wasnt updated.'
    assert old_thumbnail_hover_uid != video.thumbnail_hover_uid, 'Thumbnail hover image wasnt updated.'
  end
  
  valid_youtube_id = 'DeHtWqlt3OQ'
  unknown_youtube_id = 'MHQSfA2JQw6'
  another_valid_youtube_id = 'MHQSfA2JQwU'
  
  test 'Assert youtube thumbs get pulled.' do
    video = Video.new({item_type:'video',sub_type:'youtube'})
    video.youtube_id = unknown_youtube_id
    assert !video.save, 'YoutubeVideo saved with unknown video_id'
    
    video.youtube_id = valid_youtube_id
    
    assert video.save, 'YoutubeVideo wasnt saved, even if it has a valid video_id'
    
    assert video.title
    assert video.duration, 'Duration wasnt set.'
    assert video.image_uid, 'Main image wasnt set.'
    assert video.thumbnail_uid, 'Thumbnail image wasnt set.'
    assert video.thumbnail_hover_uid, 'Thumbnail hover image wasnt set.'
    
  end
  
  test 'Assert youtube relatade attrubutes get updated if video gets updated.' do
    
    video = Video.new({item_type:'video',sub_type:'youtube'})
    video.youtube_id = valid_youtube_id
    video.save
    
    old_image_uid           = video.image_uid
    old_thumbnail_uid       = video.thumbnail_uid
    old_thumbnail_hover_uid = video.thumbnail_hover_uid
    
    video.youtube_id = another_valid_youtube_id
    video.save
    
    assert old_image_uid           != video.image_uid, 'Main image wasnt updated.'
    assert old_thumbnail_uid       != video.thumbnail_uid, 'Thumbnail image wasnt updated.'
    assert old_thumbnail_hover_uid != video.thumbnail_hover_uid, 'Thumbnail hover image wasnt updated.'
  end
end
