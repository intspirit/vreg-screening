require "test_helper"

class InvitationTest < ActiveSupport::TestCase
  include InvitationsFactory
  
  test 'send email on creation' do
    mails_sent_before = ActionMailer::Base.deliveries.count
    @invitation = invitation_for_new_user
    assert (mails_sent_before + 1) == ActionMailer::Base.deliveries.count, "Mail was not delivered"
  end
  
  test 'set activation url when user is not activated' do
    @invitation = invitation_for_new_user
    assert_equal @invitation.viewer.after_activation_path, "/invitations/#{@invitation.id}"
  end
  
end