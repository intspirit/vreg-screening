require 'test_helper'

class AudioTest < ActiveSupport::TestCase

  valid_soundcloud_url = 'http://soundcloud.com/forss/flickermood'
  unknown_soundcloud_url = 'http://soundcloud.com/forss/flickermoodddddd'

  test 'Assert sound cloud validates.' do
    audio = Audio.new({item_type:'audio',sub_type:'soundcloud',soundcloud_id:unknown_soundcloud_url})
    assert !audio.save, 'Audio saved with unknown soundcloud_id'
    
    audio.soundcloud_id = valid_soundcloud_url
    assert audio.save, 'Audio wasnt saved, even if it has a valid audio_id'
    
    assert audio.title, 'Audio title wasnt set.'
    
  end
  
end
