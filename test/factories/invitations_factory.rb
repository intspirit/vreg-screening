require 'test_helper'

module InvitationsFactory
  include UsersFactory
  include ProjectsFactory
  
  def invitation_for_new_user email = nil
    project = valid_project
    viewer = user_with_activation({email:email})
    creator = valid_project.creator
    creator.add_contact({email:viewer.email})
    project.viewers << creator.contacts.last
    Invitation.create({project:project,:viewer => viewer,creator:creator})
  end

  def invitation_for_existing_user
    project = valid_project
    viewer = valid_user
    creator = valid_project.creator
    creator.add_contact({email:viewer.email})
    project.viewers << creator.contacts.last
    Invitation.create({project:project,:viewer => viewer,creator:creator})
  end
  
end