require "test_helper"

module UsersFactory 
  
  def init_attributes(attrs={})
    pass = (0...8).map{ ('a'..'z').to_a[rand(26)] }.join
    email = "#{pass}@test.net"
    attrs[:email] = email unless attrs[:email]
    attrs[:password] = pass unless attrs[:password]
    attrs[:password_confirmation] = pass unless attrs[:password_confirmation]
    attrs
  end
  
  def valid_user(attrs={})
    User.create init_attributes(attrs)
  end
  
  def user_with_activation(attrs={})
    u = User.new_with_activation init_attributes(attrs)
    u.save
    u
  end
  
  def add_contacts_to(u,i=1)
    i.times do 
      u.add_contact({email:valid_user.email})
    end
    u.contacts
  end
  
end 