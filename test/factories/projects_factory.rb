require 'test_helper'

module ProjectsFactory
  include UsersFactory
  
  def valid_project
    title = (0...8).map{ ('a'..'z').to_a[rand(26)] }.join
    Project.create({creator:valid_user,title:title})
  end
  
end