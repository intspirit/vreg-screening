require 'test_helper'

module ViewItemsFactory
  
  def create_attachment(attrs={})
    attrs[:item_type] = 'attachment'
    attrs[:file] = File.new(Rails.root.join('test/fixtures/file.txt'))
    Attachment.create(attrs)
  end
  
end