require 'test_helper'

class UserActionsControllerTest < ActionController::TestCase
  include ProjectsFactory
  include Devise::TestHelpers
  
  test "should get index" do
    project = valid_project
    @user = project.creator
    sign_in @user
        
    get :index, :format => :json, :project_id=>project.id
    assert_response :success
  end
end
