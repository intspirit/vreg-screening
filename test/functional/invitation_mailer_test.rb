require 'test_helper'

class InvitationMailerTest < ActionMailer::TestCase
  include InvitationsFactory
  
  test "invite new user" do
    mails_sent_before = ActionMailer::Base.deliveries.count
    @invitation = invitation_for_new_user 'andrejjancic@gmail.com'
    assert !@invitation.viewer.activated?, "Viewer is confirmed. Problem in factory."
    
    # delivery should happen by mail creation
    # email = InvitationMailer.invitation_mail(@invitation).deliver
    
    assert (mails_sent_before + 1) == ActionMailer::Base.deliveries.count, "Mail was not delivered"
    
    email =  ActionMailer::Base.deliveries.last
    # Test the body of the sent email contains what we expect it to
    assert_equal [@invitation.viewer.email], email.to
    assert_equal [@invitation.creator.email], email.reply_to
    assert email.subject.include? @invitation.creator.name
    assert email.subject.include? @invitation.project.title
    
    assert email.encoded.include?(@invitation.viewer.activation_token), 'Activation token is missing.'
    
    assert email.encoded.include? @invitation.creator.name
    assert email.encoded.include? @invitation.project.title
    
  end
  
  test "invite existing user" do
    mails_sent_before = ActionMailer::Base.deliveries.count
    @invitation = invitation_for_existing_user
    
    assert @invitation.viewer.activated?, "Viewer is NOT confirmed. Problem in factory."
    
    # delivery shpuld happen by invitation creation
    #email = InvitationMailer.invitation_mail(@invitation).deliver
    
    assert (mails_sent_before + 1) == ActionMailer::Base.deliveries.count, "Mail was not delivered"
    
    email =  ActionMailer::Base.deliveries.last
    
    assert (email.encoded.include? "/invitations/#{@invitation.id}"), 'Invitation path is missing.'
    
    assert email.encoded.include? @invitation.creator.name
    assert email.encoded.include? @invitation.project.title
  end
  
end
