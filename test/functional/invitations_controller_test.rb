require 'test_helper'

class InvitationsControllerTest < ActionController::TestCase
  include UsersFactory
  include ProjectsFactory
  include Devise::TestHelpers
  
  def setup
    @project = valid_project
    @user = @project.creator
    sign_in @user
    
    @not_activated_viewer =  user_with_activation
    @activated_viewer = valid_user
    
    @user.add_contact({:email=>@not_activated_viewer.email})
    @project.viewers << @user.contacts.last
    
    @user.add_contact({:email => @activated_viewer.email})
    @project.viewers << @user.contacts.last
    
  end
  
#  test "index" do
#    get :index, :format => :json, :project_id=>@project.id, :viewer_id=>@unconfirmed_viewer.id
#    assert_response :success
#  end

  test "create" do
    
    post :create, :format => :json, :project_id=>@project.id, :viewer_id =>@user.contacts.last.id
    assert_response :success
  
  end
#  
#  test "create two times" do
#    
#    post :create, :format => :json, :viewer => {:contact_id=>@contact_1.id}, :project_id=>@project.id
#    post :create, :format => :json, :viewer => {:contact_id=>@contact_1.id}, :project_id=>@project.id
#    assert_response :unprocessable_entity
#    assert @project.viewers.count == 2, "Same contact added twice"
#  end
#
#  test "destroy contact" do
#        
#    get :destroy, :format => :json, :id=>@contact_0.id, :project_id=>@project.id
#    assert_response :success
#    
#    @project.viewers.reload
#    assert @project.viewers.count == 0, 'Viewer was not removed.'
#  end
end
