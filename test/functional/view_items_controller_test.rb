require 'test_helper'

class ViewItemsControllerTest < ActionController::TestCase
  include UsersFactory
  include ProjectsFactory
  include Devise::TestHelpers
  
  def setup
    @project = valid_project
    @user = @project.creator
    sign_in @user
    @parent_view_item_id = @project.view_item.subtree.where("item_type='drop-panel'").first.id
  end

  test "create" do
    count = ViewItem.count
    post :create, :format => :json, :view_item => {:item_type=>'box',:sub_type=>'box-whole'}, :view_item_id=>@parent_view_item_id
    assert_response :success
    assert ViewItem.count == count + 1
  end
  
  test "destroy view item" do
    old_count = ViewItem.count
    delta = ViewItem.find(@parent_view_item_id).subtree.count
    post :destroy, :format => :json,:id=>@parent_view_item_id, :view_item_id=>@project.view_item.id
    assert_response :success
    assert ViewItem.count == (old_count - delta)
  end
  
end
