require 'test_helper'

class ViewersControllerTest < ActionController::TestCase
  include UsersFactory
  include ProjectsFactory
  include Devise::TestHelpers
  
  def setup
    @project = valid_project
    @user = @project.creator
    sign_in @user
    add_contacts_to(@user,2)
    @contact_0   = @user.contacts[0]
    @contact_1   = @user.contacts[1]
    @project.viewers << @contact_0
  end
  
  test "index" do
        
    get :index, :format => :json, :project_id=>@project.id
    assert_response :success    
    assert_match @contact_0.email, @response.body
  end

  test "create" do
    
    post :create, :format => :json, :viewer => {:contact_id=>@contact_1.id}, :project_id=>@project.id
    assert_response :success
    assert_match @contact_1.email, @response.body
    
    assert @project.viewers.count == 2, "Viewer was not aded to user"
  end
  
  test "create two times" do
    
    post :create, :format => :json, :viewer => {:contact_id=>@contact_1.id}, :project_id=>@project.id
    post :create, :format => :json, :viewer => {:contact_id=>@contact_1.id}, :project_id=>@project.id
    assert_response :unprocessable_entity
    assert @project.viewers.count == 2, "Same contact added twice"
  end

  test "destroy contact" do
        
    get :destroy, :format => :json, :id=>@contact_0.id, :project_id=>@project.id
    assert_response :success
    
    @project.viewers.reload
    assert @project.viewers.count == 0, 'Viewer was not removed.'
  end
  
end
