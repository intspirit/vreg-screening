require 'test_helper'

class ContactsControllerTest < ActionController::TestCase
  include UsersFactory
  include Devise::TestHelpers
  
  test "should get index" do
    @user = valid_user
    sign_in @user
    
    contact = add_contacts_to(@user)[0]
        
    get :index, :format => :json
    assert_response :success
    assert_match contact.email, @response.body
  end

  test "create" do
    @user = valid_user
    sign_in @user
    
    post :create, :format => :json, :contact => {:email=>'valid.email@test.net'}
    assert_response :success
    assert_match 'valid.email@test.net', @response.body
    
    assert @user.contacts.count == 1, "Contact was not aded to user"
  end
  
  test "create invalid" do
    @user = valid_user
    sign_in @user

    post :create, :format => :json, :contact => {:email=>'invalid@email'}
    assert_response :unprocessable_entity
    assert_match 'errors', @response.body
    
  end
  
  test "destroy contact" do
    @user = valid_user
    sign_in @user
    contact = add_contacts_to(@user)[0]
        
    get :destroy, :format => :json, :id=>contact.id
    assert_response :success
    
    @user.contacts.reload
    assert @user.contacts.count == 0, 'Contact was not removed.'
    
    assert User.all.count == 2, 'Contact removed, but deleted user.'
  end
  
end
