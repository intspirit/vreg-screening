require 'test_helper'

class ProjectsControllerTest < ActionController::TestCase
  include ProjectsFactory
  include Devise::TestHelpers
  
  test "init data for #edit_presentation" do
    @project = valid_project
    sign_in @project.creator
        
    get :edit_presentation, :id=>@project.id
    assert_response :success
    assert_match 'root', @response.body
  end
  
end
