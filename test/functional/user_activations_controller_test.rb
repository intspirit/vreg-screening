require 'test_helper'

class UserActivationsControllerTest < ActionController::TestCase
  include UsersFactory
  include Devise::TestHelpers
  
  
  test 'shuld sign in user when not yet activated and display change password form' do
    @user = user_with_activation
    get :edit, :id => @user.activation_token
    assert @controller.current_user == @user
    assert @response.body.include? 'user[password_confirmation]'
  end
  
  test 'should validate input and activate user and redirect when password changed' do
    @user = user_with_activation
    sign_in @user
    post :update, :user=>{:password=>'v',:password_confirmation=>'v'}
    assert_response :unprocessable_entity
    assert_match 'errors', @response.body
    
    post :update, :user=>{:password=>'validPASS123',:password_confirmation=>'validPASS123'}
    assert_response :redirect
    @user.reload
    assert @user.activated?
  end
  
  test 'should redirect and NOT signin user when already activated' do
    @user = valid_user
    get :edit, :id => @user.activation_token
    assert_response :redirect
    assert @controller.current_user != @user
  end
  
end