source 'http://rubygems.org'

gem 'rails', '3.2.11'

gem "therubyracer", :require => 'v8'

# Bundle edge Rails instead:
# gem 'rails',     :git => 'git://github.com/rails/rails.git'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass', '~> 3.2.5'
  gem 'sass-rails'
  #gem 'coffee-rails', "~> 3.2.0"
  gem 'uglifier'
  gem 'compass-rails'

    # CSS Frameworks
  gem 'compass'
  gem 'skeleton-rails'
  gem 'jquery-rails'
  gem 'jquery-ui-rails'
  gem 'rails-backbone', git: 'git://github.com/federomero/backbone-rails.git'

  # Haml
  gem 'haml_assets'
  gem 'haml_ejs'
  gem 'ejs'
end

group :production do
  gem 'exceptional'
end

gem 'haml'
gem "ckeditor", "3.7.0.rc3"

gem 'hpricot'
gem 'ruby_parser'
gem 'nokogiri'

# Authentication
gem 'devise'
gem "ruby-ntlm", require: 'ntlm/smtp'

# Titles for ids
gem 'friendly_id'

# File Processing
gem 'dragonfly'
gem 'carrierwave'
gem 'mime-types'
gem 'zencoder'
gem 'fog'

# Ajax File Upload
gem 'remotipart'

# Use unicorn as the web server
gem 'unicorn'
gem 'thin'

# Deploy with Capistrano
gem 'capistrano'
gem 'capistrano-ext'
gem 'rvm-capistrano'

# Hiearchical views
# gem 'ancestry'
gem 'ancestry', :git => "git://github.com/stefankroes/ancestry.git"

# API
gem 'vimeo'
gem 'youtube_it'

gem "soundcloud", ">= 0.2.9", :git=>"git://github.com/andrejj/soundcloud-ruby.git" # released a dependencygem, 

gem 'delayed_job_active_record'
group :production do
  gem 'pg'
end


group :linux_debug do
  # debugger
  #   "Install these gems with these comands first: "
  #   puts "gem install ./lib/gems/linecache19-0.5.13.gem -- --with-ruby-include=$rvm_path/src/#{ENV['RUBY_VERSION']}" 
  #   puts "gem install ./lib/gems/ruby-debug-base19-0.11.26 -- --with-ruby-include=$rvm_path/src/#{ENV['RUBY_VERSION']}" 
  #gem 'linecache19', '0.5.13'
    #gem 'ruby-debug-base19', '0.11.26'
    #gem 'ruby-debug19', :require => 'ruby-debug'    
    #gem 'libnotify',    :require => false if RUBY_PLATFORM =~ /linux/i
  #end
end


group :development, :test do
  gem 'sqlite3'

  # guard - file changes trigger something
  gem 'guard'

  # guard for haml files
  gem 'guard-haml'
  # guard for default rails unit testing files
  gem 'guard-test'

  # guard auto reload page browser when files change
  gem 'guard-livereload'
  # used by livereload to improve speed
  gem 'yajl-ruby'

  gem 'jasmine'
  gem 'jasmine-rails'
  gem 'jasmine-headless-webkit', :git => "git://github.com/johnbintz/jasmine-headless-webkit.git"

  gem 'rvm'

  gem 'mailcatcher'
  gem 'dotenv-rails', :groups => [:development, :test]


end

group :test do
  gem 'capybara'
  gem 'minitest'
  gem 'database_cleaner'
  # Pretty printed test output
  gem 'turn', :require => false
end
