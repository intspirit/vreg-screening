beforeEach(function() {
  
  // provide necesary html environment
  setFixtures("<div id='"+VSR.Config.Foreground.id.substring(1)+"'></div>")

  // The main purpose of thia file here is to STUB server RESPONSES
  // 1. we declare json fixtures
  // 2. we map fixtures to urls
  // 3. we create a stubbed server at this.server
  // 4. we use the url -> fixture mapping to stub responses
  // 5. we create a spy to listen on ajax cals at this.server_spy
  
  // fixtures
  this.fix = {}
  // GET OK[200] reposene mapping
  this.GET = {}
  // GET ERROR[200] server side VALIDATION ERROR
  this.GET_invalid = {}
  this.POST    = {}
  this.PUT     = {}
  this.DELETE  = {}
  
  // Contacts
  this.fix.contacts = [
      {"id": 1, "name" : "Name1", "email" : "aj1@net.net" },
      {"id": 2, "name" : "Name2", "email" : "aj2@net.net" },
      {"id": 3, "name" : "Name3", "email" : "aj3@net.net" },
      {"id": 4, "name" : "Name4", "email" : "aj4@net.net" },
    ],
  this.fix.invalid_contact = {},
  this.GET['/contacts'] = this.fix.contacts
  
  // PROJECTS with viewers
  this.fix.projects = [
      {"id": 1, "title" : "t1"},
      {"id": 2, "title" : "t2"},
    ],
  this.GET['/projects'] = this.fix.projects
  this.fix.invalid_project  = {}
  
  // VIEWERS
  
  this.fix.project_without_viewers = this.fix.projects[0]
  this.GET['/projects/1/viewers'] = []
  
  this.fix.project_with_viewers = this.fix.projects[1]
  this.fix.viewers = [ this.fix.contacts[0], this.fix.contacts[1] ]
  this.GET['/projects/2/viewers'] = this.fix.viewers
  
  // ADDING VIEWERS
  this.fix.contact_to_viewer = this.fix.contacts[2]
  this.POST['/projects/2/viewers'] = this.fix.contact_to_viewer
  
  //VIEW ITEMS
  
  this.fix.view_items = [
  {"ancestry":'',"html":"","id":5,"item_type":"root","position":0},
  {"ancestry":"5" ,"html":"","id":6,"item_type":"videos-list","position":0},
  {"ancestry":"5" ,"html":"","id":7,"item_type":"audios-list","position":1},
  {"ancestry":"5" ,"html":"","id":8,"item_type":"attachment-list","position":2},
  {"ancestry":"5" ,"html":"","id":9,"item_type":"drop-panel","position":3}
  ]
  
  // used in models/view_item_spec on child creation
  this.POST['/view_items/5/view_items'] = {"ancestry":"5" ,"html":"","id":1000,"item_type":"drop-pannel","position":0}
  
  // VIDEO - VIEW ITEM
  // child of videos-list in this.fix.view_items 

  // zencoder
  this.fix.zencoder_video = {"ancestry":"5/6" ,"html":"","id":100,"item_type":"video","sub_type":"zencoder","position":0},
  
  // youtube
  this.fix.youtube_video = {"ancestry":"5/6" ,"html":"","id":101,"item_type":"video","sub_type":"youtube","position":1},
  
  // vimeo
  this.fix.vimeo_video = {"ancestry":"5/6" ,"html":"","id":102,"item_type":"video","sub_type":"vimeo","position":2},
  
  // VIDEOS_LIST - VIEW ITEM
  
  // video list WIHTOUT VIDEOS
  
  // copy collection of all view items
  this.fix.view_items_for_videos_list_without_videos = _.clone(this.fix.view_items)
  this.fix.videos_list_without_videos = this.fix.view_items_for_videos_list_without_videos[1]
  
  // video list WITH ALL TYPES OF VIDEOS
  
  // copy collection of all view items
  this.fix.view_items_for_videos_list_with_videos = _.clone(this.fix.view_items)

  // fill videos to collection
  this.fix.view_items_for_videos_list_with_videos.push(this.fix.youtube_video)
  this.fix.view_items_for_videos_list_with_videos.push(this.fix.vimeo_video)

  // set reference to video list
  this.fix.videos_list_with_videos = this.fix.view_items_for_videos_list_without_videos[1]
  
  
  // SERVER
  
  // Create fake server
  this.server = sinon.fakeServer.create();
  
  // stub server calls
  
  _.each(this.GET, function(fixture,url){
    this.server.respondWith("GET",url,[200,{"Content-Type": "application/json"},JSON.stringify(fixture)] )
  }.bind(this));
  
  _.each(this.POST, function(fixture,url){
    this.server.respondWith("POST",url,[200,{"Content-Type": "application/json"},JSON.stringify(fixture)] )
  }.bind(this));
  
  // create server spy
  this.server_spy = sinon.spy(jQuery, 'ajax');
  
});

afterEach(function() {
  //remove server stub
  this.server.restore();
  // remove ajax spy
  this.server_spy.restore();
});