describe('VideosListView',function(){
  
  it("Should render 'Add first video' button when model has no children",function(){
    // setup
    var collection = new VSR.Collections.ViewItems(this.fix.view_items_for_videos_list_without_videos)
    var model = collection.get(this.fix.videos_list_without_videos.id)    
    var view = new VSR.Views.VideosList({model:model})
    
    view.render()

    expect(view.$el).toContain('.initial.video')
  })
  
  it("should render video list when has at least two children.",function(){
    var collection = new VSR.Collections.ViewItems(this.fix.view_items_for_videos_list_with_videos)
    var model = collection.get(this.fix.videos_list_with_videos.id)    
    var view = new VSR.Views.VideosList({model:model})
    
    view.render()
    expect(view.$el).toContain('.videos')
    expect(view.$('.videos').html()).toNotEqual('')
  })
  
})