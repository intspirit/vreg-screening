describe('VideoForm',function(){
  it('Should display a video type selection for new and switch fildsets',function(){
    var model = new VSR.Models.ViewItem({item_type:'video',sub_type:'vimeo'})
    var view = new VSR.Views.VideoForm({model:model})
    VSR.Config.Foreground.renderView(view)
    
    expect(view.$("select[name='view_item[sub_type]']")).toExist()
    expect(view.$("input[name='view_item[item_type]']")).toExist()
    
    // test select value
    expect(view.$("*[name='view_item[item_type]']").val()).toEqual(model.get('item_type'))
    
    // test fieldset display
    expect(view.$('.'+model.get('sub_type')+"-fields")).toBeVisible()
  })
})