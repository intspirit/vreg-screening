describe("ProjectView", function() {
  beforeEach(function(){
    this.project          = new VSR.Models.Project(this.fix.project_with_viewers)
    this.projectView      = new VSR.Views.Project({model:this.project});
    this.server.respond()
  });
  
  it("should load contacts and viewers on init", function() {
    this.server.respond()
    
    var urls = ['/projects/'+this.project.id+'/user_actions','/projects/'+this.project.id+'/viewers',"/contacts","/contact_groups"]
    
    // expect last three urlls to match urls
    var actual
    for(var i=0;i<urls.length;i++){
      actual = this.server_spy.getCall(i).args[0].url
      expect(_.any(urls,
          _.bind( function(expected) {
            return expected==actual
          },{actual:actual})
        )
      ).toBeTruthy(actual+' wasnt called');
    }
  });
  
  it("should render project's contacts and viewers",function(){
    this.projectView.render()
    var count = this.projectView.$("li:contains('"+this.fix.viewers[0].email+"')").length
    expect(count).toEqual(2)
  });
  
  it("should add contact to viewers when doubleclicking contact and remove when doubleclicking viewer", function(){
    this.projectView.render()
    
    var li = this.projectView.$('li:[data-id="'+this.fix.contact_to_viewer.id+'"]')
    li.trigger('dblclick')
    
    this.server.respond()
    
    var count = this.projectView.$("li:contains('"+this.fix.contact_to_viewer.email+"')").length
    expect(count).toEqual(2)
    
    var li = this.projectView.$('li:[data-id="'+this.fix.contact_to_viewer.id+'"]').last()
    li.trigger('dblclick')
    this.server.respond()
    
    var count = this.projectView.$("li:contains('"+this.fix.contact_to_viewer.email+"')").length
    expect(count).toEqual(1)    
    
  });
  
  it("should display #contact-form when clicked on 'ADD contact' ", function(){
    this.projectView.render()
    this.projectView.$('.add-contact').trigger('click')
    expect($('#contact_form')).toBeVisible()
  });
  
  it("should display #edit_project when clicked 'Edit Title and Description'", function(){
    this.projectView.render()
    this.projectView.$('#edit_project').trigger('click')
    expect($('.project-form')).toBeVisible()    
  });
  
  it("Should navigate to presentation editor when clickin 'edit presentation'.",function(){
    // it works however i dont now how to stub window.location.href
    // this.projectView.render()
    // this.server.respondWith("GET",this.projectView.model.editPresentationURL())
    // this.projectView.$('#edit_presentation').trigger('click')
    // expect(window.location.href).toEqual(this.project.editPresentationURL()); 
  })
});