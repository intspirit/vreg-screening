describe('ViewItems',function(){
  it("should reset items collection",function(){
    var router = new VSR.Routers.ViewItems(this.fix.projects[0].id, this.fix.view_items,this.fix.view_items[0].id)
    expect(router.collection.project_id).toEqual(this.fix.projects[0].id)
    expect(router.collection.length).toEqual(this.fix.view_items.length)
  })
})