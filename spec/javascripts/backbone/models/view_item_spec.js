describe('ViewItem',function(){
  
  it('ViewItem form collection, has defined children()',function(){
    var collection = new VSR.Collections.ViewItems()
    collection.reset(this.fix.view_items)
    var root = collection.first()
    expect(root.children().length).toEqual(4) //hardcoded 4 children in fixtures
  })
  
  it('collection gets updated when child is created',function(){
    
    // setup
    var collection = new VSR.Collections.ViewItems()
    collection.reset(this.fix.view_items)
    var root = collection.first()    
    var new_child = root.buildChild({item_type:'drop-panel'})
    
    //test if attributes were assigned
    expect(new_child.get('item_type')).toEqual('drop-panel')
    expect(new_child.parentID()).toEqual(''+root.id)
    
    //test if successful creation of child updates collectio  
      
    new_child.save({},{
      wait:true,
      success: function(model) {
        //insert target into wrapper        
        model.collection.add(model)
        expect(model.parent().children().length).toEqual(5) //4 in fixutres 5 after succesful save
      },
      error:function(){
        debugger
      }
    })
    
    this.server.respond()
    
    //this has to be done in a callback
    
    var response_to_save = this.server_spy.getCall(0).args[0]
    expect(response_to_save.url).toEqual("/view_items/"+new_child.parent().id+"/view_items");


    
  })
})