describe('Project',function(){
  it('should have defined editor URL', function(){
    var project = new VSR.Models.Project();
    project.id = 100;

    var url = '/projects/100/edit_presentation#projects/100/edit_presentation'
    expect(project.editPresentationURL()).toEqual(url)
  });
})