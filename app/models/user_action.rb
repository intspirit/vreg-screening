class UserAction < ActiveRecord::Base
  belongs_to :user
  belongs_to :project
  belongs_to :view_item
  
  before_validation :generate_description
  
  validates_presence_of :description
  
  def occured_at
    self.created_at
  end
  
  def as_json(options={})
    h = super(options)
    h[:occured_at] = occured_at.to_formatted_s(:long)
    h[:email] = user.email if user && user.email
    h
  end
  
  def description
    if self.action_type == 'play_video' or self.action_type == 'play_audio'
      title = view_item ? view_item.title : 'Deleted media'
      intervals = read_attribute(:description)
      if intervals and not intervals.nil?
        intervals = intervals.split(',').map do |int|
          int = int.split('-').map do |s|
            s = s.to_i
            "#{s/60}:#{s%60}"
          end
          int.join('-')
        end
        intervals = intervals.join(', ')
      end
      " #{title} #{intervals}"
    else
      read_attribute :description
    end    
  end
  
  def generate_description
    if self.description.nil?
      if self.action_type == 'download' and self.view_item_id and self.view_item.item_type == 'attachment'
        self.description = "#{view_item.file.name}"
      elsif self.action_type == 'logged'
        self.description = "Logged in"
      elsif self.action_type == 'selected'
        self.description = "Opened in dashboard"
      elsif self.action_type == 'present'
        self.description = "Show presentation"
      else
        self.description = 'Missing description'
      end
    else
      if self.action_type == 'play_video' || self.action_type == 'play_audio'
        self.description = read_attribute(:description)[1..-1] if read_attribute(:description)[0] == ","
      end
    end
    true
  end
end
