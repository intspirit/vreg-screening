class Attachment < ViewItem
    
  image_accessor :file

  after_save :update_title

private
  def update_title
    self.update_attributes :title => self.file.basename if self.file and not self.title
  end
end
