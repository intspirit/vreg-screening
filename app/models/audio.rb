class Audio < ViewItem
  
  SOUNDCLOUD_CLIENT_ID = '2db4d214c1c7546c57ec2778e3dacb5f'
  
  validates :sub_type, :inclusion => { :in => ['soundcloud'] }
  
  # SOUNDCLOUD CONFIG
  validates_presence_of :soundcloud_id, :if => "sub_type == 'soundcloud'"
  validate  :soundcloud_api_must_know_soundcloud_id_or_url, :if => "sub_type == 'soundcloud'"
  
  after_validation :get_data_from_soundcloud, :if => "sub_type == 'soundcloud'"
  
  def soundcloud_api_must_know_soundcloud_id_or_url
    @soundcloud_client ||= Soundcloud.new(:client_id => Audio::SOUNDCLOUD_CLIENT_ID)
    
    if /^[\d]+$/ === self.soundcloud_id
      begin
        @track = @soundcloud_client.get('/tracks/'+self.soundcloud_id)
      rescue
        errors.add(:soundcloud_id, ' not a valid sound cloud ID')
      end
    else
      begin
        @track = @soundcloud_client.get('/resolve', :url => self.soundcloud_id)
        self.soundcloud_id= @track.id        
      rescue
        logger.debug soundcloud_id
        errors.add(:soundcloud_id, ' not a valid soundcloud URL')
      end
    end
  end
  
  def get_data_from_soundcloud
    if errors[:soundcloud_id].empty?
      unless @track
        @soundcloud_client ||= Soundcloud.new(:client_id => Audio::SOUNDCLOUD_CLIENT_ID)
        @track = @soundcloud_client.get('/tracks/'+self.soundcloud_id)
      end
      self.title = @track.title unless self.title
    end
  end
  
end
