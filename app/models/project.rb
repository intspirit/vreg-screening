class Project < ActiveRecord::Base
  # attr_accessible :id, :ancestry, :position, :item_type, :html, :type, :image_uid,:thumbnail_uid,
  #                 :thumbnail_hover_uid,:title,:file_uid,:vimeo_id,:youtube_id,:column_size,
  #                 :sub_type,:soundcloud_id,:duration,:video_file,:zencoder_output_id,:show_link,:thumb_url,
  #                 :view_item_id, :draft_id

  # Project.order('id').each do |project|
  #   unless project.draft.nil?
  #     project.draft = nil
  #     project.save
  #     project.draft
  #   end
  # end


  extend FriendlyId
  friendly_id :title, use: :slugged

	belongs_to :creator, :class_name => "User", :foreign_key => :creator_id
  belongs_to :view_item
  belongs_to :draft, class_name: "ViewItem"

  validates_presence_of :title
  validates :category, :inclusion => { :in => %w(domestic china),
    :message => "%{value} is not a valid category" }

  has_many :invitations
  has_many :contact_groups
  has_many :user_actions

  has_and_belongs_to_many :viewers, :class_name => 'Contact', :join_table => 'projects_viewers', :association_foreign_key => 'viewer_id', :uniq => true
  has_many :invited_viewers, :class_name => 'User', :through => :invitations, :source => :viewer

  before_create :init_view_items
  after_create :create_contact_group


  def template_class_name
    @template_class_name ||= 'default'
  end

  def template_class_name=(_template_class_name)
    @template_class_name = _template_class_name
  end

  def category
    super || "domestic"
  end

  def archived
    super || false
  end

  def draft
    return super unless super.nil?
    draft = view_item.dup
    draft.save
    update_attribute(:draft_id, draft.id)

    # clone_item(draft, view_item.children)
    clone_item_new(draft, view_item.descendants.order("id").arrange_serializable)
    # clone_item_new(draft, view_item.)

    draft
  end

  def clone_item_new(new_root, old_childrens)
    old_childrens.each do |child|
      p child
      childs = child["children"]
      new_child = ViewItem.find_by_id(child['id']).dup
      new_child.parent = new_root
      new_child.save
      if childs.size > 0
        clone_item_new(new_child, childs)
      end
    end
  end

  def clone_item(new_ancestor, items)
    new_item_array = items.sort{|x, y| x.position <=> y.position}
    new_item_array.each do |child|
      new_child = child.dup
      new_child.parent = new_ancestor
      # new_child.ancestry = new_ancestor.ancestry
      new_child.save

      clone_item(new_child, child.children) if child.has_children?
      new_child =""
    end
  end

  def init_view_items(destroy_existing=false)

    view_item.destroy if destroy_existing and view_item_id

    current_parent = create_view_item({item_type:'root'})
    current_template = SCREENING_TEMPLATES['templates'].find{|t| t['class_name'] == template_class_name}

    def consume_teplate(current_parent,current_template)
      return if not current_template['children']
      current_template['children'].each_with_index do |child_template,index|
        attrs = {item_type:child_template['item_type'], position:index}
        attrs[:sub_type] = child_template['sub_type'] if child_template['sub_type']
        new_parent = current_parent.children.create(attrs)
        consume_teplate(new_parent,child_template)
      end
    end

    consume_teplate(current_parent,current_template)

  end

  def publish options
    update_attributes(view_item_id: draft.id, draft_id: nil)

    content = {message: options[:message]}
    selected_viewers = self.viewers.select do |v|
      options[:viewers].any?{|id| id.to_i == v.id}
    end
    selected_viewers.each do |viewer|
      if viewer.connected_user.invited_projects.exists? self
        Invitation.where(project_id: self.id, user_id:viewer.connected_user.id).first.update_message(options[:message])
      else
        Invitation.create_invitation  self, options[:creator].present? ? options[:creator] : self.creator, viewer.connected_user, content
      end
    end
  end

  def to_param
    "#{id} #{title}".parameterize
  end

private
  def create_contact_group
    contact_group = self.creator.contact_groups.build(:name => self.title)
    contact_group.project = self
    contact_group.save
  end
end
