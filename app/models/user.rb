class User < ActiveRecord::Base
  
  has_many :contact_groups, :dependent => :destroy
  # has_and_belongs_to_many :contacts, :class_name=>'User', :association_foreign_key=>'contact_id', :uniq=>true, :join_table=>'users_contacts'
  has_many :contacts, :dependent => :destroy
  has_many :reverse_contacts, :class_name => 'Contact', :foreign_key => 'connected_id', :dependent => :destroy
  has_and_belongs_to_many :viewable_projects, :class_name=>'Project', :join_table => 'projects_viewers', :foreign_key=>'viewer_id', :uniq => true
  has_many :projects, :foreign_key=>'creator_id'

  has_many :invitations, :source => :viewer, :dependent => :destroy
  has_many :invited_projects, :class_name => 'Project', :through => :invitations, :source => :project
  
  has_many :user_actions

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
        
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :name, :password, :password_confirmation, :remember_me, :id
   
  def self.generate_activation_token
    SecureRandom.base64(32).gsub(/[^a-zA-Z0-9]/,"-").gsub(/=+$/,"")
  end

  # Implements shared contacts for admin users
  def accessible_contacts
    if super_user?
      admin_ids = User.where(super_user: true).map(&:id)
      Contact.where(:user_id => admin_ids)
    else
      self.contacts
    end
  end

  def accessible_contact_groups
    if super_user?
      admin_ids = User.where(super_user: true).map(&:id)
      ContactGroup.where(:user_id => admin_ids)
    else
      contact_groups
    end
  end
  
  def self.new_with_activation *args
    user = User.new *args
    user.activation_token = User.generate_activation_token
    user.password = user.activation_token
    user.password_confirmation = user.activation_token
    user
  end
  
  def activated?
    (read_attribute(:activated) and not activation_token.nil?) or activation_token.nil?
  end
  
  def name_and_email
    "\"#{full_name}\" <#{email}>"
  end
  
  def name_or_email
    if name
      name
    else
      email
    end
  end
  
  def email=(_email)
    _email = _email.strip unless _email.nil?
    write_attribute(:email,_email)
  end
  
  def add_contact(contact_attributes)
    email = contact_attributes[:email]
    email = email.strip unless email.nil?
    contact_user = User.where(email: email).first
    if contact_user
      # add existing user to contacts
      existing_contact = self.accessible_contacts.where(connected_id: contact_user.id)
      
      if existing_contact.empty?
        return add_new_contact_with(contact_user, contact_attributes)
      else
        contact_user.errors.add(:email, 'is already in your contacts')
      end
    else
      contact_user = User.new_with_activation(email: email)
      return add_new_contact_with(contact_user, contact_attributes) if contact_user.save
    end
    return contact_user
  end
  
  def to_json(options = {})
    super(options.merge(:with => [ :id, :email, :name]))
  end

  def add_new_contact_with contact_user, contact_attributes
    new_contact = self.contacts.new
    new_contact.connected_user = contact_user
    new_contact.email = contact_attributes[:email]
    new_contact.full_name = contact_attributes[:full_name]
    new_contact.save
    new_contact
  end  
end
