class ContactGroup < ActiveRecord::Base
  belongs_to :user
  belongs_to :project
  
  has_and_belongs_to_many :contacts
  
  def all_contacts
    contacts = self.contacts
    contacts += self.project.viewers if self.project.present?
    contacts.uniq
  end
end
