class Video < ViewItem
  
  validates :sub_type, :inclusion => { :in => ['vimeo', 'youtube', 'upload', nil] }
  
  # VIMEO CONFIG
  validates_presence_of :vimeo_id, :if => "sub_type == 'vimeo'"
  validate  :vimeo_api_must_know_vimeo_id, :if => "sub_type == 'vimeo'"
  
  after_validation :get_thumbnail_from_vimeo, :if => "sub_type == 'vimeo'"
  
  # YOUTUBE CONFIG
  
  validates_presence_of :youtube_id, :if => "sub_type == 'youtube'"
  validate              :youtube_api_must_know_youtube_id, :if => "sub_type == 'youtube'"
  
  after_validation :get_thumbnail_from_youtube, :if => "sub_type == 'youtube'"

  # Uploaded video config
  validates_presence_of :video_file, :if => "sub_type == 'upload'"
  mount_uploader :video_file, VideoFileUploader
  
  image_accessor :image do
    copy_to(:thumbnail){|a| a.process(:thumb,'230x130#') }
    copy_to(:thumbnail_hover){|a| a.process(:thumb,'230x130#').process(:greyscale) }
  end
  image_accessor :thumbnail
  image_accessor :thumbnail_hover
  
  def get_thumbnail_from_vimeo
    if vimeo_id_changed?
      unless vimeo_id.nil? or vimeo_id.blank? or not errors[:vimeo_id].empty?
        response = Vimeo::Simple::Video.info(self.vimeo_id).parsed_response.last
        self.title= response["title"] unless self.title
        self.duration = response['duration']
        self.image_url= response["thumbnail_large"]
      end
    end
    true
  end
  
  def vimeo_api_must_know_vimeo_id
    if Vimeo::Simple::Video.info(vimeo_id).response.code == "404"
      errors.add(:vimeo_id, "ID was not found on vimeo.")
    elsif Vimeo::Simple::Video.info(vimeo_id).response.code == "403"
      errors.add(:vimeo_id, "is forbbiden to access.")
    end
  end

  def get_thumbnail_from_youtube
    if youtube_id_changed?
      unless youtube_id.nil? or youtube_id.blank? or not errors[:youtube_id].empty?
        @client ||= YouTubeIt::Client.new
        video = @client.video_by(youtube_id)
        self.title = video.title unless self.title
        self.duration = video.duration
        self.image_url = video.thumbnails.find{|t| t.url =~ /.*hqdefault/}.url
      end
    end
    true
  end

  def youtube_api_must_know_youtube_id
    unless youtube_id.nil? or youtube_id.blank?
      @client ||= YouTubeIt::Client.new
      begin
        @client.video_by(youtube_id)
      rescue OpenURI::HTTPError => e
        error_msg = case e.message
        when '400'
          "is not a valid youtube video id or url."
        when '404'
          "was not fond on youtube."
        end
        errors.add(:youtube_id, error_msg)
      end
    end
  end

  def video_file= s
    if s.is_a? String
      self.write_uploader('video_file', s)
      url = video_file.url
      puts url
      puts video_file.webm.path
      puts video_file.mp4.path
      file_path = video_file.mp4.path.match(/(?<path>.*\/)(?<filename>.*.)\-encoded.mp4/)
      zencoder_response = Zencoder::Job.create({
        input: url,
        output: [{
          url: "s3://#{VideoFileUploader.fog_directory}/#{video_file.webm.path}",
          public: true,
          format: "webm",
          thumbnails: {
            height: 130,
            width: 230,
            aspect_mode: "pad"
        }
        }, {
          url: "s3://#{VideoFileUploader.fog_directory}/#{video_file.mp4.path}",
          format: "mp4",
          public: true,
          thumbnails: {
            height: 130,
            width: 230,
            aspect_mode: "pad",
            base_url: "s3://#{VideoFileUploader.fog_directory}/#{file_path[:path]}",
            filename: file_path[:filename]
          }
        }]
      })

      url = "http://#{VideoFileUploader.fog_directory}.s3.amazonaws.com/#{file_path[:path]}#{file_path[:filename]}.png"
      puts url
      self.thumb_url = url

      puts zencoder_response.body
    else
      super(s)
    end
  end

  def as_json(options = {})
    h = super(options)
    unless self.video_file.blank?
      versions = {}
      (self.video_file.versions.map {|_, version| version}).each do |version|
        versions["video/#{File.extname(version.path).gsub('.', '')}"] = version.url
      end
      h[:versions] = versions
    end
    h
  end

end
