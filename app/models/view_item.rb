class ViewItem < ActiveRecord::Base
  has_ancestry :orphan_strategy => :destroy
  has_position_for_ancestry
  has_one :project
  
  # parent_id shuld go out
  # ancestry should also go out
  mass_assignment_sanitizer = :logger
	attr_accessible :id, :html, :position, :item_type, :sub_type, :title, :file, :image, :parent_id,:vimeo_id,:youtube_id,:soundcloud_id, :video_file, :video_file_type, :show_link, :thumb_url
  
  validates_presence_of :item_type
  
  def self.type_grammar
    @@grammar ||= {
      'template_1' => {
        'video_list' => {:position=>0},
        'audio_list' => {:position=>1},
        'attachments_list' => {:position=>2},
        'dropzone_list' => {:position=>3},
      },
      'root'=>{},
    }
  end
  
  def as_json(options = {})
    attributes = ViewItem.attr_accessible[:default].as_json
    attributes = attributes + [:ancestry,:image_url,:thumbnail_url,:thumbnail_hover_url,:file_url,:file_ext]
    options[:only] = attributes 
    h = super(options)
    h[:image_url]             = image.remote_url if image_uid?
    h[:thumbnail_url]         = thumbnail.remote_url if thumbnail_uid?
    h[:thumbnail_hover_url]   = thumbnail_hover.remote_url  if thumbnail_hover_uid?
    
    if sub_type == 'upload'
      h[:video_url] = self.video_file.url
    end

    if file_uid?
      opts = {:query => {"response-content-disposition" => "attachment"}, :expires => Time.now + 120.minutes}
      h[:file_url]              = file.remote_url(opts)
      extname = File.extname(file.remote_url)
      h[:file_ext]              = extname[1..-1]
    end
    h
  end  
end
