class Invitation < ActiveRecord::Base
  belongs_to :viewer, :class_name=>'User',:foreign_key=>'user_id'
  belongs_to :creator, :class_name=>'User',:foreign_key=>'creator_id'
  belongs_to :project
  
  validates_presence_of :viewer
  validates_presence_of :creator
  validates_presence_of :project
  
  after_create :send_invitation
  
  def self.create_invitation project, creator, viewer, options
    invitation = Invitation.new(options)
    invitation.creator = creator
    invitation.project = project
    invitation.viewer  = viewer
    invitation.save
  end

  def update_message message
    self.update_attribute :message, message
    send_invitation
  end

private
  def send_invitation
    self.viewer.update_attribute(:after_activation_path, "/invitations/#{self.id}") unless viewer.activated?
    InvitationMailer.delay.invitation_mail(self)
  end
  
end
