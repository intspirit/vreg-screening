class Contact < ActiveRecord::Base
  belongs_to :user
  belongs_to :connected_user, :class_name => 'User', :foreign_key => 'connected_id'

  has_and_belongs_to_many :viewable_projects, :class_name=>'Project', :join_table => 'projects_viewers', :foreign_key=>'viewer_id', :uniq => true
  has_and_belongs_to_many :contact_groups

  attr_accessible :full_name, :email
  
  #validates :user_id, :uniqueness => {:scope => :connected_id}

  def name_and_email
    "\"#{full_name}\" <#{email}>"
  end
end
