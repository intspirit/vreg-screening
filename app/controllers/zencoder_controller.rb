class ZencoderController < ApplicationController
  def create
    output_id = params["output"]["id"]
    job_state = params["output"]["state"]

    video = Video.find_by_zencoder_output_id(output_id)
    if job_state == "finished" && video
      video.processed!(params['output'])
    end

    render :nothing => true
  end

end
