class ContactsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_contact_group

  respond_to :json
  
  def index
    if @contact_group.present?
      respond_with @contact_group.all_contacts
    else
      respond_with current_user.accessible_contacts
    end
  end
  
  def destroy    
    if @contact_group.nil?
      current_user.accessible_contacts.delete Contact.find(params[:id])
    else
      @contact_group.contacts.delete Contact.find(params[:id])
    end  
    respond_with :nothing
  end
  
  def create
    contact = current_user.add_contact(params[:contact])
    @contact_group.contacts << contact if @contact_group.present? 

    respond_with contact, :url => nil, :location => nil
  end
  
  def update
    @contact = Contact.find(params[:contact][:id])
    if params[:contact][:group_id].present?
      @contact_group = ContactGroup.find params[:contact][:group_id]
      @contact_group.contacts << @contact
    end
    respond_with @contact
  end
  
private
  def check_contact_group
    if params[:contact_group_id].present?
      @contact_group = current_user.accessible_contact_groups.find(params[:contact_group_id])
    end
  end
end
