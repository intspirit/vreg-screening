class ViewItemsController < ApplicationController
  
  # TODO Async file upload fails unless ....
  skip_before_filter :verify_authenticity_token, :only => [:update, :create]

  
  layout false
  before_filter :authenticate_user!
  before_filter :load_parent, :only=>[:create,:update]
  before_filter :set_klass

  respond_to :json

  def upload
    @view_item = ViewItem.find(params[:id])
    @view_item.video_file = params[:view_item][:video_file]

    if @view_item.save
      respond_with @view_item do |format|
        format.json { render :json => @view_item, :status => 200 }
      end
    else
      respond_with @view_item do |format|
        format.json { render :json => @view_item.errors, :status => :unprocessable_entity }
      end
    end
  end

  def show
    @view_item = ViewItem.find(params[:id])
    respond_with @view_item
  end
  
  def destroy    
    @view_item = ViewItem.find(params[:id])
    #current_user.projects.find @view_item.root.project.id
    @view_item.destroy
    respond_with :nothing, location:''
  end
  
  def create
    #@klass.sanitize_for_mass_assignment(params[:view_item])
    @view_item = @klass.new(params[:view_item])
    @view_item.parent_id = @parent_view_item.id
    if @view_item.save
      respond_with @view_item do |format|
        format.json { render :json => @view_item, :status => 200 }
      end
    else
      respond_with @view_item do |format|
        format.json { render :json => @view_item.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def update
    #@klass.sanitize_for_mass_assignment(params[:view_item])
    @view_item = @klass.find(params[:id])
    @view_item.update_attributes params[:view_item]
    @view_item.parent = @parent_view_item
    
    if @view_item.save
      respond_with @view_item do |format|
        format.json { render :json => @view_item, :status => 200 }
      end
    else
      respond_with @view_item do |format|
        format.json { render :json => @view_item.errors, :status => :unprocessable_entity }
      end
    end
  end

private
  
  def load_parent
    params[:view_item].delete(:ancestry) if params[:view_item][:ancestry].present?
    @parent_view_item = ViewItem.find params[:view_item_id]
  end
  
  def set_klass
    # @klass
    class_name = params[:view_item].present? ? params[:view_item][:item_type] || 'view-item' : 'view-item'
    class_name = class_name.gsub('-','_').camelize
    begin
      @klass = Module.const_get(class_name)
      @klass = ViewItem unless @klass.is_a?(Class)
    rescue NameError
      @klass = ViewItem
    end
  end
end
