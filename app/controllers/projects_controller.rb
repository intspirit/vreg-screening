class ProjectsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :prepare_project, :only => [:publish]
  skip_before_filter :verify_authenticity_token, :only => [:create, :update, :destroy]

  respond_to :json
  respond_to :html, :only => [:index, :dashboard]
  
  def edit_presentation

    @mode = :edit
    @project = if current_user.super_user?
      Project.find(params[:id])
    else
      current_user.projects.find(params[:id])
    end

    @project.update_attributes(draft_id: @project.view_item.id)
    
    @view_items = @project.draft.subtree

    render :layout => 'editor', :inline => ''

  end

  def preview
    @mode = :show
    @project = Project.find(params[:id])
    @view_items = @project.view_item.subtree
    render :layout => 'preview', :inline => ''
  end

  def publish
    options = {}
    options[:message] = params[:description] if params[:description].present?
    options[:creator] = current_user
    options[:viewers] = params[:viewers] || []
    @project.publish(options)

    render :text => "Successfully Published to Viewers"
  end
  
  def index
    @mode = :edit
    @projects = if current_user.super_user?
      Project.all
    else
      current_user.projects
    end

    respond_with @projects
  end

  def dashboard
    @mode = :show
    @projects = current_user.invited_projects.uniq
    respond_with @projects
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    project = Project.find(params[:id])
    respond_with project
  end
  
  def create
    project = current_user.projects.create(params[:project])
    respond_with project, :url => nil, :location => nil
  end

  
  def update
    project = if current_user.super_user?
      Project.find(params[:id])
    else
      current_user.projects.find(params[:id])
    end

    project.update_attributes(params[:project])
    respond_with project, :url => nil, :location => nil
  end


  def destroy    
    project = if current_user.super_user?
      Project.find(params[:id])
    else
      current_user.projects.find(params[:id])
    end
    project.destroy if project
    respond_with :nothing
  end

private
  def prepare_project
    @project = if current_user.super_user?
      Project.find(params[:id])
    else
      current_user.projects.find(params[:id])
    end
  end
end
