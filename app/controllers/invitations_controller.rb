class InvitationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :load_project_and_viewer, :only => [:create]
  
  respond_to :json

  def create
    @invitation = Invitation.new(params[:invitation])
    @invitation.creator = current_user
    @invitation.project = @project
    @invitation.viewer  = @viewer
    @invitation.save
    respond_with @invitation, :url=>nil, :location => nil
  end

  def show
    @invitation = Invitation.find(params[:id])
    redirect_to presentation_path(@invitation.project)
  end

private
  def load_project_and_viewer
    @project = current_user.projects.find(params[:project_id])
    @viewer  = @project.viewers.find(params[:viewer_id]).connected_user
  end
  
end
