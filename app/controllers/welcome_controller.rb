class WelcomeController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    if current_user.super_user?
      redirect_to projects_path
    else
      redirect_to dashboard_projects_path(:anchor => dashboard_projects_path()[1..-1])
    end
  end
end
