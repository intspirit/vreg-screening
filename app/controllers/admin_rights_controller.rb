class AdminRightsController < ApplicationController

  def destroy
    user = User.find(params[:id])
    user.super_user = false
    user.save
    redirect_to :back
  end
end
