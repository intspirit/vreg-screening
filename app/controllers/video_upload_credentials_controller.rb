class VideoUploadCredentialsController < ApplicationController
  respond_to :json

  def new
    bucket            = VideoFileUploader.fog_directory
    access_key_id     = VideoFileUploader.fog_credentials[:aws_access_key_id]
    secret_access_key = VideoFileUploader.fog_credentials[:aws_secret_access_key]

    key             = "#{Rails.env}/uploads/videos/#{Time.now.to_i.to_s}"
    content_type    = ''
    acl             = 'public-read'
    expiration_date = 10.hours.from_now.utc.strftime('%Y-%m-%dT%H:%M:%S.000Z')
    max_filesize    = 1.gigabyte

    policy = Base64.encode64(
      "{'expiration': '#{expiration_date}',
      'conditions': [
      {'bucket': '#{bucket}'},
      ['starts-with', '$key', '#{key}'],
      {'acl': '#{acl}'},
      ['starts-with', '$Content-Type', '#{content_type}'],
      ['content-length-range', 0, #{max_filesize}],
      ]
      }").gsub(/\n|\r/, '')

    signature = Base64.encode64(
      OpenSSL::HMAC.digest(
        OpenSSL::Digest::Digest.new('sha1'),
        secret_access_key, policy)).gsub("\n","")

    respond_with({
      key: "#{key}.mov",
      AWSAccessKeyId: access_key_id,
      acl: acl,
      policy: policy,
      signature: signature,
      'Content-Type' => content_type
    })
  end
end
