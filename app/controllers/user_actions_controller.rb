class UserActionsController < ApplicationController
  before_filter :load
  respond_to :json

  
  def update
    user_action = current_user.user_actions.find(params[:id])
    params[:user_action].delete(:occured_at)
    params[:user_action].delete(:id)
    params[:user_action].delete(:email)
    user_action.update_attributes(params[:user_action])
    user_action.project = @project
    if user_action.save
      respond_with user_action do |format|
        format.json { render :json => user_action, :status => 200 }
      end
    else
      respond_with user_action do |format|
        format.json { render :json => user_action.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def create

    user_action = UserAction.new params[:user_action]
    user_action.user = current_user
    user_action.project = @project
    if user_action.save
      respond_with user_action do |format|
        format.json { render :json => user_action, :status => 200 }
      end
    else
      respond_with user_action do |format|
        format.json { render :json => user_action.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def index
    user_actions = @project.user_actions.order("created_at ASC")
    respond_with user_actions do |format|
      format.json { render :json => user_actions, :status => 200 }
    end
  end
  
  private
  def load
    if current_user.super_user?
      @project = Project.find(params[:project_id])
    else
      begin
        @project = current_user.projects.find(params[:project_id])
      rescue
        @project = current_user.invited_projects.find(params[:project_id])
      end
    end
  end
  
end
