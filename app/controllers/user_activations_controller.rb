class UserActivationsController < ApplicationController  
  before_filter :authenticate_user!, :only=> [:update, :index, :destroy]
  before_filter :super_user, :only => [:index, :create, :destroy]

  def index
    @admins = User.where(super_user: true) - [current_user]
    @active_users = User.where(super_user: false).where(activated: true)
    @inactive_users = User.where(super_user: false).where(activated: false)
  end

  def create
    if params[:email].present?
      existing_user = User.find_by_email params[:email]  
      if existing_user
        existing_user.update_attribute :super_user, true
      else
        user = User.new_with_activation(email: params[:email])
        user.super_user = true
        if user.save
          InvitationMailer.new_admin_activate(user).deliver
        end
      end
    end
    
    redirect_to :back   
  end

  def destroy
    if params[:id].present?
      user = User.find params[:id]
      user.destroy
    end
    redirect_to :back
  end
  
  def edit
    @user = User.where(activation_token:params[:id]).first

    if @user
      # already activated
      if @user.activated?
        if @user.after_activation_path
          redirect_to @user.after_activation_path
        else
          redirect_to root_path
        end
      else
        # show password change
        if current_user and current_user != @user
          sign_out current_user
          sign_in @user
        else
          sign_in @user
        end
      end
    else
      notice[:warn] = "This link has expired."
    end
  end
  
  def update
    @user = current_user
    @user.password = params[:user][:password]
    @user.password_confirmation = params[:user][:password_confirmation]
    @user.activated = true
    if @user.save
      sign_out current_user
      sign_in @user

      if @user.after_activation_path
        redirect_to @user.after_activation_path
      else
        redirect_to root_path
      end
    else
      render :edit, :status=>:unprocessable_entity
    end
  end

private
  def super_user
    current_user.super_user?
  end
end
