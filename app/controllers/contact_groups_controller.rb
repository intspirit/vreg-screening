class ContactGroupsController < ApplicationController
  before_filter :authenticate_user!
  
  respond_to :json
  
  def index
    respond_with current_user.accessible_contact_groups
  end
  
  def create
    contact_group = current_user.contact_groups.create(params[:contact_group])
    respond_with contact_group, :url => nil, :location => nil
  end

  def show
    
  end

  def edit
    
  end

  def destroy
    contact_group = ContactGroup.find params[:id]
    current_user.accessible_contact_groups.delete contact_group if contact_group.present?

    respond_with :nothing
  end

  def new
    
  end

  def update
    
  end
end