class ViewersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :load_project

  respond_to :json
  
  def index
    respond_with @project.viewers
  end
  
  def destroy
    contact = Contact.find(params[:id])
    @project.viewers.delete contact
    invitation = contact.connected_user.invitations.find_by_project_id @project.id
    invitation.delete
    respond_with :nothing
  end
  
  def create
    viewer = if current_user.super_user?
      Contact.find(params[:viewer][:contact_id])
    else
      current_user.contacts.find(params[:viewer][:contact_id])
    end

    unless @project.viewers.include? viewer
      @project.viewers << viewer
    else
      flash[:warn]="#{viewer.name_and_email} is already viewing this project."
      viewer.errors.add(:contact, ": #{viewer.name_and_email} is already viewing this project.")
    end
    respond_with viewer, :url=>nil, :location => nil
  end
  
private
  def load_project
    @project = if current_user.super_user?
      Project.find(params[:project_id])
    else
      current_user.projects.find(params[:project_id])
    end
  end
end