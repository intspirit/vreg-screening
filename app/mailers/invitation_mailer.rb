class InvitationMailer < ActionMailer::Base
  default from: "no-reply@vreg.com"
  
  def invitation_mail(invitation)
    @invitation = invitation
    @creator = @invitation.creator
    @viewer  = @invitation.viewer
    @project = @invitation.project
    
    if @viewer.activated?
      @url  = invitation_url(@invitation)
    else
      @url  = edit_user_activation_url(@viewer.activation_token)
    end
    
    mail(:to => @viewer.email,:subject => "[Village Screening Room] Presents #{@project.title} ", :reply_to=> "screeningroom@vreg.com" ) do |format|
      format.html
    end
  end  

  def new_admin_activate(user)
    @url = edit_user_activation_url(user.activation_token) 
    mail(:to => user.email, :subject => "Admin account has been created for you.") do |format|
      format.html
    end
  end
end
