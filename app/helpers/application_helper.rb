module ApplicationHelper
  def backbone_container id, reset_data= [], tag= :div
    content_tag tag, '', :id => id, data: {reset: reset_data}
  end
end
