// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require underscore
//= require jquery
//= require jquery_ujs
//= require jquery.remotipart
//= require jquery.ui.draggable
//= require jquery.ui.droppable
//= require jquery.ui.progressbar
//= require modernizr-2.5.3
//= require froogaloop
//= require soundcloud_api
//= require backbone
//= require backbone_rails_sync
//= require backbone_datalink
//= require backbone/vsr

// EXCLUDED FOR NOW
// require jPlayer/jquery.jplayer.js

String.prototype.truncate = function(length) {
  if (length == null) 
    length = 30;

  if (this.length > length) {
    content = this.substring(0, length);
    content += '…';
  } else {
    content = this  
  }

  return content; 
};

var viewingMode;
