VSR.Views.ProjectAnalytics = Backbone.View.extend({
    className:'project',

    template: JST["backbone/templates/projects/project"],

    events:{
      'click #destroy_project'      : 'destroy',
      'click #edit_project'         : 'edit',
      'click #edit_presentation'    : 'editPresentation',
      'click #show_statistics'      : 'showStatistics',
      'click .add-contact'          : 'addContact',
      'click .update-contacts'      : 'updateContacts',
      'click .groups-index li'      : 'updateGroupContacts',      
      'click .add-contact-group'    : 'addContactGroup',
      'dblclick .contacts-index li' : 'addContactToViewers',
      'dblclick .viewers-index li'  : 'removeContactFromViewers'
    },
    
    initialize:function() {
      this.model.on('destroy', this.remove,this)
      this.model.on('change' , this.render,this)
      this.model.viewers.fetch()
      
      this.contacts = new VSR.Collections.Contacts()
      this.contacts.fetch() 
      VSR.Data.contacts = this.contacts;
      
      this.groups = new VSR.Collections.ContactGroup()
      this.groups.fetch()
      VSR.Data.groups = this.groups;
      
      // prepare view for groups
      this.groups_index_view = new VSR.Views.ContactGroupsIndex({collection:this.groups});
      this.groups_index_view.small = 'Drag Contacts into Groups and Groups into Assigned Contacts'
      
      // prepare view for address book
      this.contacts_index_view = new VSR.Views.ContactsIndex({collection:this.contacts})
      this.contacts_index_view.title = 'Contacts book'
      this.contacts_index_view.global = true;
      this.contacts_index_view.group_id = null;

      // prepare view for viewers
      this.viewers_index_view = new VSR.Views.ContactsIndex({collection:this.model.viewers})
      this.viewers_index_view.title = 'Assigned Contacts'
      this.viewers_index_view.small = 'double click to remove from project'
      this.viewers_index_view.project = this.model;
    },
    
    // Re-render the contents of the viewItem item.
    render: function() {  
      this.$el.html(this.template({project:this.model}));

      this.$('#viewers_management .actions').append('<button class="update-contacts">Open Contact Book</button>')
      this.$('#viewers_management .actions').append('<button class="add-contact">New Contact</button>')
      this.$('#viewers_management .actions').append('<button class="add-contact-group" style="display: none;">New Group</button>')
    
      this.groups_index_view.render()
      this.$('.groups-index').append(this.groups_index_view.el)
      this.$('.groups-index').hide();
      
      this.contacts_index_view.render()
      this.$('.contacts-index').append(this.contacts_index_view.el)
      this.$('.contacts-index').hide();
      
      this.viewers_index_view.render()
      this.$('.viewers-index').html(this.viewers_index_view.el)
      return this;
    },
    
    destroy: function(){
      this.model.destroy()
      Backbone.history.navigate('')
    },
    
    edit: function(){
      var project_view = new VSR.Views.ProjectFormView({model: this.model});
      VSR.Config.Foreground.renderView(project_view)
    },
    
    addContactToViewers: function(event){
      var id = $(event.currentTarget).attr('data-id')
      var viewer = new VSR.Models.Viewer({contact_id:id})
      viewer.project = this.model
      if (this.viewers_index_view.collection.get(id) == undefined) {
        this.viewers_index_view.collection.create(viewer) 
      } else {
        alert('This contact is already on the list');
      }
    },
    
    removeContactFromViewers: function(event){
      var id = $(event.currentTarget).attr('data-id')
      var viewer = this.viewers_index_view.collection.get(id)
      if(viewer){
        viewer.project = this.model
        viewer.destroy()
        this.viewers_index_view.collection.remove(viewer) 
      }
    },
    
    addContact: function(){
      var contact = new VSR.Models.Contact()
      contact.collection = this.contacts_index_view.collection
      //change should trigger after saving a model and the server responds with 200 OK
      contact.on('change', function(change){
        var viewer = new VSR.Models.Viewer({contact_id:contact.id})
        viewer.project = this.model
      }.bind(this))
      var new_contact_form = new VSR.Views.ContactForm({model:contact})
      VSR.Config.Foreground.renderView(new_contact_form)
    },
    
    updateGroupContacts: function(event) {
      var id = $(event.currentTarget).attr('data-id');
      this.$('.groups-index .selected').removeClass('selected');
      $(event.currentTarget).addClass('selected');
      
      if (id == undefined) {
        this.contacts_index_view.allContactList();
      } else {
        this.contacts_index_view.updateContactList(id);
      }
    },

    updateContacts: function() {
      if (this.$(".update-contacts").html() =="Open Contact Book") {
        this.$(".update-contacts").html("Close Contact Book");
        this.$('.contacts-index').show();
        this.$('.groups-index').show();
        this.$('.add-contact-group').show();
        this.$('.viewers-index').addClass('slim');
      } else {
        this.$(".update-contacts").html("Open Contact Book");
        this.$('.contacts-index').hide();
        this.$('.groups-index').hide();
        this.$('.viewers-index').removeClass('slim');
        this.$('.add-contact-group').hide();
      }
    },
    
    addContactGroup: function() {
      var contactGroup = new VSR.Models.ContactGroup()
      contactGroup.on('change', function(change){
        this.groups_index_view.collection.create(contactGroup);
      }.bind(this));
      var newGroupForm = new VSR.Views.ContactGroupForm({model: contactGroup});
      VSR.Config.Foreground.renderView(newGroupForm);
    },
    
    editPresentation: function(){
      //window.location.href = '/editor_sample'
      window.location.href = this.model.editPresentationURL()
    },
    showStatistics: function(event){
      event.preventDefault()
      Backbone.history.navigate(this.model.showStatisticsURL())
    }
});