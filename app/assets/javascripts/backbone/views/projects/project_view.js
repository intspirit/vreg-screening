VSR.Views.Project = Backbone.View.extend({
  className:'project',

  dashboardTemplate: JST["backbone/templates/projects/dashboard_project"],
  template: JST["backbone/templates/projects/project"],

  events:{
    'click #destroy_project'      : 'destroy',
    'click #archive_project'      : 'archive',
    "click #present_project"      : 'present',
    'click #edit_project'         : 'edit',
    'click #edit_presentation'    : 'editPresentation',
    'click #show_statistics'      : 'showStatistics',
    'click .add-contact'          : 'addContact',
    'click .update-contacts'      : 'updateContacts',
    'click .groups-index li'      : 'updateGroupContacts',      
    'click .add-contact-group'    : 'addContactGroup',
    'click .viewers-index li'     : 'filterActivityLog',
    'click .log_all'              : 'filterActivityLog',
    'change .groups-index input[type=checkbox]' : 'selectedGroupItems',
    'change .contacts-index input[type=checkbox]' : 'selectedContactItems',
    'change .viewers-index input[type=checkbox]' : 'selectedViewerItems',
    'click .add-contacts'     : 'addContacts',
    'click .remove-contacts'  : 'removeContacts',
    'click .add-to-group'      : 'addToGroup',
    'click .remove-from-group' : 'removeFromGroup',
    'click .remove-group'      : 'removeGroup',
    'click .add-to-group-select' : 'addToSelectedGroup',
    'click .add-to-group-cancel' : 'cancelAddToGroup'
  },

  selectedGroupItems: function(e) {
    e.preventDefault();
    var checkboxes = this.groups_index_view.$('input[type=checkbox]:checked');
    this.contacts_index_view.$('input[type=checkbox]:checked').attr('checked', false);
    this.viewers_index_view.$('input[type=checkbox]:checked').attr('checked', false);
    if (checkboxes.size() != 0) {
       this.$('.remove-contacts').hide()
       this.$('.add-contacts').show()
       this.$('.add-to-group').hide()
       this.$('.remove-group').hide()
    } else {
       this.$('.remove-contacts').hide()
       this.$('.add-contacts').hide()
       this.$('.add-to-group').hide()
       this.$('.remove-group').hide()
    }

    if (checkboxes.size() == 1) 
      this.$('.remove-group').show();
  },

  selectedContactItems: function(e) {
    e.preventDefault();
    var checkboxes = this.contacts_index_view.$('input[type=checkbox]:checked');
    this.viewers_index_view.$('input[type=checkbox]:checked').attr('checked', false)
    this.groups_index_view.$('input[type=checkbox]:checked').attr('checked', false)

    if (checkboxes.size() != 0) {
       this.$('.remove-contacts').show()
       this.$('.add-contacts').show()
       this.$('.add-to-group').show()
       this.$('.remove-group').hide()
    } else {
       this.$('.remove-contacts').hide()
       this.$('.add-contacts').hide()
       this.$('.add-to-group').hide()
       this.$('.remove-group').hide()
    }
  },

  selectedViewerItems: function(e) {
    e.preventDefault();
    var checkboxes = this.viewers_index_view.$('input[type=checkbox]:checked');
    this.contacts_index_view.$('input[type=checkbox]:checked').attr('checked', false)
    this.groups_index_view.$('input[type=checkbox]:checked').attr('checked', false)

    if (checkboxes.size() != 0) {
       this.$('.remove-contacts').show()
       this.$('.add-contacts').hide()
       this.$('.add-to-group').hide()
       this.$('.remove-group').hide()
    } else {
       this.$('.remove-contacts').hide()
       this.$('.add-contacts').hide()
       this.$('.add-to-group').hide()
       this.$('.remove-group').hide()
    }
  },
  
  initialize:function() {
    if (VSR.Config.mode == 'show') {
      var user_action = new VSR.Models.UserAction({
        project_id:this.model.id,
        action_type:'selected'
      })
      user_action.save()
    } else {
      this.selected_group = null

      this.model.viewers.fetch()
      this.model.user_actions.fetch() 
      this.contacts = new VSR.Collections.Contacts()
      this.contacts.fetch() 
      VSR.Data.contacts = this.contacts;
      
      this.groups = new VSR.Collections.ContactGroup()
      this.groups.fetch()
      VSR.Data.groups = this.groups;
      
      // prepare view for groups
      this.groups_index_view = new VSR.Views.ContactGroupsIndex({collection:this.groups});
      this.groups_index_view.small = 'Drag Contacts into Groups and Groups into Assigned Contacts'
      
      // prepare view for address book
      this.contacts_index_view = new VSR.Views.ContactsIndex({collection:this.contacts})
      this.contacts_index_view.title = 'Contacts book'
      this.contacts_index_view.global = true;
      this.contacts_index_view.group_id = null;      

      // prepare view for viewers
      this.viewers_index_view  = new VSR.Views.ContactsIndex({collection:this.model.viewers})
      this.viewers_index_view.title = 'Assigned Contacts'
      this.viewers_index_view.small = 'Contacts capable of beeing invited to view presentation.'
      this.viewers_index_view.project = this.model;
      this.viewers_index_view.draggable_children = false
      
      // activity log     
      var contactsMap = new VSR.Collections.Contacts()
      contactsMap.fetch() 
      this.user_actions_index_view = new VSR.Views.UserActionsIndex({collection:this.model.user_actions, contactsCollection: contactsMap})
      this.user_actions_index_view.title = "Activity log"
    }

    this.model.on('destroy', this.remove,this)
    this.model.on('change' , this.render,this)
    
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {  
    if (VSR.Config.mode == 'show') {
      this.$el.html(this.dashboardTemplate({project:this.model}));
    } else {
      this.$el.html(this.template({project:this.model}));
      this.$('#viewers_management .actions').append('<button class="update-contacts">Open Contact Book</button>')
      this.$('#viewers_management .actions').append('<button class="add-contact">New Contact</button>')
      this.$('#viewers_management .actions').append('<button class="add-contact-group" style="display: none;">New Group</button>')
      this.$('#viewers_management .actions').append('<button class="remove-contacts" style="display: none;">Remove</button>')
      this.$('#viewers_management .actions').append('<button class="add-contacts" style="display: none;">Add to Project</button>')
      this.$('#viewers_management .actions').append('<button class="add-to-group" style="display: none;">Add to Group</button>')
      this.$('#viewers_management .actions').append('<div class="group-selection-list" style="display:inline;"></div>')
      this.$('#viewers_management .actions').append('<button class="add-to-group-select" style="display: none;">Select</button>')
      this.$('#viewers_management .actions').append('<button class="add-to-group-cancel" style="display: none;">Cancel</button>')
      this.$('#viewers_management .actions').append('<button class="remove-from-group" style="display: none;">Remove from Group</button>')
      this.$('#viewers_management .actions').append('<button class="remove-group" style="display: none;">Remove Group</button>')
    
      this.groups_index_view.render()
      this.$('.groups-index').append(this.groups_index_view.el)
      this.$('.groups-index').hide();
      
      this.contacts_index_view.render()
      this.$('.contacts-index').append(this.contacts_index_view.el)
      this.$('.contacts-index').hide();
      
      this.viewers_index_view.render()
      this.$('.viewers-index').html(this.viewers_index_view.el)
      
      this.user_actions_index_view.render()
      this.$('.user-actions-index').html(this.user_actions_index_view.el)
    }

    return this;
  },

  addContacts: function(e) {
    e.preventDefault();

    var ids = this.$('input[type=checkbox]:checked').parent().parent()
    if (ids.size != 0 && $(ids[0]).attr('data-type') == 'ContactGroup') {
      ids = ids.map(function(){return $(this).attr('data-id')});
      ids.each(function(index, id) {
        var contacts = VSR.Data.groups.get(id).collection;
        contacts.each(function(contact) {
          if (this.viewers_index_view.collection.get(contact.get('id')) == undefined) {
            var viewer = new VSR.Models.Viewer({contact_id:contact.get('id')})
            viewer.project = this.model;
            this.viewers_index_view.collection.create(viewer); 
          }        
        }.bind(this));
      }.bind(this));
    } else{
      ids = ids.map(function(){return $(this).attr('data-id')});
      ids.each(function(index, id){
        var viewer = new VSR.Models.Viewer({contact_id:id})
        viewer.project = this.model
        if (this.viewers_index_view.collection.get(id) == undefined) {
          this.viewers_index_view.collection.create(viewer) 
        }
      }.bind(this));
    };
  },

  removeContacts: function(e) {
    e.preventDefault();
    var ids = this.$('input[type=checkbox]:checked').parent().parent().map(function(){return $(this).attr('data-id')});

    if (this.$('input[type=checkbox]:checked').parents('.viewers-index').length == 0) {
      if (this.selected_group == null) {
        ids.each(function(index, id) {
          var contact = this.contacts_index_view.collection.get(id)
          if(contact){
            contact.project = this.model
            contact.destroy()
            this.contacts_index_view.collection.remove(contact) 
          }        
        }.bind(this))
      } else{
        ids.each(function(index, id) {
          var group = VSR.Data.groups.get(this.selected_group);
          var contact = this.contacts_index_view.collection.get(id)
          contact.set('group_id', group.id);
          
          contact.destroy({data: $.param({contact_group_id: group.id})});
          group.collection.remove(contact);
        }.bind(this));
      }
    } else{
      ids.each(function(index, id) {
        var viewer = this.viewers_index_view.collection.get(id)
        if(viewer){
          viewer.project = this.model
          viewer.destroy()
          this.viewers_index_view.collection.remove(viewer) 
        }        
      }.bind(this))
    };
  },

  addToGroup: function(e) {
    e.preventDefault();

    $('.group-selection-list').html(this.groups_index_view.contactGroupSelection());
    $('.add-to-group-select').show();
    $('.add-to-group-cancel').show();
    $('.add-to-group').hide();
  },

  addToSelectedGroup: function(e) {
    e.preventDefault();
    var contact_group_id = this.$('.group-selection-list select').val();
    var ids = this.$('input[type=checkbox]:checked').parent().parent().map(function(){return $(this).attr('data-id')});
    var contactGroup = VSR.Data.groups.get(contact_group_id);

    ids.each(function(index, id) {
      var contact = VSR.Data.contacts.get(id);
      contact.set('group_id', contact_group_id);
    
      if (contactGroup.collection.get(contact.id) == undefined)
        contactGroup.collection.create(contact);
    }.bind(this))

    $('.group-selection-list').html('');
    $('.add-to-group-select').hide();
    $('.add-to-group-cancel').hide();
    $('.add-to-group').show();
  },

  cancelAddToGroup: function(e) {
    e.preventDefault();
    $('.group-selection-list').html('');
    $('.add-to-group-select').hide();
    $('.add-to-group-cancel').hide();
    $('.add-to-group').show();
  },

  removeFromGroup: function(e) {
    e.preventDefault();
  },

  removeGroup: function(e) {
    e.preventDefault();
    var ids = this.$('input[type=checkbox]:checked').parent().parent().map(function(){return $(this).attr('data-id')});
    ids.each(function(index, id) {
      var group = VSR.Data.groups.get(id);
      group.destroy();
      this.groups_index_view.collection.remove(group)
    }.bind(this));
  },
  
  destroy: function(){
    if (confirm("Do you really want to remove this project?")) {
      this.model.destroy()
      Backbone.history.navigate('')
    }
  },

  archive: function(){
    this.model.save({archived: true},
      {
        wait: true,
        success: _.bind(function(){
          this.model.trigger('change')
          Backbone.history.navigate('',{trigger:true});
        },this),
      }
    )
  },
  
  edit: function(){
    var project_view = new VSR.Views.ProjectFormView({model: this.model});
    VSR.Config.Foreground.renderView(project_view)
  },


  present: function() {
    if (VSR.Config.mode == 'show') {
      var user_action = new VSR.Models.UserAction({
        project_id:this.model.id,
        action_type:'present'
      })
      user_action.save()
    }

    location.href = this.model.presentationURL();
  },
  
  filterActivityLog: function(event){
    var contact_id = $(event.currentTarget).attr('data-id')
    if(contact_id){
      var contact = this.model.viewers.get(contact_id)
      this.user_actions_index_view.current_user_id = contact.get('connected_id')
      this.user_actions_index_view.title = contact.get('full_name')+' <span class="log_all">Show all</span>'
      this.user_actions_index_view.small = contact.get('email')
    }else{
      this.user_actions_index_view.title = "Activity log"
      this.user_actions_index_view.small = false
      this.user_actions_index_view.current_user_id = false
    }
    this.user_actions_index_view.render()
  },
  
  addContactToViewers: function(event){
    var id = $(event.currentTarget).attr('data-id')
    var viewer = new VSR.Models.Viewer({contact_id:id})
    viewer.project = this.model
    if (this.viewers_index_view.collection.get(id) == undefined) {
      this.viewers_index_view.collection.create(viewer) 
    } else {
      alert('This contact is already on the list');
    }
  },
  
  removeContactFromViewers: function(event){
    var id = $(event.currentTarget).attr('data-id')
    var viewer = this.viewers_index_view.collection.get(id)
    if(viewer){
      viewer.project = this.model
      viewer.destroy()
      this.viewers_index_view.collection.remove(viewer) 
    }
  },
  
  removeContact: function(event) {
    var id = $(event.current).attr('data-id');
    // var contact = this.contacts_index_view.collection.get(id);
  },
  
  addContact: function(){
    var contact = new VSR.Models.Contact()
    contact.collection = this.contacts_index_view.collection
    //change should trigger after saving a model and the server responds with 200 OK
    contact.on('change', function(change){
      var viewer = new VSR.Models.Viewer({contact_id:contact.id})
      viewer.project = this.model
      if(!this.$('add-contact-group').is(':visible'))
        this.viewers_index_view.collection.create(viewer);
    }.bind(this))
    var new_contact_form = new VSR.Views.ContactForm({model:contact})
    VSR.Config.Foreground.renderView(new_contact_form)
  },
  
  updateGroupContacts: function(event) {
    var id = $(event.currentTarget).attr('data-id');
    this.$('.groups-index .selected').removeClass('selected');
    $(event.currentTarget).addClass('selected');
    
    if (id == undefined) {
      this.contacts_index_view.allContactList();
      this.selected_group = null
    } else {
      this.contacts_index_view.updateContactList(id);
      this.selected_group = id
    }
  },

  updateContacts: function() {
    if (this.$(".update-contacts").html() =="Open Contact Book") {
      
      this.$(".update-contacts").html("Close Contact Book");
      this.$('.contacts-index').show();
      this.$('.groups-index').show();
      this.$('.add-contact-group').show();
      this.$('.user-actions-index').hide();
      this.$('.viewers-index').addClass('slim');
      this.viewers_index_view.draggable_children = true
      this.viewers_index_view.render()
    } else {
      this.$(".update-contacts").html("Open Contact Book");
      this.$('.contacts-index').hide();
      this.$('.groups-index').hide();
      this.$('.user-actions-index').show();
      this.$('.viewers-index').removeClass('slim');
      this.viewers_index_view.draggable_children = false
      this.viewers_index_view.render()
      this.$('.add-contact-group').hide();
    }
  },
  
  addContactGroup: function() {
    var contactGroup = new VSR.Models.ContactGroup()
    contactGroup.on('change', function(change){
      this.groups_index_view.collection.create(contactGroup);
    }.bind(this));
    var newGroupForm = new VSR.Views.ContactGroupForm({model: contactGroup});
    VSR.Config.Foreground.renderView(newGroupForm);
  },
  
  editPresentation: function(){
    //window.location.href = '/editor_sample'
    window.location.href = this.model.editPresentationURL()
  },
  showStatistics: function(event){
    event.preventDefault()
    Backbone.history.navigate(this.model.showStatisticsURL())
  }
});