VSR.Views.ProjectFormView = Backbone.View.extend({
  tagName: "div",
  className: "foreground form",

  template: JST["backbone/templates/projects/project_form"],
  
  // The DOM events specific to an item
  events: {
    "click .save": "submit",
    "click .cancel-form": "cancel"
  },

  initialize: function() {
    this.model.on('destroy', this.remove, this);
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.html(this.template({project: this.model}));
    return this;
  },

  initCKEditor: function() {
    if (CKEDITOR.instances['project_description']) { delete CKEDITOR.instances['project_description'] };
    if (CKEDITOR.instances['project_description']) { CKEDITOR.instances['project_description'].destroy(); }

    this.$el.find('#project_description').ckeditor({
      toolbar: 'Basic',
      toolbar_Basic: [
        ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Font', 'FontSize']
      ]
    })
  },

  submit: function(event) {
    event.preventDefault();
    this.model.save(
      this.serialize(),
      {
        wait: true,
        success: _.bind(function(){
          if(!this.model.collection.get(this.model.id)){
            this.model.collection.add(this.model)
          }
          this.model.trigger('change')
          this.remove()
        },this),
        error:_.bind( function(entry,response){
          var err_cont = this.$('.error-explanation')
          if(response.status == 422){
            err_cont.html('')
            var errors = $.parseJSON(response.responseText)
            for(field in errors){
              err_cont.append('<span class="field">'+field+'</span> '+errors[field]+'.<br/>')
            }
          }else{
            err_cont.html(VSR.Config.generircErrorMSG)
          }
        },this)
      }
    )
  },

  cancel: function(event){
    event.preventDefault()
    this.remove()
  },

  serialize: function() {
    return {
      //csrf_token: $('meta[name=\"csrf-token\"]').attr('content'),
      title: this.$("input[name='project[title]']").val(),
      description: this.$("textarea[name='project[description]']").val(),
      category: this.$("select[name='project[category]']").val(),
    };
  }
});
