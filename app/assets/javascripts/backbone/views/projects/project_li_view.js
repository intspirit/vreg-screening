VSR.Views.ProjectListElement = Backbone.View.extend({
    tagName: "li",
    className: "project",

    template: JST["backbone/templates/projects/project_li"],

    // The DOM events specific to an item
    events: {
      "click": "show"
    },

    // The ViewItemView listens for changes to its model, re-rendering.
    initialize: function() {
      this.model.on('change', this.render, this);
      this.model.on('destroy', this.remove, this);
	    this.render();
    },

    // Re-render the contents of the viewItem item.
    render: function() {
      $(this.el).html(this.template({project:this.model}));
      return this;
    },

    show: function(){
      Backbone.history.navigate(this.model.url(),{trigger:true})
    }
  });