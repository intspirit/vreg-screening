VSR.Views.ProjectsIndexView = Backbone.View.extend({
    template: JST["backbone/templates/projects/index"],

    events:{
      'click #first_project'  : 'newProject',
      'click #create_project' : 'newProject'
    },
    
    // The ViewItemView listens for changes to its model, re-rendering.
    initialize: function() {
      if (!VSR.Data.contacts) {
        this.contacts = new VSR.Collections.Contacts()
        this.contacts.fetch() 
        VSR.Data.contacts = this.contacts;
      }
      this.collection.on('add', this.render, this);
      this.collection.on('reset', this.render, this);
      this.collection.on('destroy', this.render, this);
      this.collection.on('change', this.render, this);
      this.collection.reset(this.$el.data('reset'))
    },

    // Re-render the contents of the projects.
    render: function() {
      this.$el.html(this.template());
      var projects_container = this.$('#projects')
      this.$('#projects').html()
      if(this.collection.length == 0) {
        this.$('#create_project').hide()
        this.$('#project').html('<div id="create-new-project">There are currently no projects. Please create your first project.<br/><button id="first_project" type="button">Create your first project</button></div>');
      } else {
        this.$('#create_project').show()
        projects_container.html('')
        // debugger;
        var categories = ['domestic', 'china'];
        var categoryNames = {domestic: 'Domestic', china: 'China'};
        _(categories).each(function(category){
          var subset = _(this.collection.where({category: category, archived: false}));
          projects_container.append('<li class="category"><h1>' + categoryNames[category] + '</h1></li>');
          if (!subset.any()) {
            projects_container.append('<li>No projects.</li>');
          } else {
            subset.each(function(project){
              projects_container.append(this.appendProject(project));
            }.bind(this))
          }
        }.bind(this));
        projects_container.append('<li class="category"><a href="#archive">Archived projects</a></li>');
      }

      return this;
    },

    newProject: function(e) {
      e.preventDefault();
      var newProject = new VSR.Models.Project();
      newProject.collection = this.collection
      var project_view = new VSR.Views.ProjectFormView({model: newProject});
      VSR.Config.Foreground.renderView(project_view)
    },

    appendProject:function(project){
      var view = new VSR.Views.ProjectListElement({model: project})
      return view.render().$el
    }
});