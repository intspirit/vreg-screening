VSR.Views.ProjectsArchivedView = Backbone.View.extend({
    template: JST["backbone/templates/projects/archived"],

    initialize: function(){
      if (!VSR.Data.contacts) {
        this.contacts = new VSR.Collections.Contacts()
        this.contacts.fetch() 
        VSR.Data.contacts = this.contacts;
      }
    },

    // Re-render the contents of the projects.
    render: function() {
      this.$el.html(this.template());
      var projects_container = this.$('#archived-projects')
      this.$('#archived-projects').html()
      var archived = _(this.collection.where({archived: true}));


      projects_container.html('')
      if (!archived.any()) {
        projects_container.append('<li>No archived projects.</li>');
      } else {
        projects_container.append('<li class="category"><h1>Archived projects</h1></li>');
        archived.each(function(project){
          projects_container.append(this.appendProject(project));
        }.bind(this))
      }
      projects_container.append('<li class="category"><a href="#">All projects</a></li>');

      return this;
    },

    appendProject:function(project){
      var view = new VSR.Views.ProjectArchivedListElement({model: project})
      return view.render().$el
    }
});