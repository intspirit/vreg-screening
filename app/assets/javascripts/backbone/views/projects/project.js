VSR.Views.Project = Backbone.View.extend({
    tagName: "tr",

    template: function() {
      if (VSR.Config.mode == 'view') {
        JST["backbone/templates/projects/dashboard_project"]
      } else {
        JST["backbone/templates/projects/project"]
      } 
    },

    // The DOM events specific to an item
    events: {
      "click .edit": "edit",
      "click .destroy": "remove"
    },

    // The ViewItemView listens for changes to its model, re-rendering.
    initialize: function() {
      this.model.bind('change', this.render, this);
      this.model.bind('destroy', this.remove, this);

      _.bindAll(this, 'render');
	  this.render();
    },

    // Re-render the contents of the viewItem item.
    render: function() {
      $(this.el).html(this.template({project:this.model}));
      return this;
    },

    // Edit content of the viewItem
    edit: function(e) {
      e.preventDefault();
      this.projectForm = new VSR.Views.ProjectFormView({model: this.model});
    },

    // Close editing mode
    close: function() {
      this.model.save({html: this.input.val()});
      this.$el.removeClass('editing');
    },

    // Remove this view from the DOM
    remove: function(e) {
        e.preventDefault();
      this.$el.remove();
      this.model.destroy();
    },

    // Remove the item, destroy the model
    clear: function(e) {
      e.preventDefault();
      this.$el.remove();
      this.model.destroy();
    }
  });