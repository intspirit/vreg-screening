VSR.Views.ProjectArchivedListElement = Backbone.View.extend({
    tagName: "li",
    className: "archived-project",

    template: JST["backbone/templates/projects/project_archived_li"],

    // The DOM events specific to an item
    events: {
      "click button": "unarchive"
    },

    // The ViewItemView listens for changes to its model, re-rendering.
    initialize: function() {
      this.model.on('change', this.render, this);
      this.model.on('destroy', this.remove, this);
      this.render();
    },

    // Re-render the contents of the viewItem item.
    render: function() {
      $(this.el).html(this.template({project:this.model}));
      return this;
    },

    unarchive: function(){
      this.model.save({archived: false},
        {
          wait: true,
          success: _.bind(function(){
            this.model.trigger('change')
            Backbone.history.navigate('',{trigger:true});
          },this),
        }
      )
    },
  });