VSR.Views.ProjectsDashboardView = Backbone.View.extend({
    template: JST["backbone/templates/projects/dashboard"],

    events:{
    },
    
    // The ViewItemView listens for changes to its model, re-rendering.
    initialize: function() {
      if (!VSR.Data.contacts) {
        this.contacts = new VSR.Collections.Contacts()
        this.contacts.fetch() 
        VSR.Data.contacts = this.contacts;
      }

      this.collection.on('add', this.render, this);
      this.collection.on('reset', this.render, this);
      this.collection.on('destroy', this.render, this);
      this.collection.reset(this.$el.data('reset'))

      // this brakes - controller is not ready to create user actions without projects
      
      //if (VSR.Config.mode == 'show') {
      //  var user_action = new VSR.Models.UserAction({
      //    action_type:'logged',
      //    description:'User entered dashboard.'
      //  })
      //  user_action.save()
      //}
    },

    // Re-render the contents of the projects.
    render: function() {
      this.$el.html(this.template());
      var projects_container = this.$('#projects')
      this.$('#projects').html()
      if(this.collection.length == 0) {
        this.$('#create_project').hide()
        projects_container.append('<li>No projects.</li>');
        this.$('#project').html('<div id="create-new-project">There are currently no projects.</div>');
      } else {
        this.$('#create_project').show()
        projects_container.html('')
        _(this.collection.where({archived:false})).each(function(project){
          projects_container.append(this.appendProject(project));
        }.bind(this))
      }

      return this;
    },

    appendProject:function(project){
      var view = new VSR.Views.ProjectListElement({model: project})
      return view.render().$el
    }
});