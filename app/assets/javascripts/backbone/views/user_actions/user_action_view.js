VSR.Views.UserActionView = Backbone.View.extend({
  template: JST["backbone/templates/user_actions/user_action"],
  
  initialize:function(){
    // this.collection.on('change',this.render,this)
    // this.collection.on('reset',this.render,this)
  },
  
  render:function(){
    // debugger;
    var contact = this.model.find_contact(this.collection, this.model.get('user_id')) || new VSR.Models.Contact({});
    this.$el.html(this.template({model: this.model, contact: contact}));
    return this;
  }
  
})