VSR.Views.UserActionsIndex = Backbone.View.extend({
  template: JST["backbone/templates/user_actions/user_actions_index"],
  member_template: JST["backbone/templates/user_actions/user_action"],
  
  initialize:function(attrs){
    this.collection.on('change',this.render,this)
    this.collection.on('reset',this.render,this)
    this.contactsCollection = attrs.contactsCollection;
    this.contactsCollection.on('all', this.render, this)
  },
  
  render:function(){
    this.$el.html(this.template())
    var title = this.title || "Activity log"
    this.$('h3').html(title)
    if(this.small){
      this.$('.head').append('<small>'+this.small+'</small>')
    }

    var user_actions
    if(this.current_user_id){
      user_actions = this.collection.filter(function(model){
        return model.get('user_id') == this.current_user_id
      },this )
    }else{
      user_actions = this.collection.models
    }
    
    if(user_actions.length == 0){
      this.$('ul').remove()
      this.$el.append('<h3>Log is empty.</h3>')
    } else {
      _.each(user_actions,function(action){    
        var view = new VSR.Views.UserActionView({model: action, collection: this.contactsCollection});
        this.$('ul').append(view.render().el)  
        // this.$('ul').append(this.member_template({model:action}))
      },this) 
    }
  }
  
})