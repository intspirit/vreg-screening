VSR.Editors.Content = Backbone.View.extend({
    el: "#main-editor",

    //editorTemplate: _.template($("#editor-template").html()),

    // Future events happening on editor like drag'n'drop, etc.
    events: {
      "click": "clicked",
      "keypress #new-viewItem": "createOnEnter"
    },

    view_items: {},

    initialize: function() {
      this.input = this.$('#new-viewItem');

      VSR.Collections.ViewItems.bind('add', this.addOne, this);
      VSR.Collections.ViewItems.bind('reset', this.addAll, this);
      VSR.Collections.ViewItems.bind('all', this.render, this);

      this.view_items = new VSR.Collections.ViewItems;
      this.view_items.fetch();
    },

    // Re-rendering only parts relevant to the editor. The rest of the app doesn't change
    render: function() {
      $(this.el).html(this.editorTemplate);
      this.$("#editing-toolbar").html('dfsa')
      return this;
    },

    clicked: function() {
      alert('cliked on top');
    },

    // Add one new view item to the editor.
    addOne: function(viewItem) {
      var view = new VSR.Views.ViewItemView({model: viewItem});
      this.$("#viewItem-list").append(view.render().el);
    },

    // Add all view items in ViewItem collection to the editor
    addAll: function() {
      VSR.Collections.ViewItems.each(this.addOne);
    },

    // Generate the attributes for a new View item.
    newAttributes: function() {
      return {
        html: this.input.val(),
        position: 0
      }
    },

    // Hitting enter in the input field will create new viewItem
    createOnEnter: function(e) {
      if (e.keyCode != 13) return;
      VSR.Collections.ViewItems.create(this.newAttributes());
      this.input.val('');
    }
  });