VSR.Views.PublishFormView = Backbone.View.extend({
  tagName: "div",
  className: "foreground form",

  template: JST["backbone/templates/editor/publish_form"],
  
  // The DOM events specific to an item
  events: {
    "click .save": "submit",
    "click .cancel-form": "cancel",
    "click .all": "selectAll"
  },

  initialize: function() {
    // this.model.on('destroy', this.remove, this);
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.html(this.template({project: this.model}));
    return this;
  },

  initCKEditor: function() {
    if (CKEDITOR.instances['project_description']) { delete CKEDITOR.instances['project_description'] };
    if (CKEDITOR.instances['project_description']) { CKEDITOR.instances['project_description'].destroy(); }

    this.$el.find('#project_description').ckeditor({
      toolbar: 'Basic',
      toolbar_Basic: [
        ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Font', 'FontSize']
      ]
    })
  },

  selectAll: function(e){
    this.$(".viewers input").prop('checked', this.$('.all input').prop('checked'));
  },

  submit: function(event) {
    event.preventDefault();
    if(this.$("input[type='checkbox']:checked").length == 0){
      alert('Please select at least one viewer to notify');
      return false
    }

    $.ajaxSetup({ cache: false });
    $.ajax(this.url(), {
        type: 'POST',
        data: this.serialize(),
        processData: true,
        success: this.submitSuccess.bind(this),
        error: this.submitError.bind(this)
    });
    this.$el.find(".save").attr('disabled', true);
  },

  submitSuccess: function(response, event) {  
    var responseText
    if(response.responseText){
      responseText = response.responseText
    }else{
      responseText = response
    }
    
    alert(responseText);
    this.remove()
  },

  submitError: function(response, event){
    var err_cont = this.$('.error-explanation')
    if(response.status == 422){
      err_cont.html('')
      var errors = $.parseJSON(response.responseText).errors
      for(field in errors){
        err_cont.append('<span class="field">'+field+'</span> '+errors[field]+'.<br/>')
      }
    }else if(response.status >= 200 && response.status < 300){
      // not realy an error
      this.submitSuccess(event,response)
    }else{
      err_cont.html(VSR.Config.generircErrorMSG)
    }
  },

  url: function() {
    return '/projects/' + this.model.id + "/publish";
  },

  cancel: function(event){
    event.preventDefault()
    this.remove()
  },

  serialize: function() {
    var viewers = [];
    this.$("input[type='checkbox']:checked").each(function(x, e){viewers.push(e.value)});
    return {
      description: this.$("textarea[name='project[description]']").val(),
      viewers: viewers
    };
  }
});
