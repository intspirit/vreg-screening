VSR.Editors.ToolbarItem = Backbone.View.extend({
  tagName: "div",
  className: "drag",

  template: JST["backbone/templates/view_items/toolbar_item"],

  // Future events happening on editor like drag'n'drop, etc.
  events: {
  },

  initialize: function(itemType,subItemType) {
    this.itemType    = itemType
    this.subItemType = subItemType
    this.toolbarClass = subItemType ? subItemType : itemType
  },

  render: function() {
    //this.$el.html(this.template);
    
    this.$el.html('<div class="new-'+this.toolbarClass+'-bg bg'+'"></div>')
    this.$el.addClass( 'new-'+this.toolbarClass )
    this.$el.attr( 'data-item-type',this.itemType )
    this.$el.attr( 'data-item-sub-type',this.subItemType )
    this.$el.draggable({
      revert: VSR.Views.Drag.revert,
      scroll: false,
      iframeFix:true,
      drag: VSR.Views.Drag.onDrag,
      start: VSR.Views.Drag.dragStart,
      stop: VSR.Views.Drag.dragStop
    });
    return this;
  },

  clicked: function() {
  },

  // Add one new view item to the editor.
  addOne: function(viewItem) {
    var view = new VSR.Views.ViewItemView({model: viewItem});
    this.$("#viewItem-list").append(view.render().el);
  },

  // Add all view items in ViewItem collection to the editor
  addAll: function() {
    VSR.Collections.ViewItems.each(this.addOne);
  },

  // Generate the attributes for a new View item.
  newAttributes: function() {
    return {
      html: this.input.val(),
      position: 0
    }
  },

  // Hitting enter in the input field will create new viewItem
  createOnEnter: function(e) {
    if (e.keyCode != 13) return;
    VSR.Collections.ViewItems.create(this.newAttributes());
    this.input.val('');
  }
});