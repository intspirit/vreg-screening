VSR.Editors.Toolbar = Backbone.View.extend({
    template: JST["backbone/templates/view_items/toolbar"],

    // Future events happening on editor like drag'n'drop, etc.
    events: {
      "keypress #new-viewItem": "createOnEnter"
    },

    initialize: function() {
    },

    render: function() {
      $(this.el).html(this.template());
      
      var item;
      //_.each(["box-whole","box2","box2-1","box1-2","box3"], function(drag){
      _.each(["box"], function(drag){
        // item = new VSR.Editors.ToolbarBoxItem('box',drag);
        item = new VSR.Editors.ToolbarItem(drag);
        item.render();
        this.$('#grids').append(item.el);
      }, this);
      
      _.each(VSR.Config.FillItemTypes, function(drag){
        item = new VSR.Editors.ToolbarItem(drag);
        item.render();
        this.$('#fillers').append(item.el);
      }, this);
      this.$el.append('<div id="data"></div>');
      return this;
    },

    publish: function(e) {
      e.preventDefault();
      // 1 preare a model at end of drop pannel
      // 1 show form
    },


    // Add one new view item to the editor.
    addOne: function(viewItem) {
      var view = new VSR.Views.ViewItemView({model: viewItem});
      this.$("#viewItem-list").append(view.render().el);
    },

    // Add all view items in ViewItem collection to the editor
    addAll: function() {
      VSR.Collections.ViewItems.each(this.addOne);
    },

    // Generate the attributes for a new View item.
    newAttributes: function() {
      return {
        html: this.input.val(),
        position: 0
      }
    },

    // Hitting enter in the input field will create new viewItem
    createOnEnter: function(e) {
      if (e.keyCode != 13) return;
      VSR.Collections.ViewItems.create(this.newAttributes());
      this.input.val('');
    }
  });