VSR.Editors.Publish = Backbone.View.extend({
    el: "#editor-header",

    // Future events happening on editor like drag'n'drop, etc.
    events: {
      "click .publish": "clicked"
    },

    initialize: function() {
    },

    // Re-rendering only parts relevant to the editor. The rest of the app doesn't change
    render: function() {
      return this;
    },

    clicked: function() {
      var that = this;
      this.model.viewers.fetch().then(function(){
        that.publishForm = new VSR.Views.PublishFormView({model: that.model});
        VSR.Config.Foreground.renderView(that.publishForm);
      }, function(){
      });

    }
});
