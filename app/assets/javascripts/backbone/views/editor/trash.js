VSR.Editors.TrashView = Backbone.View.extend({
  tagName: "div",
  id: 'trash',

  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/trash"],

  // The DOM events specific to an item
  events: {
    drop: "dropped"
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    // this.model.on('change' , this.render, this);
  },

  initializeDropable: function() {
    $(this.el).droppable({
      accept: VSR.Views.BoxViewConfig.beforeAfterDropSelector + ',' + VSR.Views.BoxViewConfig.insideDropSelector,
      greedy: true,
      activeClass: "can-drop",
      hoverClass: "hovering",
      tolerance: 'pointer'
    });
  },

  dropped: function(event, ui) {
    var id = $(ui.draggable).attr('data-id')
    if(id){
      var model = this.collection.get(id)
      var parent = model.parent()
      model.collection.removeWithSubtree(model)
      model.destroy({silent:true})
      parent.trigger('change')
    }else{
      //console.log('ID is missing data-id is missing.')
    }
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.html(this.template());
    this.initializeDropable();
    return this;
  }
});