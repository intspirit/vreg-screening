VSR.Views.ContactGroupsIndex = Backbone.View.extend({
  template: JST['backbone/templates/contact_groups/index'],
  
  initialize:function(){
    this.collection.on('reset', this.render, this)
    this.collection.on('add', this.render, this)
    this.collection.on('change', this.render, this)
    this.collection.on('remove', this.render, this)
    this.collection.on('destroy', this.render, this)
  },
  
  events:{
    'keyup .search' : 'filter',
    'dragstart'     : 'dragStart',
    'dragstop'      : 'dragStop',
    'change'        : 'checkboxClicked'
  },

  checkboxClicked: function(e) {
    e.preventDefault();

  },
  
  dragStart:function(){
    this.$('ul').addClass('dragging-child')
  },
  
  dragStop:function(){
    this.$('ul').removeClass('dragging-child')
  },
  
  render:function(){
    this.$el.html('<div class="head"><h3>Groups</h3><div>')
    if(this.small && this.collection.length <= 9){
      this.$('.head').append('<small>'+this.small+'</small>')
    }
    if(this.collection.length == 0) {
      this.$el.append('<ul class="contact_groups_list"><li><div style="text-align:center; ">Group list is empty. Add new new.</div><li></ul>')
    } else {
      if (this.collection.length > 9 ){
        this.$(".head").append('<input class="search" type="text" placeholder="Search groups">')
      }
      this.$el.append('<ul class="contact_groups_list"><li class="selected"><div style="text-align:center; ">All</div></li></ul>')
      this.collection.each( function(contactGroup){
        this.appendContactGroup(contactGroup)
      }.bind(this)) 
    }
    
    return this
  },

  contactGroupSelection: function() {
    var response = $('<select>');

    if (this.collection.length != 0) {
      this.collection.each(function(contactGroup) {
        var el = $('<option>').html(contactGroup.get('name')).attr('value', contactGroup.get('id'));
        response.append(el);
      }.bind(this));
    }

    return response;
  },
  
  appendContactGroup:function(contactGroup){
    var view = new VSR.Views.ContactGroup({model: contactGroup})
    this.$('.contact_groups_list').append(view.render().el)
  },
  
  filter:function(){
    this.regex=new RegExp('.*' + this.$('.search').val() + '.*', 'i')
    this.$('.contact_groups_list li').each(function(i, li){
      li = $(li)
      if(this.regex.test(li.attr('data-search'))){
        li.show()
      }else{
        li.hide()
      }
    }.bind(this))
  }
})