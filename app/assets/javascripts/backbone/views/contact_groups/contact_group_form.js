VSR.Views.ContactGroupForm = Backbone.View.extend({
  template: JST['backbone/templates/contact_groups/contact_group_form'],
  className:'foreground',
  
  events:{
    'submit form'         : 'createContactGroup',
    'click .cancel-form'  : 'cancel'
  },
  
  render: function(){
    this.$el.html(this.template())
    return this
  },
  
  cancel: function(event){
    event.preventDefault()
    this.remove()
  },
  
  createContactGroup: function(event){
    event.preventDefault();
    this.model.save(
      {
        name : this.$('input[name="contact_group[name]"]').val()
      },
      {
        wait: true,
        success: function(){
          this.remove()
        }.bind(this),
        error:function(entry, response){
          if(response.status == 422){
            errors = $.parseJSON(response.responseText).errors
            this.$('.errors').html('Name ' + errors.name + '.')
          }
        }
      }
    )
  }
})