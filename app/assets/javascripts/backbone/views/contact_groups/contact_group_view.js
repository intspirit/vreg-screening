VSR.Views.ContactGroup = Backbone.View.extend({
  template: JST['backbone/templates/contact_groups/contact_group_li'],
  tagName:'li',
  className:'contact-group',
    
  addContact: function(event, ui) {
    var contact = VSR.Data.contacts.get($(ui.draggable).attr('data-id'));
    contact.set('group_id', this.model.id);
    
    if (this.model.collection.get(contact.id) == undefined) {
      this.model.collection.create(contact);
    }
  },
  
  render:function(){
    this.$el.attr('data-id', this.model.id)
    this.$el.attr('data-type', 'ContactGroup');
    this.$el.html(this.template({contactGroup: this.model}))
    this.$el.attr('data-search','' + this.model.get('name'))
    return this
  }
})