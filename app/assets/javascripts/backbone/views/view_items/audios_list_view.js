VSR.Views.AudiosList = Backbone.View.extend({
  tagName: "div",
  className: 'audios-list',

  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/audios_list"],
  soundcloud_template: JST["backbone/templates/view_items/embed_code/soundcloud"],

  // The DOM events specific to an item
  events: {
    'click .audio':'play'
  },
  
  // change current video and rerender
  play:function(event){
    event.preventDefault()
    this.current_audio = this.model.collection.get($(event.currentTarget).attr('data-id'))
    if(VSR.Config.mode == 'edit'){
     this.model.parent().trigger('change') 
    }else{
      this.render()
    }
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , function(){this.model.parent().trigger('change')}, this);
  },
  
  initializeDropables: function() {
    this.$('.audios > .audio').droppable({
      greedy: true,
      accept: '.new-audio,.audio',
      activeClass: "can-drop",
      hoverClass: "hovering",
      tolerance: 'pointer',
      drop: _.bind(this.addAudio,this)
    });
  },
  
  addAudio: function(event, ui){
    var position = 0
    var dropReceiver = $(event.target).attr('data-id')
    
    if(dropReceiver){
      dropReceiver = this.model.collection.get(dropReceiver)
      if(dropReceiver.get('item_type') == 'audio' )
        position = dropReceiver.get('position') 
    }
    
    if($(ui.draggable).attr('data-id')){
      // EXISTING 
      var existing = this.model.collection.get($(ui.draggable).attr('data-id'))
      var oldParent = existing.parent()
      var newParent = this.model
      newParent.insertChildAt(existing,position).save({},{silent:true})
      newParent.trigger('change')
      if(oldParent.id != newParent.id){
        oldParent.trigger('change')  
      }
    }else{
      // NEW
      var new_model = this.model.buildChild({position:position})
      var view = new VSR.Views.AudioForm({model:new_model})
      VSR.Config.Foreground.renderView(view) 
    }
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    var children = this.model.children()
    this.$el.html('');
    this.$el.attr('data-id',this.model.id)
    if (VSR.Config.mode == 'edit' && children.length==0) {
      this.$el.append('<div class="initial audio">Place a Audio here.<br/><sub>(This Placeholder is Visible Only in Editing Mode)</sub></div>')
    }
    
    // render player for current video
    if(this.current_audio){
      // IS current_video still in my children ? if not then is not my current_video any more 
      if( ! _.any(children,function(child){return this.current.id == child.id },{current:this.current_audio}) ){
        this.current_audio = children[0]
      }
    }else{
      this.current_audio = children[0]
    }

    if(this.current_audio){
      this.$el.html(this.template({model: this.current_audio}));
      // this.current_audio = the video to appear into player
      
      this.$('h2').html(this.current_audio.get('title'))
      if(this.current_audio.get('soundcloud_id')){
        var view = new VSR.Views.Soundcloud({model:this.current_audio,el:this.$('.audio-player-container')[0]})
        view.render()
      }
    }else{
      this.$el.html('')
    }

    // audios
    if (VSR.Config.mode == 'edit' || children.length > 1)
    if (children.length >= 1){
      this.$el.append('<div class="audios"></div>')
      // videos thumbs for changing videos
      var child_view
      _.each( children,function(item){
        
        child_view = new VSR.Views.Audio({model:item})
        //child_view.afterRender = VSR.Views.BoxViewConfig.wrapWithDropAreas
        child_view.render()
        //is in the player ?
        if(item.id == this.current_audio.id){
          child_view.$el.addClass('active')
        }else{
          child_view.$el.removeClass('active')
        }
        this.$('.audios').append(child_view.el)
      },this)
    }else{
      this.$el.append('<div class="audios"><div class="initial audio">Audio player - place an audio here.<br/><sub>(This Placeholder is Visible Only in Editing Mode)</sub></div></div>')
    }
      
    if (VSR.Config.mode == 'edit') {
      this.initializeDropables()
      VSR.Views.addElementControls(this.$el,{edit:false})
    }
    if(this.afterRender) this.afterRender(this)
    return this;
  }  
});