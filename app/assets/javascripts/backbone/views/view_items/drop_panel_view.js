VSR.Views.DropPanel = Backbone.View.extend({
  tagName: "div",
  id: "dropzone-list",
  className: "container",

  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/dropzone_list"],

  // The DOM events specific to an item
  events: {
    //'drop .initial': "dropped"
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
    this.model.on('sync'   , this.render, this);
  },

  initializeDropable: function() {
    this.$('.initial').droppable({
      accept: '.new-box,.new-box-whole,.new-box2,.new-box2-1,.new-box1-2,.new-box3',
      greedy: true,
      activeClass: "can-drop",
      hoverClass: "hovering",
      drop: _.bind(this.dropped,this)
    });   
  },

  dropped: function(event, ui) {
    var newParent = this.model
    var newPosition = 0
    var droppedId = $(ui.draggable).attr('data-id')
    var droppedType = $(ui.draggable).attr('data-item-type')
    var droppedSubType = $(ui.draggable).attr('data-item-sub-type')
    
    var attrs = {}
    attrs.item_type = droppedType
    var viewItem = newParent.buildChild({item_type:'box',sub_type:'box2',position:newPosition});
    var view = new VSR.Views.BoxForm({model: viewItem});
    VSR.Config.Foreground.renderView(view);
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.css( 'min-height' , this.$el.height()  ) // prevent element rerender to cause flickering
    
    //this.$el.html(this.template({model:this.model}));
    if(this.model.children().length == 0){
      //console.log('NO children')
      this.$el.html('<div class="initial">Please choose a Content Box from the menu and place it here to edit.</div>')
      this.initializeDropable(); 
    }else{
      this.$el.html('')
    }
    
    var child_view
    _.each(this.model.children(),function(item){
      child_view = VSR.Views.ViewItem.getView(item.get('item_type'));      
      child_view = new child_view({model:item})
      this.$el.append(child_view.el)
      child_view.render()
    },this);
    
    this.$el.css( 'min-height' , ''  )

    return this;
  }
});