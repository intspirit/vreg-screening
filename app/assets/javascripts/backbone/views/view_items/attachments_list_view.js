VSR.Views.AttachmentsList = Backbone.View.extend({
  tagName: "div",
  className: 'attachments-list',

  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/attachments_list"],
  attachment_template: JST["backbone/templates/view_items/attachment"],

  // The DOM events specific to an item
  events: {
    //'click .add-file':'newChild'
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , function(){this.model.parent().trigger('change')}, this);
  },
  
  initializeDropables: function() {
    this.$('> .attachment').droppable({
      greedy: true,
      accept: '.new-attachment,.attachment',
      activeClass: "can-drop",
      hoverClass: "hovering",
      tolerance: 'pointer',
      drop: _.bind(this.addAttachment,this)
    });
  },
  
  addAttachment: function(event, ui){
    var position = 0
    var dropReceiver = $(event.target).attr('data-id')
    
    if(dropReceiver){
      dropReceiver = this.model.collection.get(dropReceiver)
      if(dropReceiver.get('item_type') == 'attachment')
        position = dropReceiver.get('position') 
    }
    
    if($(ui.draggable).attr('data-id')){
      // EXISTING 
      var existing = this.model.collection.get($(ui.draggable).attr('data-id'))
      var oldParent = existing.parent()
      var newParent = this.model
      newParent.insertChildAt(existing,position).save({},{silent:true})
      newParent.trigger('change')
      if(oldParent.id != newParent.id){
        oldParent.trigger('change')  
      }
    }else{
      // NEW
      new_model = this.model.buildChild({position:position})
      var view = new VSR.Views.AttachmentForm({model:new_model})
      VSR.Config.Foreground.renderView(view) 
    }
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    var children = this.model.children()
    this.$el.html('');
    this.$el.attr('data-id',this.model.id)
    if (VSR.Config.mode == 'edit' && children.length==0) {
      this.$el.append('<div class="initial attachment">Place an attachment here.<br/><sub>(This Placeholder is Visible Only in Editing Mode)</sub></div>')
    }
    
    
    var child_view
    _.each( this.model.children(),function(item){
        child_view = VSR.Views.ViewItem.getView(item.get('item_type'));
        child_view = new child_view({model:item, parent: this.model})
        child_view.render()
        
        this.$el.append(child_view.el)
      },this)
    if (VSR.Config.mode == 'edit') {
      this.initializeDropables()
      VSR.Views.addElementControls(this.$el,{edit:false})
    }
    if(this.afterRender) this.afterRender(this)
    return this;
  }  
});