VSR.Views.Audio = Backbone.View.extend({
  tagName: "a",
  className: 'audio',

  template: JST["backbone/templates/view_items/audio"],

  // The DOM events specific to an item
  events: {
    'click .edit-button': 'edit'
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
  },
  
  // Edit Text Content
  edit: function(e) {
    if(VSR.Config.mode == 'edit') {
      e.preventDefault();
      var view = new VSR.Views.AudioForm({model: this.model});
      VSR.Config.Foreground.renderView(view);
    }
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.attr('data-id',this.model.id)
    if (this.model.get('soundcloud_id')) {
      this.$el.html(this.template({model: this.model}));
    }else if(VSR.Config.mode == 'edit'){
      this.$el.html('<div class="initial">Empty Audio</div>')
    }

    if(VSR.Config.mode == 'edit')
      VSR.Views.addElementControls(this.$el)
      
    return this;
  }
});