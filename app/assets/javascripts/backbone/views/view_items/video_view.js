VSR.Views.Video = Backbone.View.extend({
  tagName: "div",
  className: 'video-thumb',

  template: JST["backbone/templates/view_items/video"],

  // The DOM events specific to an item
  events: {
    'click .edit-button': 'edit'
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
  },
  
  // Edit Text Content
  edit: function(event) {
    event.preventDefault()
    var view = new VSR.Views.VideoForm({model: this.model});
    VSR.Config.Foreground.renderView(view);
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.attr('data-id',this.model.id)
    this.$el.addClass('video')
    if (this.model.get('image_url')) {
      this.$el.html(this.template({model: this.model}));
    }else if(VSR.Config.mode == 'edit'){
      this.$el.html('<div class="initial">Empty Video Box</div>')
    }

    if(VSR.Config.mode == 'edit') {
      VSR.Views.addElementControls(this.$el)
    }
    
    if(this.afterRender) this.afterRender(this)
    return this;
  }
});
