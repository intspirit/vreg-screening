VSR.Views.VimeoVideo = Backbone.View.extend({

  template: JST["backbone/templates/view_items/embed_code/vimeo"],

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
    this.initUserAction()
  },
  
  initUserAction : function(){
    this.previous_second = -1
    this.play_action = new VSR.Models.UserAction({
        view_item_id:this.model.id,
        project_id:VSR.Routers.view_items_router.project.id,
        action_type:'play_video',
        description: this.model.get('title')+'</br>'
    })
  },
  
  iframeId: function (){
    return "vimeo"+this.cid
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {
    // render iframe
    this.$el.html(this.template({iframe_id:this.iframeId(),video_id:this.model.get('vimeo_id')}))

    Froogaloop(this.$('iframe')[0]).addEvent('ready', _.bind(this.initPlayerAPI,this));
    return this;
  },
  
  initPlayerAPI: function(playerID){  
    // Add event listerns
    // http://vimeo.com/api/docs/player-js#events
    Froogaloop(this.$('iframe')[0]).addEvent('playProgress', _.bind(this.playing,this));
  },
  
  playing: function(data){
    // data = {
    //  duration:"241.209"
    //  percent:"0.004"
    //  seconds:"0.952"
    // }
    var currentSecond = Math.round(parseFloat(data.seconds))
    // going back ? - create a new user_action model
    if(this.previous_second > currentSecond){
      this.initUserAction()
    }
    this.previous_second = currentSecond
    this.play_action.mark_as_played(currentSecond)
  },
  
  onPlayerReady:function(){
    this.play_action = new VSR.Models.UserAction({
      view_item_id:this.model.id,
      project_id:VSR.Routers.vite_items_router.project.id,
      action_type:'play_video',
      description: this.model.get('title')+'</br>'
    })
  }
  
});