VSR.Views.BoxViewConfig = {}
VSR.Views.BoxViewConfig.subTypeChildren = {
    'box1':   ['box-whole'],
    'box2':   ['box-half','box-half'],
    'box2-1': ['box-two-thirds','box-third'],
    'box1-2': ['box-third','box-two-thirds'],
    'box3':   ['box-third','box-third','box-third']
}
VSR.Views.BoxViewConfig.topLevelSubTypes = ['box1', 'box2', 'box2-1', 'box1-2','box3']
VSR.Views.BoxViewConfig.lowLevelSubTypes = ['box-whole', 'box-half', 'box-two-thirds', 'box-third']
VSR.Views.BoxViewConfig.cssGridClasses = {
  'box-whole':'twelvecol', 'box-half':'sixcol', 'box-two-thirds':'eightcol', 'box-third':'fourcol'
}

// this selecteor determines what children types can be droped inside this one
// this code basically turns ['type'] to '.type,.new-type' for VSR.Config.FillItemTypes
VSR.Views.BoxViewConfig.insideDropSelector = _.map(VSR.Config.FillItemTypes,function(type){return [type,'new-'+type]})
VSR.Views.BoxViewConfig.insideDropSelector = _.flatten(VSR.Views.BoxViewConfig.insideDropSelector)
// add wrappers also
_.each(VSR.Config.FillItemTypesWrappers, function(value){VSR.Views.BoxViewConfig.insideDropSelector[VSR.Views.BoxViewConfig.insideDropSelector.length] = value})
VSR.Views.BoxViewConfig.insideDropSelector = '.'+VSR.Views.BoxViewConfig.insideDropSelector.join(',.')

// this selecteor determines what children types can be inserted before and after
// this code basically turns ['type'] to '.type,.new-type' for VSR.Views.BoxViewConfig.topLevelSubTypes
VSR.Views.BoxViewConfig.beforeAfterDropSelector = _.map(VSR.Views.BoxViewConfig.topLevelSubTypes,function(type){return [type,'new-'+type]})
VSR.Views.BoxViewConfig.beforeAfterDropSelector = _.flatten(VSR.Views.BoxViewConfig.beforeAfterDropSelector)
VSR.Views.BoxViewConfig.beforeAfterDropSelector.push('new-box')
VSR.Views.BoxViewConfig.beforeAfterDropSelector = '.'+VSR.Views.BoxViewConfig.beforeAfterDropSelector.join(',.')


VSR.Views.BoxViewConfig.addBox = function(parent,subType,position){
    position = position ? position : 0;
    var newItem = parent.buildChild({item_type: 'box'});
    
    parent.insertChildAt(newItem,position) //this makes the other children move
    
    newItem.set('sub_type',subType)
    newItem.save({},{
      wait: true,
      success:function(model){
        model.collection.add(model)
        // if we have a subtype children have to be created
        if(model.get('sub_type')) {
          var childSubTypes = VSR.Views.BoxViewConfig.subTypeChildren[model.get('sub_type')]
          if(childSubTypes){
            for(var i=0; i < childSubTypes.length; i++){            
              var child = model.buildChild({item_type: 'box',sub_type:childSubTypes[i], position:i});
              model.collection.add(child)
              child.save()
            } 
          }
        }
        model.parent().trigger('change')
      }
    })   
  }

VSR.Views.BoxViewConfig.wrapWithDropAreas = function(view){
  //if child is a fillable type here whe inject drop areas
  //console.log('Wrap with drop areas '+view.model.id)
  if(view.$el.is(VSR.Views.BoxViewConfig.insideDropSelector)) {
    if(view.model.get('position')==0)
      view.$el.prepend('<div class="drop-before"></div>')
      view.$el.append('<div class="clearfix"></div><div class="drop-after"></div>')
    }
}
  
VSR.Views.Box = Backbone.View.extend({
  tagName: "div",
  className: 'box',

  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/box"],
  
  events:{
    'click .box-edit-button' : 'edit'
  },

  initialize: function() {
    // this.model.on('change' , this.render, this);
    this.model.on('change',this.render,this)
  },
  
  edit: function() {
    var view = new VSR.Views.BoxForm({model: this.model});
    VSR.Config.Foreground.renderView(view);
  },
  
  isTopLevelBox: function() {
    return _.any(VSR.Views.BoxViewConfig.topLevelSubTypes,function(className){
      return this.model.get('sub_type') == className
    },this)
  },
  
  isLowLevelBox: function() {
    return _.any(VSR.Views.BoxViewConfig.lowLevelSubTypes,function(className){
      return this.model.get('sub_type') == className
    },this)
  },
  
  initializeDropables: function(children) {
    // BOX droppables
    this.$('> .drop-before',this.$el).droppable({
      greedy: true,
      accept: VSR.Views.BoxViewConfig.beforeAfterDropSelector,
      activeClass: "can-drop",
      hoverClass: "hovering",
      tolerance: 'pointer',
      drop: _.bind(this.droppedBefore,this)
    });

    this.$('> .drop-after',this.$el).droppable({
      greedy: true,
      accept: VSR.Views.BoxViewConfig.beforeAfterDropSelector,
      activeClass: "can-drop",
      hoverClass: "hovering",
      tolerance: 'pointer',
      drop: _.bind(this.droppedAfter,this)
    });

    //FILL DROPPABLES
    if( children.length == 0 && this.isLowLevelBox() ){
      this.$el.droppable({
        greedy: true,
        accept: VSR.Views.BoxViewConfig.insideDropSelector,
        activeClass: "can-drop",
        hoverClass: "hovering",
        tolerance: 'pointer',
        drop: _.bind(this.droppedInside,this)
      }); 
    }else{
      this.$el.droppable( "destroy" )
    }

    this.$('* .drop-before',this.$el).droppable({
      greedy: true,
      accept: VSR.Views.BoxViewConfig.insideDropSelector,
      activeClass: "can-drop",
      hoverClass: "hovering",
      tolerance: 'pointer',
      drop: _.bind(this.droppedBefore,this)
    });

    this.$('* .drop-after',this.$el).droppable({
      greedy: true,
      accept: VSR.Views.BoxViewConfig.insideDropSelector,
      activeClass: "can-drop",
      hoverClass: "hovering",
      tolerance: 'pointer',
      drop: _.bind(this.droppedAfter,this)
    });
  },

  droppedBefore: function(event, ui) {
    this.addSibbling(event, ui,'before')
  },
  
  droppedAfter: function(event, ui) {
    this.addSibbling(event, ui,'after')
  },
  
  droppedInside: function(event, ui) {
    this.addSibbling(event, ui,'inside')
  },
  
  addSibbling:function(event, ui, position){
    var droppedId = $(ui.draggable).attr('data-id');
    var droppedType = $(ui.draggable).attr('data-item-type');
    var droppedSubType = $(ui.draggable).attr('data-item-sub-type');
    // sibblings of boxes can be only boxes!
    var dropReceiver = $(event.target.parentElement).attr('data-id')
    dropReceiver = this.model.collection.get(dropReceiver)

    // new parent
    var newParent
    // new position
    var newPosition

    switch(position) {
    case 'after':
    if (droppedId){
      var droppedElement = this.model.collection.get(droppedId);
      if (droppedElement.get('position') < dropReceiver.get('position')) {
        newPosition = dropReceiver.get('position');
      } else {
        newPosition = dropReceiver.get('position') + 1;
      }
    } else {
        newPosition = dropReceiver.get('position') + 1;
    }

      newParent = dropReceiver.parent()
      break;
    case 'before':
      newPosition = dropReceiver.get('position')
      newParent = dropReceiver.parent()
      break;
    case 'inside':
      newPosition = 0
      newParent = this.model
    }

    
    
    // MOVING AN EXISTING ITEM ?
    if(droppedId){
      var movingModel = this.model.collection.get(droppedId)
      var oldParent = movingModel.parent()
      
      // function to inject element
      var insertChild = _.bind(function(parent,position){
        parent.insertChildAt(this.movingModel,position).save({},{silent:true})
        parent.trigger('change')
        if(this.oldParent.id != parent.id){
          this.oldParent.trigger('change')  
        } 
      },{oldParent:oldParent,movingModel:movingModel})

      
      // maybe this item_type cant be inserted directly
      // it may require a wrapping type
      if(VSR.Config.FillItemTypesWrappers[movingModel.get('item_type')]){
        //build wrapper
        var wrapper = newParent.buildChild({
          item_type:VSR.Config.FillItemTypesWrappers[movingModel.get('item_type')],
          position:newPosition
        })
        //save wrapper
        wrapper.save({},{
         wait:true,
         success:_.bind(function(model){
          //insert target into wrapper
          model.collection.add(model)
          model.parent().trigger('change')
          this.insertChild(model,0)     
         },{insertChild:insertChild})
        })
      }else{
        insertChild(newParent,newPosition)
      }
    }else{
      
      // CREATE A NEW ITEM
    
      var attrs = {}
      attrs.item_type = droppedType
      if(droppedSubType)
        attrs.sub_type  = droppedSubType
      
      // All but BOXes
      if(attrs.item_type!='box'){
          
          var createNewAt = _.bind(function(parent,position){
          attrs.position = position
          var newItem = parent.buildChild(attrs)
          
          var formView = VSR.Views.ViewItem.getView(newItem.get('item_type'),true)


          formView = new formView({model:newItem})
          VSR.Config.Foreground.renderView(formView);
          if(event.fileDropped){
            formView.setFile(event.fileDropped)
          }
          
          //parent.insertChildAt(newItem,position)
          //newItem.save({},{
          //  wait: true,
          //  success:_.bind(function(model){
          //    model.collection.add(model)
          //    model.parent().trigger('change')
          //  },this)
          //})
        },{attrs:attrs})
        
        // maybe this item_type cant be inserted directly
        // it may require a wrapping type
        if(VSR.Config.FillItemTypesWrappers[droppedType]){
          //build wrapper
          var wrapper = newParent.buildChild({
            item_type:VSR.Config.FillItemTypesWrappers[droppedType],
            position:newPosition
          })
          //save wrapper
          wrapper.save({},{
           wait:true,
           success:_.bind(function(model){
            model.collection.add(model)
            model.parent().trigger('change')
            //insert target into wrapper
            this.createNewAt(model,0)          
           },{createNewAt:createNewAt})
          })
        }else{
          createNewAt(newParent,newPosition)
        }
      }else{
        
        var viewItem = newParent.buildChild({item_type:'box',sub_type:'box2',position:newPosition});
        var view = new VSR.Views.BoxForm({model: viewItem});
        VSR.Config.Foreground.renderView(view);
        
        //VSR.Views.BoxViewConfig.addBox(newParent, attrs.sub_type ,newPosition) 
      } 
    }
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    var children = this.model.children()
    
    var sub_type = this.model.get('sub_type')
    this.$el.attr('data-id',this.model.id)
    this.$el.addClass(sub_type)

    this.$el.html('');
    
    // drag handle and drop before
    
    if(this.isTopLevelBox()){
      this.$el.addClass('row')
      
      if(VSR.Config.mode == 'edit'){
        this.$el.html('<div class="drag-handle"></div>');
        //only top level box can receive before after drops
        if(this.model.get('position') <= 0 ){
          this.$el.append('<div class="drop-before"></div>'); 
        }
      }
    }else{
      this.$el.addClass(VSR.Views.BoxViewConfig.cssGridClasses[this.model.get('sub_type')])
    }
    
    if(children.length == 0){
      this.$el.append('<div class="initial-box">This content box is empty. Please choose an element from the menu and place it here to edit.</div>')
    } else {
      
      // children
      var child_view
      var last_view = false
      _.each(children,function(item,index){
        if( item.get('sub_type') != 'disabled' ){
          child_view = VSR.Views.ViewItem.getView(item.get('item_type'));
          child_view = new child_view({model:item, parent: this.model})
          if(VSR.Config.mode == 'edit'){
            child_view.afterRender = VSR.Views.BoxViewConfig.wrapWithDropAreas  
          }          
          this.$el.append(child_view.el)
          child_view.render()
          if(item.get('item_type')=='box'){
            last_view = child_view  
          }
        }
      },this)
      
      if(last_view){
        last_view.$el.addClass('last')
      }
    }
    
    //only top level box can receive before after drops
    if(this.isTopLevelBox() && VSR.Config.mode == 'edit'){
      this.$el.append('<div class="clearfix"></div><div class="drop-after"></div>')
    }
    this.initializeDropables(children);
    
    if(this.isTopLevelBox() && VSR.Config.mode == 'edit'){
      VSR.Views.addElementControls(this.$el,{edit_class:'box-edit-button edit-button'})
      // this.$el.css('height', this.$el.css('height'))
    }

    return this;
  }
});
