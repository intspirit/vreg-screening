VSR.Views.Text = Backbone.View.extend({
  tagName: "div",
  className: 'text',

  template: JST["backbone/templates/view_items/text"],

  // The DOM events specific to an item
  events: {
    'click .edit-button': 'edit'
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
  },

  // Edit Text Content
  edit: function(e) {
    if(VSR.Config.mode == 'edit') {
      e.preventDefault();
      var view = new VSR.Views.TextForm({model: this.model});
      VSR.Config.Foreground.renderView(view);
    }
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.attr('data-id',this.model.id)
    this.$el.html('');
    if (this.model.get('html') != undefined && this.model.get('html') != '') {
      this.$el.append(this.model.get('html'));
    } else if(VSR.Config.mode == 'edit'){
      this.$el.append('<div class="initial">Empty Text Box - Double click to Edit Content</div>')
    }

    if(VSR.Config.mode == 'edit')
      VSR.Views.addElementControls(this.$el)
    
    if(this.afterRender) this.afterRender(this)
    return this;
  }
});