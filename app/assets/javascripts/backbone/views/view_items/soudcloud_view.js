VSR.Views.Soundcloud = Backbone.View.extend({

  template: JST["backbone/templates/view_items/embed_code/soundcloud"],

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
    this.initUserAction()
  },
  
  initUserAction : function(){
    this.previous_second = -1
    this.play_action = new VSR.Models.UserAction({
        view_item_id:this.model.id,
        project_id:VSR.Routers.view_items_router.project.id,
        action_type:'play_audio',
        description: this.model.get('title')+'</br>'
    })
  },
  
  iframeId: function (){
    return "sc"+this.cid
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {
    // render iframe
    this.$el.html( this.template({iframe_id:this.iframeId(),track_id:this.model.get('soundcloud_id')}) )
    this.$('iframe')[0].onload = _.bind(function(){
      this.player = SC.Widget(this.$('iframe')[0])
      this.player.bind(SC.Widget.Events.READY, _.bind(this.initPlayerAPI,this));    
    },this)
    
  
    return this;
  },
  
  initPlayerAPI: function(){
    this.player.bind(SC.Widget.Events.PLAY_PROGRESS, _.bind(this.playing,this));
  },
  
  playing: function(data){
    // data = {
    //  relativePosition — relative current position of the current sound, in the range of [0,1].
    //  loadProgress     — the current value of the loading progress, in the range of [0,1].
    //  currentPosition  — the position of the current sound (in milliseconds).
    // }
    var currentSecond = Math.round(data.currentPosition/1000.00)

    // going back ? - create a new user_action model
    if(this.previous_second > currentSecond){
      this.initUserAction()
    }
    this.previous_second = currentSecond
    this.play_action.mark_as_played(currentSecond)
  }
});