VSR.Views.TextForm = Backbone.View.extend({
  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/forms/text_form"],
  className: 'foreground',
  // The DOM events specific to an item
  events: {
    'click .cancel-form':'cancel',
    'click .save':'submit'
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change', this.remove, this);
    this.model.set({item_type:'text'},{silent:true})
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.html(this.template({view_item:this.model}));
    return this;
  },
  
  cancel: function(event){
    event.preventDefault();
    this.remove();
  },
  
  submit: function(e) {
    if(this.ckeditor)
      this.ckeditor.updateElement();

    _.bind(VSR.Views.Forms.genericSubmit,this)(e)
  },
  
  initCKEditor: function(){
    if (CKEDITOR.instances['ckeditor']) { delete CKEDITOR.instances['ckeditor'] };
    if (CKEDITOR.instances['ckeditor']) { CKEDITOR.instances['ckeditor'].destroy(); }

    var default_text = this.model.get("html") || "";
    this.$el.find('#ckeditor').ckeditor({
      toolbar: 'Basic',
      toolbar_Basic: [
        ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Font', 'FontSize', '-', 'Link', 'Unlink','-','About']
      ]
    })

    this.$el.find('#ckeditor').val(default_text);

  },

  serialize: function() {
    return {html :this.$("textarea[name='view_item[html]']").val()}
  } 
});