VSR.Views.VideoForm = Backbone.View.extend({
  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/forms/video_form"],
  className: 'foreground',
  // The DOM events specific to an item
  events: {
    'click .cancel-form':'cancel',
    'click .save': 'submit'
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.set({'item_type':'video'},{silent:true})
    this.select_value = this.model.get('sub_type') || 'youtube'
    var that = this;
    // Handle communication with upload iframe
    this.tickets = 0;
    this.callbacks = [];
    window.addEventListener("message", function( event ) {
      var data = event.data;
      if(data.ticket && that.callbacks[data.ticket]){
        that.callbacks[data.ticket](data);
      }
    }, false);

  },

  // Re-render the contents of the viewItem item.
  render: function() {
    
    if(this.$el.html().length == 0){
      this.$el.html(this.template({view_item:this.model, aws_directory: 'http://'+ $("#aws_directory").val() +'.s3.amazonaws.com/iframe.html'}));
    } 

    var select = this.$("select[name='view_item[sub_type]']")
    select.val(this.select_value)

    select.on('change',function(select){
      this.select_value = $(select.currentTarget).val();
      this.render()
      return true
    }.bind(this))

    this.$('fieldset').hide()
    this.$('.'+select.val()+'-fields').show()
    this.$("input[name='view_item[sub_type]']").remove()
    return this;
  },

  setFile: function(file){
    this.select_value = 'upload';
    this.$('.filename').text(file.name);
    this.file = file;
    this.render();
  },

  cancel: function(event){
    event.preventDefault()
    this.remove()
  },

  submitFile: function(event, file) {
  
  },

  submit: function(e) {
    e.preventDefault();

    var sub_type = this.$("select[name='view_item[sub_type]']").val()

    if(sub_type == 'upload') {
      var $bar = this.$(".progress-bar");
      $bar.progressbar({ value: 0 });

      var that = this;
      var url = '/video_credentials.json';

      $.ajax(url, {
        dataType: 'JSON',
        success: function(data){
          that.tickets++;
          data.ticket = that.tickets;
          that.callbacks[data.ticket] = function(a){
            if(a.type == 'progress'){
              $bar.progressbar({ value: a.progress });
              if(a.progress == 100){
                $(".progress-bar").replaceWith('<div style="text-align: center; margin: 5px;">Please wait while your upload is processing. It can take up to 30 minutes, do not close or reload this page during that time.<div>');
              }
            }else if(a.type == 'success'){
              that.$("form").append('<input name="view_item[video_file]" type="hidden" value="'+data.key+'" />')
              VSR.Views.Forms.genericSubmit.call(that, e);
            }
          }
          that.$("iframe")[0].contentWindow.postMessage(data, '*');
        }
      });
    }else{
      VSR.Views.Forms.genericSubmit.call(this, e);
    }
  },

  serialize: function() {
    var sub_type = this.$("select[name='view_item[sub_type]']").val()
    var result = {
      'sub_type' :sub_type,
      'title': this.$("input[name='view_item[title]']").val()
    }
    if(sub_type == 'vimeo'){
      result.vimeo_id = this.$("input[name='view_item[vimeo_id]']").val()
    }else if(sub_type == 'youtube'){
      result.youtube_id = this.$("input[name='view_item[youtube_id]']").val()
    }else if(sub_type == 'upload'){
      result.video_file = this.$("input[name='view_item[video_file]']").val().split('/').pop();
      result.show_link = this.$("input[name='view_item[show_link]']").is(":checked");
    }

    return result
  }

});
