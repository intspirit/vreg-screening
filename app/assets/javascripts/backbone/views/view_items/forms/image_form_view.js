VSR.Views.ImageForm = Backbone.View.extend({
    // template: _.template($('#viewItem-template').html()),
    template: JST["backbone/templates/view_items/forms/image_form"],
    className: 'foreground',
    // The DOM events specific to an item
    events: {
      'click .cancel-form' :'cancel',
      'click .save': 'save'
    },

    // The ViewItemView listens for changes to its model, re-rendering.
    initialize: function() {
      this.model.on('change' , this.remove, this);
    },

    finish: function(){
      this.model.parent().trigger('sync')
    },


    // Re-render the contents of the viewItem item.
    render: function() {
      this.$el.html(this.template({view_item:this.model}));
      //cant catch events :S panic timed solution
      this.$('form').bind('ajax:beforeSend', _.bind(function(){
          this.uploading = true
          this.finish()
      },this));
      return this;
    },
    
    cancel: function(event){
      event.preventDefault()
      this.remove()
    },

    save: function(event){
      event.preventDefault()

      $.ajaxSetup({ cache: false });
      $.ajax(this.model.url(), {
          type: 'POST',
          data: this.$('form').find('input:hidden, input:text').serializeArray(),
          files: $(":file", this.$('form')),
          iframe: true,
          processData: false,
          success: this.submitSuccess.bind(this),
          error: this.submitError.bind(this)
      });
    },
    
    submitSuccess: function(response, event) {
      // TODO - this desnt work becouse of bug in rails 3.2.2
      // it wraps the json with some garbage
      
      var responseText
      if(response.responseText){
        responseText = response.responseText
      }else{
        responseText = response
      }
      
      var start = responseText.indexOf('{')
      var end = responseText.lastIndexOf('}')+1
      responseText = responseText.substring(start,end)
      
      var attributes = $.parseJSON(responseText)
      var add = this.model.isNew()
      this.model.set(attributes,{silent:true})
      if(add){
       this.model.collection.add(this.model)
      }
      this.model.parent().insertChildAt(this.model,this.model.get('position'))
      this.model.trigger('change');
      this.model.parent().trigger('change');
      this.remove()
    },
    
    submitError: function(response, event){
      var err_cont = this.$('.error-explanation')
      if(response.status == 422){
        err_cont.html('')
        var errors = $.parseJSON(response.responseText).errors
        for(field in errors){
          err_cont.append('<span class="field">'+field+'</span> '+errors[field]+'.<br/>')
        }
      }else if(response.status >= 200 && response.status < 300){
        // not realy an error
        this.submitSuccess(event,response)
      }else{
        err_cont.html(VSR.Config.generircErrorMSG)
      }
    }
    
  });