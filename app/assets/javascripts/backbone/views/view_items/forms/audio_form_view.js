VSR.Views.AudioForm = Backbone.View.extend({
    // template: _.template($('#viewItem-template').html()),
    template: JST["backbone/templates/view_items/forms/audio_form"],
    className: 'foreground',
    // The DOM events specific to an item
    events: {
      'click .cancel-form':'cancel',
      'click .save':'submit'
    },

    // The ViewItemView listens for changes to its model, re-rendering.
    initialize: function() {
      this.model.on('change' , this.remove, this);
      if(!this.model.get('item_type'))
        this.model.set({sub_type:'audio'},{silent:true})
      if(!this.model.get('sub_type'))
        this.model.set({sub_type:'soundcloud'},{silent:true})
    },

    // Re-render the contents of the viewItem item.
    render: function() {
      this.$el.html(this.template({view_item:this.model}));      
      
      return this;
    },
    
    cancel: function(event){
      event.preventDefault()
      this.remove()
    },
    
    submit: function(e) {
      _.bind(VSR.Views.Forms.genericSubmit,this)(e)
    },
  
    serialize: function() {
      return {
        item_type:'audio',
        sub_type:'soundcloud',
        soundcloud_id:this.$('input[name="view_item[soundcloud_id]"]').val(),
        title:this.$('input[name="view_item[title]"]').val()
      }
    }
    
  });