VSR.Views.BoxForm = Backbone.View.extend({
    // template: _.template($('#viewItem-template').html()),
    template: JST["backbone/templates/view_items/forms/box_form"],
    className: 'foreground',
    // The DOM events specific to an item
    events: {
      'click .cancel-form' : 'cancel',
      'click .save'        : 'submit',
      'click .option'      : 'option'
    },

    // The ViewItemView listens for changes to its model, re-rendering.
    initialize: function() {
      this.model.set({'item_type':'box'},{silent:true})
    },
    
    option: function(event) {
      this.$('.active').removeClass('active')
      $(event.currentTarget).addClass('active')
    },

    // Re-render the contents of the viewItem item.
    render: function() {
      this.$el.html(this.template({view_item:this.model}));
      
      if(this.initial){
        this.$('h3').html('This presentation is empty. Please choose a layout to start with.')
      }
      
      var active
      _.each(VSR.Views.BoxViewConfig.topLevelSubTypes, function(sub_type){
        active = this.model.get('sub_type') == sub_type ? 'active' : ''
        this.$('.options').append('<div class="option '+active+' drag new-'+sub_type+' new-'+sub_type+'-bg bg'+'" data-item-type="box" data-item-sub-type="'+sub_type+'"></div>')
      }, this);
      
      return this;
    },
    
    cancel: function(event){
      event.preventDefault()
      this.remove()
    },
    
    submit: function(event) {
      
      event.preventDefault()
      var new_sub_type = this.$(".active").attr('data-item-sub-type')
      
      if( this.model.isNew() ){
        VSR.Views.BoxViewConfig.addBox(this.model.parent(),new_sub_type,this.model.get('position'))
        
      }else if( new_sub_type != this.model.get('sub_type') ){
        
        var existing_children          = this.model.children()
        var existing_children_length   = existing_children.length
        var new_children_length        = VSR.Views.BoxViewConfig.subTypeChildren[new_sub_type].length
        var max_lenght = existing_children_length > new_children_length ? existing_children_length : new_children_length
        var childSubTypes = VSR.Views.BoxViewConfig.subTypeChildren[new_sub_type]
        
        // setup appropriate child boxes
        for(var i=0; i < new_children_length; i++){
          // changes subtypes for existing children
          if(existing_children[i]){
            existing_children[i].set({item_type:'box',sub_type:childSubTypes[i]})
            existing_children[i].save()
          }else{
          // create children if missing
            var child = this.model.buildChild({item_type: 'box',sub_type:childSubTypes[i], position:i});
            this.model.collection.add(child)
            child.save()
          }
        }
        
        // when children should be reduced, just disable those unused
        for(var i=new_children_length; i < max_lenght; i++){
          if(existing_children[i]){
            existing_children[i].set({item_type:'box',sub_type:'disabled'})
            existing_children[i].save()
          }
        }
        
        this.model.set({sub_type:new_sub_type})
        this.model.save()
        // assure right number of box-children present
        // transfer contents
        // rebrand children
      }
      this.remove()
    }
  });