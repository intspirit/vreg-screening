VSR.Views.VideosList = Backbone.View.extend({
  tagName: "div",
  className: 'videos-list',

  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/videos_list"],
  vimeo_template: JST["backbone/templates/view_items/embed_code/vimeo"],
  youtube_template: JST["backbone/templates/view_items/embed_code/youtube"],

  // The DOM events specific to an item
  events: {
    'click .video-thumb img':'play'
  },
  
  // change current video and rerender
  play:function(event){
    event.preventDefault()
    var clicked_id = $(event.currentTarget.parentNode).attr('data-id')
    this.model.current_video = this.model.collection.get(clicked_id)
    if(VSR.Config.mode == 'edit'){
      this.model.parent().trigger('change') 
    }else{
      this.render()
    }
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , function(){this.model.parent().trigger('change')}, this);
  },
  
  initializeDropables: function() {
    this.$('.videos > .video').droppable({
      greedy: true,
      accept: '.new-video,.video',
      activeClass: "can-drop",
      hoverClass: "hovering",
      tolerance: 'pointer',
      drop: _.bind(this.addVideo,this)
    });
  },
  
  addVideo: function(event, ui){
    var position = 0
    var dropReceiver = $(event.target).attr('data-id')
    
    if(dropReceiver){
      dropReceiver = this.model.collection.get(dropReceiver)
      if(dropReceiver.get('item_type') == 'video-thumb' )
        position = dropReceiver.get('position')
    }
    
    if($(ui.draggable).attr('data-id')){
      // EXISTING
      var existing = this.model.collection.get($(ui.draggable).attr('data-id'))
      var oldParent = existing.parent()
      var newParent = this.model
      position = dropReceiver.get('position')
      existingDrop = this.model.collection.get(existing).get('position')
      if (existingDrop < position) {
        position += 1
      }
      newParent.insertChildAt(existing, position).save({},{silent:true})
      newParent.trigger('change')
      if(oldParent.id != newParent.id){
        oldParent.trigger('change')  
      }
    } else {
      // NEW
      var new_model = this.model.buildChild({position:position})
      var view = new VSR.Views.VideoForm({model:new_model})
      VSR.Config.Foreground.renderView(view) 
    }
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    var children = this.model.children()
    this.$el.html('');
    this.$el.attr('data-id',this.model.id)
    if (VSR.Config.mode == 'edit' && children.length==0) {
      this.$el.append('<div class="initial video">Place a Video here.<br/><sub>(This Placeholder is Visible Only in Editing Mode)</sub></div>')
    }
    
    var children = this.model.children()
    
    // render player for current video
    
    if(this.model.current_video){
      // IS current_video still in my children ? if not then is not my current_video any more 
      if( ! _.any(children,function(child){return this.current.id == child.id },{current:this.model.current_video}) ){
        this.model.current_video = children[0]
      }
    }else{
      this.model.current_video = children[0]
    }

    if(this.model.current_video){
      this.$el.html(this.template({model: this.model.current_video}));
      // this.model.current_video = the video to appear into player
      
      this.$('.video-title').html(this.model.current_video.get('title'))

      if(this.model.current_video.get('sub_type') == 'vimeo' && this.model.current_video.get('vimeo_id')){
        var video = new VSR.Views.VimeoVideo({model:this.model.current_video,el:this.$('.video-player-container')})
        video.render()
      }else if(this.model.current_video.get('sub_type') == 'youtube' && this.model.current_video.get('youtube_id')){
        var video = new VSR.Views.YouTubeVideo({model:this.model.current_video,el:this.$('.video-player-container')})

        video.render()
      }else if(this.model.current_video.get('sub_type') == 'upload'){

        var video = new VSR.Views.UploadVideo({model:this.model.current_video,el:this.$('.video-player-container')})
        video.render()

      } else if(VSR.Config.mode == 'edit'){
        this.$el.html('<div class="initial">Edit video to show player.</div>')
      }
    }else{
      this.$el.html('')
    }

    // videos
    if ((VSR.Config.mode == 'edit' && children.length > 0) || (VSR.Config.mode != 'edit' || children.length > 1) ){
      this.$el.append('<div class="videos"></div>')
      // videos thumbs for changing videos
      var child_view
      _.each( children,function(item){
        
        child_view = new VSR.Views.Video({model:item})
        //child_view.afterRender = VSR.Views.BoxViewConfig.wrapWithDropAreas
        child_view.render()
        //is in the player ?
        if(item.id == this.model.current_video.id){
          child_view.$el.addClass('active')
        }else{
          child_view.$el.removeClass('active')
        }
        this.$('.videos').append(child_view.el)
      },this)
    }else{
      this.$el.append('<div class="videos"><div class="initial video">Video player - place a video here.<br/><sub>(This Placeholder is Visible Only in Editing Mode)</sub></div></div>')
    }
      
    if (VSR.Config.mode == 'edit') {
      this.initializeDropables()
      VSR.Views.addElementControls(this.$el,{edit:false})
      // this.$el.css('height', this.$el.css('height'))
    }
    if(this.afterRender) this.afterRender(this)
    return this;
  }
});
