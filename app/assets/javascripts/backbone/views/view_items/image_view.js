VSR.Views.Image = Backbone.View.extend({
  className: 'image',

  // template: _.template($('#viewItem-template').html()),
  template: JST["backbone/templates/view_items/image"],

  // The DOM events specific to an item
  events: {
    'click .edit-button': 'edit'
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    // this.model.on('change' , this.render, this);
    // this.model.on('sync' , this.sync_render, this);
    this.model.on('change' , this.render, this);
  },
  
  edit: function(event) {
    if(VSR.Config.mode == 'edit') {
      event.preventDefault();
      var view = new VSR.Views.ImageForm({model:this.model})
      VSR.Config.Foreground.renderView(view)
    }
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {
    //console.log('Image render '+this.model.id)
    this.$el.attr('data-id',this.model.id)
    // this.$el.html(this.template({model:this.model}));
    // console.log(this.parent.id)
    if(this.model.get('image_url')){
      this.$el.html('<img src="'+this.model.get('image_url')+'" />')
    }else{
      this.$el.html('<div class="initial new-image-bg">New Image</div>')
    }
    if(VSR.Config.mode == 'edit')
      VSR.Views.addElementControls(this.$el)

    if(this.afterRender) this.afterRender(this)
    return this;
  }
});