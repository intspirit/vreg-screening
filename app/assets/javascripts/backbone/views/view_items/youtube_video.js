VSR.YouTubeAPI = {}
VSR.YouTubeAPI.InitCallbacks = []
VSR.YouTubeAPI.playingCallback = false
VSR.YouTubeAPI.init = function(){
  setInterval(VSR.YouTubeAPI.checkPlayer,500)
}
VSR.YouTubeAPI.checkPlayer = function(){
  if(VSR.YouTubeAPI.playingCallback){VSR.YouTubeAPI.playingCallback()}
}

VSR.Views.YouTubeVideo = Backbone.View.extend({

  template: JST["backbone/templates/view_items/embed_code/youtube"],

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
  },
  
  iframeId: function (){
    return "yt"+this.cid
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {
    // render iframe
    this.$el.html(this.template({iframe_id:this.iframeId(),video_id:this.model.get('youtube_id')}))

    // is YT API ready ? if not add a callback to be called when its ready
    if(VSR.YouTubeAPI.Ready){
      this.initPlayerAPI()
    }else{
      VSR.YouTubeAPI.InitCallbacks[VSR.YouTubeAPI.InitCallbacks.length] = _.bind(this.initPlayerAPI,this)
    }
    return this;
  },
  
  initPlayerAPI:function(){
    this.player = new YT.Player(this.iframeId(), {
      events: {
        'onReady': _.bind(this.onPlayerReady,this),
        'onStateChange': _.bind(this.onPlayerStateChange,this)
      }
    });
  },
  
  onPlayerReady:function(){
    this.play_action = new VSR.Models.UserAction({
      view_item_id:this.model.id,
      project_id:VSR.Routers.view_items_router.project.id,
      action_type:'play_video',
      description: this.model.get('title')+'</br>'
    })
  },
  
  playing: function(data){
    if(this.previes_second > currentSecond){
      this.initUserAction()
    }
    var currentSecond = Math.round(parseFloat(this.player.getCurrentTime()))
    this.play_action.mark_as_played(currentSecond)
  },
  
  onPlayerStateChange:function(event){
    if(event.data == YT.PlayerState.PLAYING){
      VSR.YouTubeAPI.playingCallback = _.bind(this.playing,this)
    }else if(event.data == YT.PlayerState.BUFFERING) {
    }else{
      VSR.YouTubeAPI.playingCallback = false
    }
    
    //if(event.data == YT.PlayerState.BUFFERING){
    //}else if(event.data == YT.PlayerState.ENDED){
    //}else if(event.data == YT.PlayerState.PAUSED){
    //}else if(event.data == YT.PlayerState.CUED){
    //}else if(event.data == -1){  }
  }
});

// 1. this function is called when the API is ready
function onYouTubePlayerAPIReady() {
  VSR.YouTubeAPI.Ready = true
  var callback
  while(callback = VSR.YouTubeAPI.InitCallbacks.shift()){
    callback()
  }
  VSR.YouTubeAPI.init()
}


// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "http://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);