VSR.Views.ViewItem = Backbone.View.extend({
  tagName: "div",

  template: JST["backbone/templates/view_items/default"],

  // The DOM events specific to an item
  events: {
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.html('');
    _.each(this.model.children(),function(item) {
      var child_view = VSR.Views.ViewItem.getView(item.get('item_type'));
      child_view = new child_view({model:item, parent: this.model});
      this.$el.append(child_view.el);
      child_view.render();
    }, this);
    return this;
  }
});

/* returns the view class function based on item_type*/
VSR.Views.ViewItem.getView = function(item_type,form) {
  //camelcase
  var class_name = item_type.replace(/(\-[a-z])/g, function($1){return $1.toUpperCase().replace('-','');});  
  //capitalize
  class_name = class_name.charAt(0).toUpperCase() + class_name.slice(1);
  // form
  if(form) class_name += 'Form' 
  return VSR.Views[class_name] ? VSR.Views[class_name] : VSR.Views.ViewItem
}
