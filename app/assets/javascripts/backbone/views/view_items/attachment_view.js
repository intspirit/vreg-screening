VSR.Views.Attachment = Backbone.View.extend({
  tagName: "div",
  className: 'attachment',

  template: JST["backbone/templates/view_items/attachment"],

  // The DOM events specific to an item
  events: {
    'click .edit-button': 'edit',
    'click a.download-link':'download'
  },
  
  download: function(event){
    var user_action = new VSR.Models.UserAction({
      view_item_id:this.model.id,
      project_id:VSR.Routers.view_items_router.project.id,
      action_type:'download'
    })
    user_action.save()
  },

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
  },

  // Edit Text Content
  edit: function(e) {
    if(VSR.Config.mode == 'edit') {
      e.preventDefault();
      var view = new VSR.Views.AttachmentForm({model: this.model});
      VSR.Config.Foreground.renderView(view);
    }
  },

  // Re-render the contents of the viewItem item.
  render: function() {
    this.$el.html('')
    this.$el.attr('data-id',this.model.id)
    if (this.model.get('file_url')) {
      this.$el.append(this.template({view_item: this.model}));
    }else if(VSR.Config.mode == 'edit'){
      this.$el.append('<div class="initial">Empty Attachment - Edit to upload a file</div>')
    }

    if(VSR.Config.mode == 'edit')
      VSR.Views.addElementControls(this.$el)
    
    if(this.afterRender) this.afterRender(this)
    return this;
  }  
});