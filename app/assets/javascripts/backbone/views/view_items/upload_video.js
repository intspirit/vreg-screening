VSR.Views.UploadVideo = Backbone.View.extend({

  template: JST["backbone/templates/view_items/embed_code/upload"],

  // The ViewItemView listens for changes to its model, re-rendering.
  initialize: function() {
    this.model.on('change' , this.render, this);
  },
  
  // Re-render the contents of the viewItem item.
  render: function() {
    // render iframe
    var width = Math.min(this.$el.width(), 854);
    var height = Math.round(width/(16/9));
    this.$el.width(width);
    var id = 'player-'+this.model.get('id');
    console.log(this.model.get('thumb_url'));
    console.log('a')
    this.$el.html(this.template({versions: this.model.get('versions'), width: width, height: height, id: id, show_link: this.model.get('show_link'), thumbnail_url: this.model.get('thumb_url')}))
    delete _V_.players[id];
    var view = this;
    this.player = _V_(id);
    this.player.ready(function(){
        view.onPlayerReady();
        this.addEvent('play', function(){view.playing()});
        this.addEvent('timeupdate', function(){view.playing()});
    });

    return this;
  },
  
  playing: function(data){
    var currentSecond = Math.round(parseFloat(this.player.currentTime()))
    this.play_action.mark_as_played(currentSecond)
  },

  onPlayerReady:function(){
    this.play_action = new VSR.Models.UserAction({
      view_item_id:this.model.id,
      project_id:VSR.Routers.view_items_router.project.id,
      action_type:'play_video',
      description: this.model.get('title')+'</br>'
    })
  },
  
  onPlayerStateChange:function(event){
    if(event.data == YT.PlayerState.PLAYING){
      VSR.YouTubeAPI.playingCallback = _.bind(this.playing,this)
    }else if(event.data == YT.PlayerState.BUFFERING) {
    }else{
      VSR.YouTubeAPI.playingCallback = false
    }
    
  }
});
