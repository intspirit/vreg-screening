VSR.Views.ContactsIndex = Backbone.View.extend({
  global: false,
  
  initialize:function(){
    this.collection.on('reset', this.render, this)
    this.collection.on('add', this.render, this)
    this.collection.on('change', this.render, this)
    this.collection.on('remove', this.render, this)
    this.collection.on('destroy', this.render, this)
    this.draggable_children = true
  },
  
  events:{
    'keyup .search' : 'filter',
    'change'        : 'checkboxClicked'
  },

  checkboxClicked: function(e) {
    e.preventDefault();
    if (this.global) {

    } else {

    }
  },
  
  render:function(){
    var title = this.title || "Contacts"
    this.$el.html('<div class="head"><h3>'+title+'</h3><div>')
    if(this.small && this.collection.length <= 9){
      this.$('.head').append('<small>'+this.small+'</small>')
    }
    if(this.collection.length == 0) {
      this.$el.append('<ul class="contacts_list"><li><div style="text-align:center; ">Contact list is empty. Add new contacts.</div><li></ul>')
    } else {
      if (this.collection.length > 9 ){
        this.$('.head').append('<input class="search" type="text" placeholder="Search">')
      }
      this.$el.append('<ul class="contacts_list"></ul>')      
      
      this.collection.each( function(contact){
        this.appendContact(contact)
      }.bind(this)) 
    }
    
    return this
  },
  
  appendContact:function(contact){
    var view = new VSR.Views.Contact({model: contact})
    view.draggable = this.draggable_children
    this.$('.contacts_list').append(view.render().el)
    if (this.global) {
      this.$('.contacts_list li').addClass('global')
    }
  },
  
  updateContactList: function(contact_group_id) {
    this.collection.fetch({
	    data: { contact_group_id: contact_group_id },
      processData:true
    });
  },
  
  allContactList: function() {
    this.collection.fetch()
  },

  
  addContact: function(event, ui) {
    var draggableElement = $(ui.draggable).attr('data-id');
    if ($(ui.draggable).attr('data-type') == 'Contact') {
      if (this.collection.get(draggableElement) == undefined) {
        var viewer = new VSR.Models.Viewer({contact_id:draggableElement})
        viewer.project = this.project;
        this.collection.create(viewer) 
      }
    } else{
      var contacts = VSR.Data.groups.get(draggableElement).collection;
      contacts.each(function(contact) {
        if (this.collection.get(contact.get('id')) == undefined) {
          var viewer = new VSR.Models.Viewer({contact_id:contact.id})
          viewer.project = this.project;
          this.collection.create(viewer); 
        }        
      }.bind(this));
    };
  },
  
  filter:function(){
    this.regex=new RegExp('.*'+this.$('.search').val()+'.*','i')
    this.$('.contacts_list li').each(function(i,li){
      li = $(li)
      if(this.regex.test(li.attr('data-search'))){
        li.show()
      }else{
        li.hide()
      }
    }.bind(this))
  }
})