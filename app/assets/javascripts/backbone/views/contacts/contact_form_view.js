VSR.Views.ContactForm = Backbone.View.extend({
  template: JST['backbone/templates/contacts/contact_form'],
  
  className:'foreground',
  
  events:{
    'submit form'         : 'createContact',
    'click .cancel-form'  : 'cancel'
  },
  
  render: function(){
    this.$el.html(this.template())
    return this
  },
  
  cancel: function(event){
    event.preventDefault();
    this.remove();
  },
  
  createContact: function(event){
    event.preventDefault();
    this.model.save(
      {
        full_name : this.$('input[name="contact[full_name]"]').val(),
        email: this.$('input[name="contact[email]"]').val()
      },
      {
        wait: true,
        success: function(){
          if(!this.model.collection.get(this.model.id)){
            this.model.collection.add(this.model)
          }
          this.remove()
        }.bind(this),
        error:function(entry,response){
          if(response.status == 422){
            errors = $.parseJSON(response.responseText).errors
            this.$('.errors').html('Email '+errors.email+'.')
          }
        }
      }
    )
  }
})