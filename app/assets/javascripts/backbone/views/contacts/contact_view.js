VSR.Views.Contact = Backbone.View.extend({
  template: JST['backbone/templates/contacts/contact_li'],
  
  tagName:'li',
  className:'contact',
    
  initialize: function() {
    
  },
  
  render:function(){
    this.$el.attr('data-id', this.model.id)
    this.$el.attr('data-type', 'Contact');    
    this.$el.html(this.template({contact:this.model}))
    this.$el.attr('data-search', '' + this.model.get('full_name') + this.model.get('email'))
    return this
  }
})