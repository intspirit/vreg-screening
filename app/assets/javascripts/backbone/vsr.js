//= require_self
//= require_tree ./templates
//= require_tree ./models
//= require_tree ./views
//= require_tree ./routers

Backbone.Collection.prototype.update = function(col_in){  
  var that = this;

  var ids = [];

  _(col_in).each(function(mod_in){
    if (that.get(mod_in.id)){
      that.get(mod_in.id).set(mod_in);
    } else {
      that.add(mod_in);
    }

    ids.push(mod_in.id);
  });

  var to_remove = that.reject(function(mod){
    return _(ids).include(mod.id);
  });

  this.remove(to_remove);
};

window.VSR = {
  Config: {},
  Data: {},
  Models: {},
  Collections: {},
  Routers: {},
  Views: {},
  Editors: {},
  init: function() {
    if (viewingMode != undefined) {
      VSR.Config.mode = viewingMode;
    } else {
      VSR.Config.mode = 'view';
    }

    var id = ''
    
    id = '#contacts-container'
    if($(id).length != 0){      
      //this.Routers.contacts = new VSR.Routers.Contacts(id)
    }
    
    id = '#projects-container'
    if($(id).length != 0){
      this.Routers.projects = new VSR.Routers.Projects(id)
    }
      
    if( $(document.body).hasClass('editor') || $(document.body).hasClass('preview') ) {      
      var project_id = $(document.body).data('project-id')
      var data = $(document.body).data('view-items')
      var root_view_item = $(document.body).data('root-id')
      VSR.Routers.view_items_router = new VSR.Routers.ViewItems(project_id, data, root_view_item);
      VSR.Routers.view_items_router.rootView.render()
      
    } else if ($(document.body).hasClass('editor')) {  
      
      // if no layout present, show form for selecting one
      // root -> drop-pannel.children.empty?
      var drop_panel = VSR.Routers.view_items_router.project.rootViewItem.children()[0]
      if(drop_panel.children().length == 0) {
        var viewItem = drop_panel.buildChild({item_type:'box',sub_type:'box2',position:0});
        var view = new VSR.Views.BoxForm({model: viewItem});
        view.initial = true
        VSR.Config.Foreground.renderView(view);      

      }
      
    }
    
    if(Backbone.history)
    Backbone.history.start()
  }
};

VSR.Config.generircErrorMSG = 'An error happened.'

VSR.Config.Foreground = {
      id :"#foreground-panel",
      // pass A backbone view here
      // it will render to foreground container
      // and it will hide the foreground container when remove is called
      renderView: function(view){
        view.render()
        $(VSR.Config.Foreground.id).html(view.el).show()
        if(view.initCKEditor){
          view.initCKEditor()
        }        
        view.remove = _.wrap(view.remove,function(func){
          _.bind(func,view)()
          $(VSR.Config.Foreground.id).hide()
        })
      }
    },
VSR.Config.Background = { id: "#background-panel" }

VSR.Views.Drag = {}
VSR.Views.Drag.scrollSensitivity = 0.15
VSR.Views.Drag.scrollSpeed = 20 // px/s

VSR.Views.Drag.dragStart = function(event, ui) {
  // Only toolbar items have 'drag' class. Their container is position fixed, so position is related to viewport
  VSR.Views.Drag.relativeToViewport = ui.helper.hasClass('drag')
  VSR.Views.Drag.viewportHeight = $(window).height()
  VSR.Views.Drag.viewportWidth  = $(window).width()
  if(VSR.Views.Drag.relativeToViewport){
    VSR.Views.Drag.offset = {left:event.clientX, top:event.clientY} 
  }else{
    VSR.Views.Drag.offset = {left:event.pageX, top:event.pageY} 
  }
  $(document.body).addClass('draging')
}

VSR.Views.Drag.dragStop = function(event, ui) {
  // Only toolbar items have 'drag' class. Their container is position fixed, so position is related to viewport  
  // ui.originalPosition = {top:0,left:0}
  $(document.body).removeClass('draging')
}

VSR.Views.Drag.revert = function () {
  //overwrite original position
  if($(this).data("draggable"))
  $(this).data("draggable").originalPosition = {
      top: 0,
      left: 0
  };
  //return boolean
  return true;
}

VSR.Views.Drag.toolbarWidth = 360
VSR.Views.Drag.toolbarHeight = 50

$(document).ready(function(){
  $(document).bind('dragenter', function(event){
    var $e = $('.new-video').eq(0);
    // hack to fool jquery-ui
    event.type = 'mousedown';
    $e.draggable().data().draggable._mouseStart(event);
  });

  var moveVideoHelper = function(e){
    var $e = $('.new-video').eq(0);
    var x = e.originalEvent.x;
    var y = e.originalEvent.y;
    $e.offset({top: y, left: x});
    var d = $e.draggable().data().draggable;
    d.offset.click.top = 0;
    d.offset.click.left = 0;
    d._mouseDrag(e.originalEvent, true)
  }

  $(document).bind('dragover', function(e){
    moveVideoHelper(e);
  })

  $(document).bind('drop', function(e, ui){
    var dt = e.originalEvent.dataTransfer;
    e.stopPropagation();
    e.preventDefault();

    if(!dt || ui) {
      return true;
    }

    e.originalEvent.fileDropped = dt.files[0];
    moveVideoHelper(e);
    var $e = $('.new-video').eq(0);
    var d = $e.draggable().data().draggable;
    d._mouseStop(e.originalEvent)
    $e.removeClass("ui-draggable-dragging");
    d._clear();
    return false;
  });

});

VSR.Views.Drag.onDrag = function(event,ui){
  VSR.Views.Drag.dragged = ui.helper
  var mouseY =  event.clientY
  var mouseX =  event.clientX
  var scroll = 0
  
  //set position of dragged
  if(VSR.Views.Drag.relativeToViewport){
    ui.position = {left:event.clientX - VSR.Views.Drag.offset.left,top:event.clientY - VSR.Views.Drag.offset.top} 
  }else{
    ui.position = {left:event.pageX - VSR.Views.Drag.offset.left,top:event.pageY - VSR.Views.Drag.offset.top} 
  }
  
  if( mouseY < VSR.Views.Drag.toolbarHeight ) {    
    //scroll up ?
    var left =  VSR.Views.Drag.viewportWidth / 2 - VSR.Views.Drag.toolbarWidth / 2
    var right = VSR.Views.Drag.viewportWidth / 2 + VSR.Views.Drag.toolbarWidth / 2
    if( mouseX < left || right < mouseX ){      
      scroll = VSR.Views.Drag.scrollSpeed * -1
    }
    
    
  } else if(mouseY >  (VSR.Views.Drag.viewportHeight - VSR.Views.Drag.toolbarHeight*2)){
    //scroll down ?
    scroll = VSR.Views.Drag.scrollSpeed
  }
  VSR.Views.Drag.scroll = scroll
}

VSR.Views.Drag.scroll = function(){
  if(VSR.Views.Drag.dragged && VSR.Views.Drag.dragged.hasClass('ui-draggable-dragging')){
    //console.log(window.scrollY + VSR.Views.Drag.scroll)
    window.scroll(0,window.scrollY + VSR.Views.Drag.scroll)
  }else{
    VSR.Views.Drag.dragged = null
  }
}

VSR.Views.Drag.init = function(){
  setInterval(VSR.Views.Drag.scroll,jQuery.fx.interval)
}

VSR.Views.addElementControls = function(mainElement, options){
    if(!(options && options.edit===false)){
      var edit_class = (options && options.edit_class) || "edit-button"
      mainElement.append('<a class="'+edit_class+'"></a>') 
    }
    if(!(options && options.drag===false))
      mainElement.append('<div class="drag-handle"></div>')
    $(mainElement).draggable({
      handle: $('> .drag-handle',mainElement),
      revert: VSR.Views.Drag.revert,
      scroll: false,
      iframeFix:true,
      drag: VSR.Views.Drag.onDrag,
      start: VSR.Views.Drag.dragStart,
      stop: VSR.Views.Drag.dragStop
    });
},

VSR.Views.Forms={
/*
this function handels rails' vlidation errors
requirements:
- receives jquery event e
- this.model
- this.serialize() -> {attr:for_model}
- this.remove() -> removes view on success
- this.$('.error_explanation') -> container for errors
*/
  
  genericSubmit:function(e, options) {
    e.preventDefault();
    var attrs = this.serialize();
    options = options || {};
    return this.model.save(attrs, _.extend(
      {
        wait: true,
        success:_.bind( function(model,response){
          if(!this.model.collection.get(this.model.id)){
            this.model.collection.add(this.model)
          }
          this.model.parent().insertChildAt(this.model,this.model.get('position'))
          this.model.trigger('change')
          if(this.model.parent)
            this.model.parent().trigger('change')
          this.remove()
        },this),
        error:_.bind( function(entry,response){
          var err_cont = this.$('.error-explanation')
          if(response.status == 422){
            err_cont.html('')
            var errors = $.parseJSON(response.responseText)
            for(field in errors){
              err_cont.append('<span class="field">'+field+'</span> '+errors[field]+'.<br/>')
            }
          }else{
            err_cont.html(VSR.Config.generircErrorMSG)
          }
        }, this)
      }, options)
    );
  }
}

// this item_types are considered types which fill boxes

VSR.Config.FillItemTypes = ['video','text','image','attachment','audio']
VSR.Config.FillItemTypesWrappers = {'attachment':'attachments-list','video':'videos-list','audio':'audios-list'}

$(document).ready(function(){
  VSR.init()
  if(VSR.Config.mode == 'edit')
    VSR.Views.Drag.init()
})
