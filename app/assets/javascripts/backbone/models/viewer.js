VSR.Models.Viewer = Backbone.Model.extend({

	paramRoot: 'viewer',

	url : function() {
	  return this.id ? '/projects/' + this.project.id + '/viewers/' + this.id : '/projects/' + this.project.id + '/viewers';
	}
  
});

VSR.Collections.Viewers = Backbone.Collection.extend({
  model: VSR.Models.Viewer,
  
  url: function() {
    return '/projects/' + this.project.id + '/viewers'
  },

  comparator: function(viewItem) {
    return viewItem.get('name')!='' ? viewItem.get('name') : viewItem.get('email');
  }
});