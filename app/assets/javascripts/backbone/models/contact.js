VSR.Models.Contact = Backbone.Model.extend({
	initialize: function() {
	},

	paramRoot: 'contact',

	url : function() {
	  return this.id ? '/contacts/' + this.id : '/contacts';
	},

	clear: function() {
	  this.destroy();
	  this.view.remove();
	}
});

VSR.Collections.Contacts = Backbone.Collection.extend({
  model: VSR.Models.Contact,
  
  url: function() {
    if (this.contact_group_id == undefined) {
      return '/contacts';
    } else {
      return '/contact_groups/'+ this.contact_group_id +'/contacts';
    }
  },
  
  comparator: function(viewItem) {
    return viewItem.get('full_name') != '' ? viewItem.get('full_name') : viewItem.get('email');
  }
});