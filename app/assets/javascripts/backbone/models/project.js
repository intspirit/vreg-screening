VSR.Models.Project = Backbone.Model.extend({

	paramRoot: 'project',

  viewItems: null,
  rootViewItemId: null,
  rootViewItem: null,
  
  initialize:function(){
    this.viewers = new VSR.Collections.Viewers()
    this.viewers.project = this
    this.user_actions = new VSR.Collections.UserActions()
    this.user_actions.project = this
  },

  initializeViewItems: function() {
    this.viewItems = new VSR.Collections.ViewItems({project_id:this.id})
    if (this.rootViewItemId != null) 
      this.rootViewItem = this.viewItems.get(this.rootViewItem);
  },

	url : function() {
	  return this.id ? '/projects/' + this.id : '/projects/';
	},
  
  editPresentationURL: function(){
    return '/projects/' + this.id + '-' +escape(this.get('title')) + '/edit_presentation' + '#projects/' + this.id + '/edit_presentation'
  },

  presentationURL: function() {
    return '/presentation/' + this.id + '-' +escape(this.get('title')) + '#projects/' + this.id + '/preview'
  },
  
  showStatisticsURL: function(){
    return '/projects/' + this.id + '/user_actions'
  },

	clear: function() {
	  this.destroy();
	  this.view.remove();
	}
});

VSR.Collections.Projects = Backbone.Collection.extend({
  model: VSR.Models.Project,

  url: '/projects',

  comparator: function(viewItem) {
    return viewItem.get('title');
  }
});