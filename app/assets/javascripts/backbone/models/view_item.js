VSR.Models.ViewItem = Backbone.Model.extend({
	initialize: function() {
	  if (!this.get('html')) {
	    this.set({'html': this.defaults.html})  ;
	  }
	},

	paramRoot: 'view_item',

	defaults: {
	  ancestry: null,
	  position: 0
	},

  parentID : function(){
    if(this.get('ancestry')){      
      //last of ancestry is parent_id
      var ids = this.get('ancestry').split('/')
      return ids[ids.length-1]
    }else{
      return null
    }
  },
  
	url : function() {	  
    if (this.id){
      return '/view_items/'+this.parentID()+'/view_items/' + this.id
    } else {
      return '/view_items/'+this.parentID()+'/view_items'
    }
	},

	clear : function() {
	  this.destroy();
	  this.view.remove();
	},
  
  parent : function(){
    if (this.parentID()){
      return this.collection.get(this.parentID())  
    } else {
      return null
    }
  },
  
  setParent : function(parent){
    // this is also needed in children()
    // when changing parent this.ancestry and ancestry of all subtree has to be updated
    var oldAncestry = this.get('ancestry')
    var newAncestry = parent.get('ancestry')+'/'+parent.id
    
    // update the subtree
    _.each(this.subtree(),function(desc){
        if(desc.id!=this.skip){
          var anc = desc.get('ancestry').replace(this.oldPart,this.newPart)
          desc.set('ancestry',anc,{silent:true})
        }
      },
      {oldPart:oldAncestry+'/'+this.id,newPart:newAncestry+'/'+this.id,skip:this.id}
    )
    // move under new parent
    this.set({'ancestry':newAncestry},{silent:true})
  },
  
  children : function(){
    // this method has no sense wihout a cllection present
    if(!this.collection)
      return []
    var childrens_ancestry = this.get('ancestry') ? this.get('ancestry')+'/'+this.id : ''+this.id
    var result
    
    result = this.collection.filter(function(view_item){
      
      return view_item.get('ancestry') == this.childrens_ancestry
      
    }.bind({childrens_ancestry:childrens_ancestry}))
    
    return _.sortBy(result,function(model){return model.get('position')})
  },
  
  subtree:function(){
    // this method has no sense wihout a collection present
    if(!this.collection)
      return []
      
    var descendantRegex = this.get('ancestry') ? this.get('ancestry')+'/'+this.id : ''+this.id
    descendantRegex = new RegExp(descendantRegex+'/*')
    var result
    
    result = this.collection.filter(function(view_item){
      
      return descendantRegex.test(view_item.get('ancestry'))
      
    }.bind({descendantRegex:descendantRegex}))
    
    // im'also part of my subtree, i am its head
    result[result.length] = this
    
    return _.sortBy(result,function(model){return model.get('position')})
  },
  
  buildChild : function(attrs){
    attrs = attrs || {}
    attrs['ancestry'] = this.get('ancestry')+'/'+this.id
    if(!attrs['position'])
      attrs['position'] = 0
    var new_child = new VSR.Models.ViewItem(attrs)
    new_child.collection = this.collection
    // add to collection if created
    return new_child
  },
  
  insertChildAt:function(child,position){
    var oldParent = child.parent()
    //correct other childrens position  
    var oldDrugBlockPosition = child.get('position');
    _.each(this.children(),function(sibbling){
      // this sibbling has to move one place forwrd
      // silent becouse server will do this on its own
      if (sibbling.get('position') >= oldDrugBlockPosition && sibbling.get('position') <= this.reservedPosition && this.reservedPosition > oldDrugBlockPosition) {
          sibbling.set({'position': sibbling.get('position')-1},{silent:true});
      }
      if (sibbling.get('position') < oldDrugBlockPosition && sibbling.get('position') >= this.reservedPosition && this.reservedPosition < oldDrugBlockPosition) {
          sibbling.set({'position': sibbling.get('position')+1},{silent:true});

      }
    },{reservedPosition:position})    
    child.set({'position':position},{silent:true})
    child.setParent(this)
    // you sgould call save right after this
    return child
  }
  
});

VSR.Collections.ViewItems = Backbone.Collection.extend({
  model: VSR.Models.ViewItem,

  initialize: function(opts) {
    opts = opts || {}
    this.project_id = opts.project_id
  },
  
	url : function() {
	  return '/projects/'+this.project_id + '/view_items'
	},

  comparator: function(viewItem) {
    return viewItem.get('position');
  },
  
  removeWithSubtree : function(model){
    var parent = model.parent()
    _.each(parent.children(),function(child,index){child.set({position:index},{silent:true})})
    this.remove(model.subtree())
  }
});