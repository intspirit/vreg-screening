VSR.Models.ContactGroup = Backbone.Model.extend({
	initialize: function() {
	  this.collection = new VSR.Collections.Contacts();
    this.collection.contact_group_id = this.id;
    this.collection.fetch();
	},

	paramRoot: 'contact_group',

	defaults: {
	},
  
	url : function() {
    if (this.id){
      return '/contact_groups/' + this.id
    } else {
      return '/contact_groups'
    }
	},

	clear : function() {
	  this.destroy();
	  this.view.remove();
	},
    
  contacts : function(){
    if(!this.collection) {
      this.collection = new VSR.Collections.Contacts();
      this.collection.fetch({
  	    data: { contact_group_id: this.id },
        processData:true
      });
    }
    return this.collection;
  },
  
  addContact: function(contact_id) {
    
  }

});

VSR.Collections.ContactGroup = Backbone.Collection.extend({
  model: VSR.Models.ContactGroup,

  initialize: function(opts) {
    opts = opts || {}
    this.id = opts.id
  },
  
	url : function() {
	  return '/contact_groups'
	},

  comparator: function(contactGroup) {
    return contactGroup.get('id');
  }
});