VSR.Models.UserAction = Backbone.Model.extend({

	paramRoot: 'user_action',

  initialize : function(){
    var that = this;
    if (VSR.Data.contacts){
      this.contact = VSR.Data.contacts.find(function(r){return r.get('user_id')==that.get('user_id')});
    }
    this.slow_save = _.throttle(this.save, 3000)
    this.playedIntervals = ''
    this.last_second = -3
  },
  
	url : function() {
    var base
    if(this.get('project_id')){
      base = '/projects/'+this.get('project_id')+'/user_actions'
    }else{
      base = '/user_actions'
    }
    
	  return this.id ? ( base+'/'+this.id ) : base;
	},
  
  add_statement : function(statement){
    this.set('description',this.get('description')+statement+'<br/>')
    this.slow_save()
  },

  find_contact: function(contacts, user_id) {
    var contact = contacts.find(function(c){return c.get('connected_id') == user_id})
    return contact;
  },
  
  mark_as_played : function(second){
    if(this.last_second < second){
      if(second - this.last_second > 2){ // new interval ?
        this.playedIntervals += (','+second+'-'+second)
      }else{ // same interval ?
        this.playedIntervals = this.playedIntervals.replace(/[0-9]+$/,second)
        this.set('description',this.playedIntervals)
        this.slow_save()
      }
      this.last_second = second
    }
  }
  
});

VSR.Collections.UserActions = Backbone.Collection.extend({
  model: VSR.Models.UserAction,

  url: function(){
    return '/projects/'+this.project.id+'/user_actions'
  },

  comparator: function(member) {
    return member.get('started_at');
  }

});