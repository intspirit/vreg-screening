VSR.Routers.Projects = Backbone.Router.extend({
	routes: {
		""                            : "index",
    "projects/dashboard"          : "dashboard",
    "archive"                     : "archived",
    "projects/:id"                : "show",
    "projects/:id/user_actions"   : "showAnalytics",
	},

  initialize: function(container_id){
    this.container_id = container_id
    this.collection = new VSR.Collections.Projects()
    this.index_view = new VSR.Views.ProjectsIndexView({collection:this.collection, el:this.container_id})
  },
  
  index: function() {
    this.index_view.render()
  },
  
  show: function(id){
    var model = this.collection.get(id)
    if(model){
      var view = new VSR.Views.Project({model:model})
      this.index_view.$('#project').html(view.render().el) 
    }
  },
  
  showAnalytics:function(project_id){
    var model = this.collection.get(id)
    if(model){
      var view = new VSR.Views.ProjectAnalytics({model:model, el:this.container_id})
      view.render()
    }
  },

  archived: function(){
    this.archived_view = new VSR.Views.ProjectsArchivedView({collection: this.collection, el: this.container_id})
    this.archived_view.superUser = false;
    this.archived_view.render();
  },

  dashboard: function() {
    VSR.Config.mode = 'show'
    this.dashboard_view = new VSR.Views.ProjectsDashboardView({collection:this.collection, el:this.container_id})
    this.dashboard_view.superUser = false;
    this.dashboard_view.render();
  }
});