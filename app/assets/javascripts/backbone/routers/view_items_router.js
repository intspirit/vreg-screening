VSR.Routers.ViewItems = Backbone.Router.extend({
	routes: {
    "projects/:project_id/edit_presentation" : "index",
		"projects/:project_id/preview"           : "preview"
	},

  index: function(project_id) {
  },

  preview: function(project_id, data, root_view_item_id) {
  },
  
  initialize: function(project_id, data, root_view_item_id) {
    this.project = new VSR.Models.Project({id: project_id});
    this.project.rootViewItemId = root_view_item_id;
    this.project.viewItems = new VSR.Collections.ViewItems({project_id:project_id});
    this.project.viewItems.reset(data);
    this.project.rootViewItem = this.project.viewItems.get(this.project.rootViewItemId);
    this.project.s3_bucket = $("#aws_directory").val();
    this.project.fetch();
    
    this.collection = this.project.viewItems;
    
    this.rootView = new VSR.Views.ViewItem(
      {
        el:$('#main-panel'),
        model: this.project.rootViewItem,
        className: this.project.rootViewItem.get('item_type')
      }
    );
    
    if(VSR.Config.mode == 'edit'){
      this.toolbar = new VSR.Editors.Toolbar({el:$('#editing-toolbar')});
      this.toolbar.render();

      this.trash = new VSR.Editors.TrashView({collection:this.collection});
      this.trash.render();
      $('#trash').html(this.trash.$el);
  
      this.header = new VSR.Editors.Publish({model: this.project});
    }
  }
});