VSR.Routers.Contacts = Backbone.Router.extend({
  routes:{
    'contacts':'index',
    'contacts/:id':'show'
  },

  initialize:function(container_id){
    this.container_id = container_id
    this.collection = new VSR.Collections.Contacts()
    this.index_view = new VSR.Views.ContactsIndex({collection:this.collection, el:this.container_id})
    this.collection.reset($(this.container_id).data('reset'))
  },
  
  index:function(){
    this.index_view.render()
  }
})