# encoding: utf-8

class VideoFileUploader < CarrierWave::Uploader::Base
  include CarrierWave::MimeTypes

  include Rails.application.routes.url_helpers
  Rails.application.routes.default_url_options = ActionMailer::Base.default_url_options

  def store_dir
    "#{Rails.env}/uploads/videos/"
  end

  storage :fog

  version :webm do
    def full_filename(for_file)
      "#{File.basename(for_file, File.extname(for_file))}-encoded.webm"
    end
  end

  version :mp4 do
    def full_filename(for_file)
      "#{File.basename(for_file, File.extname(for_file))}-encoded.mp4"
    end
  end

  ##
  # Intercept carrierwave#cache_versions! so we can process versions later.
  def cache_versions!(new_file)
  end

  def process!(new_file=nil)
  end



end
