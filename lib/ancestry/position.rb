module PositionForAncestry
  extend ActiveSupport::Concern 
  
  included do
  end
 
  module ClassMethods
    def has_position_for_ancestry
      
      ActiveRecord::Base.send :include, PositionForAncestry
      
      # INSTANCE METHODS
      before_save   :prevent_invalid_position
      after_destroy :delete_sort
      before_update :update_sort
      after_create  :create_sort
      # position management ancestry should be modofied to support this
    end
  end
  
  
  def fix_children_positions
    self.children.each_with_index do |c,index|
      MasterPage.update_all("position=#{index}","id=#{c.id}")
    end
    self.children.each_with_index do |c|
      c.fix_children_positions
    end
  end
  
  def prevent_invalid_position
    if self.changed.include? 'position'
      if self.id
        max ||= self.siblings.where("id<>'#{self.id}'").count
      else
        max ||= self.siblings.count
      end
      self.position = ( self.position > max ? max : self.position )
    end
    true
  end  
  
  def delete_sort(use_old_scope = false)
    #puts "delete sort"
    return unless self.parent_id
    max = 100000
    position_is = max
    position_was = self.position + 1
    execute_sort(position_is,position_was,use_old_scope)
    true
  end
  
  def create_sort
    #puts "create sort"
    return unless self.parent_id
    max = 100000
    position_is = self.position
    position_was = max
    execute_sort(position_is,position_was)
    true
  end
  
  def update_sort    
    return unless self.parent_id
    #puts 'update sort'
    if self.changed.include?('position') or self.changed.include?('ancestry')
      
      # ANCESTRY PLUG
      if self.changed.include?('ancestry')
        logger.debug self.changed
        
        # delete sort with old position and old ancestry
        new_pos = self.position
        self.position = self.changed.include?('position') ? self.position_was : self.position        
        delete_sort(true)
        
        # delete restore new position
        self.position = new_pos
        
        # create sort with new position and new ancestry
        create_sort
        
        #TODO very strange, if not done here position doennt get updated
        self.class.update_all("position=#{self.position}","id=#{self.id}")
        return true
      end
      
      max = 100000
      position_is = self.position
      position_was = self.position_was
      
      if self.position_was.nil? #the object is new
        position_was = max
      end
      execute_sort(position_is,position_was)
    end
    true
  end
  
  def execute_sort(position_is,position_was,use_old_scope=false)
    #puts 'execute sort'
    dir = position_was < position_is ? " -1 ":" +1 "
    
    sql = "UPDATE #{self.class.table_name} "
    sql += " SET position = position#{dir}"
    if not use_old_scope
      sql += " WHERE ancestry='#{self.ancestry}'"
    else
      sql += " WHERE ancestry='#{self.ancestry_was}'"
    end
    sql += " AND NOT id=#{self.id}" if self.id
    if position_was > position_is #moved left
      sql += " AND position >= #{position_is}"
      sql += " AND position <= #{position_was}"
    else
      sql += " AND position >= #{position_was}"
      sql += " AND position <= #{position_is}"
    end
    ActiveRecord::Base.connection.execute sql
  end
end 

ActiveRecord::Base.send :include, PositionForAncestry