namespace :migrate do
  desc "Update image_accessor attributes in all records"
  task :dragonfly_s3, [:env] => :environment do |t, args|

    if args.env.nil? || args.env.empty?
      @env = Rails.env
      puts "Using env '#{env}'"
    else
      @env = args.env
    end

    def update_records(model, attr_name)
      model.all.each do |record|
        prefix = "#{@env}/uploads/files/"
        next unless record[attr_name]
        next if record[attr_name].match(/^#{prefix}/)
        puts "updating #{model} #{prefix}#{record[attr_name]}"
        record.update_column(attr_name, "#{prefix}#{record[attr_name]}")
      end
    end

    update_records(Attachment, :file_uid)
    update_records(Image, :image_uid)
    update_records(Video, :image_uid)
    update_records(Video, :thumbnail_uid)
    update_records(Video, :thumbnail_hover_uid)

  end
end