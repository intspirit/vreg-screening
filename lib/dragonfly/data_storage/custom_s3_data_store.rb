module Dragonfly
  module DataStorage

    class CustomS3DataStore < S3DataStore

      def generate_uid(name)
        "#{::Rails.env}/uploads/files/#{Time.now.strftime '%Y/%m/%d/%H/%M/%S'}/#{rand(1000)}/#{name.gsub(/[^\w.]+/, '_')}"
      end

      def url_for(uid, opts={})
        if opts && opts[:expires] || opts[:query]
          if opts[:query]
            options = {query: opts[:query]}
          else
            options = {}
          end
          if storage.respond_to?(:get_object_https_url) # fog's get_object_url is deprecated (aug 2011)
            rewrite_url_to_bucket_name_path(
              storage.get_object_https_url(bucket_name, uid, opts[:expires], options),
              opts[:scheme] || url_scheme
            )
          else
            rewrite_url_to_bucket_name_path(
              storage.get_object_url(bucket_name, uid, opts[:expires], options),
              opts[:scheme] || url_scheme
            )
          end
        else
          scheme = opts[:scheme] || url_scheme
          host   = opts[:host]   || url_host || "s3.amazonaws.com"
          "#{scheme}://#{host}/#{bucket_name}/#{uid}"
        end
      end

      private

        def rewrite_url_to_bucket_name_path(url, scheme)
          url.gsub(/([^:\/]+:\/\/)(#{bucket_name}).([^\/]+)/, '\1\3/\2\4')
        end

    end
  end
end