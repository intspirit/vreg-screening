####################
# shh to server:   #
####################

chmod 400 doc/cmg-dev.pem
ssh –i cmg-dev.pem root@184.72.154.120

# Load .env.example

* ```cp .env.example .env```

* Edit your settings: ```vi .env```

* ```export $(<.env)```



# Setup new heroku app

* ```heroku plugins:install git@github.com:rainforestapp/heroku.json.git```

* ```heroku bootstrap```



# Migrate MySQL DB to Postgresql DB

* Export mysql db: ```mysqldump -u root --password=<password>  vsr_production > /tmp/dump.sql```

* Create ```vsr_production``` databases in mysql and postgres on your dev env

* Copy mysql dump to your dev env: ```scp -i doc/deployer.pem deployer@<server>:/tmp/dump.sql dump.sql```

* Import mysql db to your dev env: ```mysql -u root  vsr_production < dump.sql```

* ```gem install mysql2postgres```

* ```cd config```

* Convert mysql db to postgres db: ```mysql2psql```

* Update reference attributes of dragonfly attachments: ```rake migrate:dragonfly_s3[staging]```

* Create postgres db dump for use with heroku: ```pg_dump -Fc --no-acl --no-owner -h localhost vsr_production > dump.psql```



# Upload assets stored on EC2 server to S3

* http://s3tools.org/repositories#note-deb

* ```export LANG=en_US.utf8```

* Install s3cmd: ```sudo apt-get install s3cmd```

* ```cd ~/vsr_deploy/shared/system/dragonfly```

* ```s3cmd --dry-run --acl-public  sync staging/ s3://vreg.screeningapp/staging/uploads/files/```
