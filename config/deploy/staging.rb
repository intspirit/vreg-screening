role :web, "23.22.78.52"                          # Your HTTP server, Apache/etc
role :app, "23.22.78.52"                          # This may be the same as your `Web` server
role :db,  "23.22.78.52", :primary => true        # This is where Rails migrations will run
set :rails_env, :staging
