role :web, "184.72.154.120"                          # Your HTTP server, Apache/etc
role :app, "184.72.154.120"                          # This may be the same as your `Web` server
role :db,  "184.72.154.120", :primary => true        # This is where Rails migrations will run
set :rails_env, :production
