Vsr::Application.routes.draw do
  devise_for :user, :path=>'user', :path_names => { :sign_in => 'login', :sign_out => 'logout'}
  devise_scope :user do
    get 'logout' => 'devise/sessions#destroy'
    get 'login' => 'devise/sessions#new'
  end

  match "editor_sample" => "projects#editor_sample"
  match "presentation/:id" => "projects#preview", :as => :presentation

  resources :admin_rights, only: [:destroy]
  resources :user_activations, :only => [:edit,:update, :index, :create, :destroy]  
  resources :contacts, :only => [:create, :destroy, :index, :update]
  resources :view_items do
    resources :view_items, :only=>[:create,:update,:destroy, :show]
  end
  resources :invitations

  resources :contact_groups do
    resources :contacts, :only => [:create, :update, :destroy, :show, :index]
  end

  resources :projects do
    resources :viewers, :only=>[:create,:destroy,:index] do
      resources :invitations
    end

    resources :user_actions, :only=>[:create,:index,:update]

    collection do
      get :dashboard
    end

    member do
      get 'edit_presentation'
      get 'preview'
      post "publish"
    end
  end

  resources :user_actions, :only=>[:create,:index,:update]

  mount Ckeditor::Engine => '/ckeditor'
  #mount JasmineRails::Engine => "/specs" unless Rails.env.production? || Rails.env.staging?

  post "zencoder-callback" => "zencoder#create", :as => "zencoder_callback"
  root :to => 'welcome#index'

  get 'video_credentials' => "video_upload_credentials#new"

end
