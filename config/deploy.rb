# https://rvm.beginrescueend.com/integration/capistrano/
$:.unshift(File.expand_path('./lib', ENV['rvm_path']))
require "rvm/capistrano"
set :rvm_ruby_string, '1.9.3-p0@vsr'
set :rvm_type, :user

set :stages, %w(production staging)
set :default_stage, "staging"
require 'capistrano/ext/multistage'

#http://gembundler.com/deploying.html
require "bundler/capistrano"

# forwards ssh authentication so it doesn't require passwords'
default_run_options[:pty] = true
set :ssh_options, {:forward_agent => true}

set :application, "vsr"
set :repository,  "deployer@184.72.154.120:git/vsr.git"

set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :deploy_to, "/home/deployer/vsr_deploy"

#role :db,  "your slave db-server here"

set :user, "deployer"
set :password, "d3pl0y3r"
set :scm_username, "deployer"

# run unicorn
set :unicorn_binary, "unicorn"
set :unicorn_config, "#{current_path}/config/unicorn.rb"
set :unicorn_pid, "#{current_path}/tmp/pids/unicorn.pid"

set :thin_pid, "#{current_path}/tmp/pids/thin.pid"

# assets precompile
load 'deploy/assets'

namespace :deploy do
  
  # override assets deplyment task
  # this runs compilation only if assets have changed

  namespace :assets do
    task :precompile, :roles => :web, :except => { :no_release => true } do
      from = source.next_revision(current_revision)
      if capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
        run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
      else
        logger.info "Skipping asset pre-compilation because there were no asset changes"
      end
    end
  end
  
  task :start, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} ls" #triggers password input
    # run "cd #{current_path} && rvmsudo bundle exec #{unicorn_binary} -E #{rails_env} -D -p 80"
    run "cd #{current_path} && rvmsudo bundle exec thin start -e #{rails_env} -d -p 80"
  end
  task :stop, :roles => :app, :except => { :no_release => true } do 
    run "#{try_sudo} ls" #triggers password input
    # run "rvmsudo kill `cat #{unicorn_pid}`"
    run "rvmsudo kill `cat #{thin_pid}`"
  end
  task :graceful_stop, :roles => :app, :except => { :no_release => true } do
    #run "rvmsudo bundle exec kill -s QUIT `cat #{unicorn_pid}`"
  end
  task :reload, :roles => :app, :except => { :no_release => true } do
    #run "rvmsudo bundle exec kill -s USR2 `cat #{unicorn_pid}`"
  end
  task :restart, :roles => :app, :except => { :no_release => true } do
    stop
    start
  end
end


after "deploy:update_code", "deploy:migrate"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
