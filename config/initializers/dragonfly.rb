require 'dragonfly'
require 'dragonfly/data_storage/custom_s3_data_store'

app = Dragonfly[:images]
app.configure_with(:imagemagick)
app.configure_with(:rails)

app.configure do |c|
  c.datastore = Dragonfly::DataStorage::CustomS3DataStore.new
  c.datastore.configure do |d|
    d.bucket_name = ENV['AWS_DIRECTORY']
    d.access_key_id = ENV['AWS_ACCESS_KEY_ID']
    d.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
  end
end

app.define_macro(ActiveRecord::Base, :image_accessor)