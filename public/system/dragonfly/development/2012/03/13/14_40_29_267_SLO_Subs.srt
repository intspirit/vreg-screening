1
00:00:42,438 --> 00:00:46,249
<i>Vojna med vampirji in �lovekom se je 
za�ela ve� stoletij pred mojim rojstvom. </i>

2
00:00:46,283 --> 00:00:50,437
<i>Njihov neskon�ni konflikt,
skrit pred ostalim svetom. </i>

3
00:00:50,822 --> 00:00:54,167
<i>Spremenila sem se v vampirja.</i>

4
00:00:56,282 --> 00:01:00,897
<i>In dobila sem nalogo, da ma��ujem 
svojo dru�ino pred volkodlaki.</i>

5
00:01:05,819 --> 00:01:09,358
<i>Bila sem zelo dobra.</i>

6
00:01:15,819 --> 00:01:19,856
<i>In potem sem 
na�la Michaela Corwina.</i>

7
00:01:20,971 --> 00:01:25,816
<i>�lovek, ki ni bil spremenjen 
v vampirja ali volkodlaka.</i>

8
00:01:25,850 --> 00:01:30,700
<i>Bil je hibrid dveh vrst
in vse se je spremenilo.</i>

9
00:01:31,969 --> 00:01:34,320
<i>Zavezniki so postali sovra�niki,</i>

10
00:01:34,354 --> 00:01:38,123
<i>vampirske stare�ine, 
ki sem jih ��itila �est stoletij,</i>

11
00:01:38,156 --> 00:01:41,199
<i>me zdaj �elijo mrtvo.</i>

12
00:01:41,276 --> 00:01:44,544
<i>Odzvali smo se.</i>

13
00:01:44,929 --> 00:01:49,083
<i>Ubili smo stare�ine 
in za nek trenutek,</i>

14
00:01:49,116 --> 00:01:52,505
<i>smo bili varni.</i>

15
00:01:54,659 --> 00:01:59,812
<i>Toda takrat se je 
vzdignila nova tema.</i>

16
00:02:00,312 --> 00:02:03,580
<i>Devet dni od odkritja 
ne�love�ke vrste,</i>

17
00:02:03,614 --> 00:02:06,904
<i>se je tako imenovana 
zaveza odkrila.</i>

18
00:02:06,938 --> 00:02:10,194
<i>�tevilke oku�enih 
so se dvignile v nebo.</i>

19
00:02:10,233 --> 00:02:14,271
<i>Voja�ki vir je potrdil, 
da glavni obrambni ukrep proti neljudem,</i>

20
00:02:14,305 --> 00:02:17,117
<i>temelji na 
UV svetlobi in srebru,</i>

21
00:02:17,155 --> 00:02:20,006
<i>na katerega so 
oku�eni usodno ob�utljivi.</i>

22
00:02:20,040 --> 00:02:24,236
<i>Postavljeni so �otori 
pri vsaki barikadi v mestu.</i>

23
00:02:24,270 --> 00:02:29,153
<i>Tam se bo testiralo in 
preverjalo, �e ste oku�eni.</i>

24
00:02:43,652 --> 00:02:46,425
<i>Z stalnimi napadi, 
uporaba UV �arkov in</i>

25
00:02:46,459 --> 00:02:50,189
<i>srebrno-nitratnega streliva,
ki je smrtonosen za oku�ene.</i>

26
00:02:50,227 --> 00:02:55,804
<i>Zaradi nevarne situacije, je bilo 
pred dvema urama razgla�eno bojevanje.</i>

27
00:02:57,996 --> 00:03:01,957
<i>Mnogi izmet nas so izgubili 
ljubljene zaradi tega ...</i>

28
00:03:04,841 --> 00:03:09,114
<i>Prosim verjemite, 
da si sami ne morejo pomagati.</i>

29
00:03:09,148 --> 00:03:12,608
<i>�okantno odkritje, da ni samo 
ena vrsta, ampak ve� razli�nih,</i>

30
00:03:12,642 --> 00:03:15,460
<i>ki so sobivale 
v �love�ki populaciji.</i>

31
00:03:15,494 --> 00:03:20,570
<i>Obstoj vampirjev in volkodlakov
ima sedaj dejansko podlago.</i>

32
00:03:20,608 --> 00:03:23,459
<i>Mesto �e vedno no�e 
dati nobene informacije,</i>

33
00:03:23,492 --> 00:03:28,492
<i>toda dobivamo poro�ila o tistem, 
�emur pravijo veliko �i��enje.</i>

34
00:03:31,952 --> 00:03:35,413
<i>Ni zdravil.</i>

35
00:04:05,025 --> 00:04:11,716
PODZEMLJE: 
<i>Prebujenje</i>

36
00:04:17,600 --> 00:04:19,489
<i>�i��enje.</i>

37
00:04:19,522 --> 00:04:23,484
<i>Ljudje niso ve�
ubijali drug drugega.</i>

38
00:04:23,518 --> 00:04:27,330
<i>Mi smo sovra�niki,
na katere so �akali.</i>

39
00:04:27,599 --> 00:04:31,829
<i>Beg nam je ostal edina 
prilo�nost, da pre�ivimo.</i>

40
00:04:31,863 --> 00:04:36,636
<i>Zato Michael 
in jaz odideva, zve�er.</i>

41
00:04:39,405 --> 00:04:41,328
Stoj!

42
00:05:13,361 --> 00:05:16,246
<i>Stoj! Na tla!

43
00:05:47,318 --> 00:05:51,241
Tukaj bodo vsak �as,
pripravite se na odhod.

44
00:05:54,201 --> 00:05:57,317
<i>Enota Tango prihaja na polo�aj.</i>

45
00:05:58,471 --> 00:06:03,854
<i>Vidimo hibrida.
-Ne streljate, ostati mora �iv.</i>

46
00:06:07,969 --> 00:06:11,430
<i>Delta na polo�aju.</i>

47
00:06:11,622 --> 00:06:15,007
<i>Alfa na polo�aju.</i>

48
00:06:17,584 --> 00:06:20,544
<i>Lansirajte srebrno granato.</i>

49
00:06:20,852 --> 00:06:23,352
Michael!
<i>-Ogenj!</i>

50
00:06:34,773 --> 00:06:37,004
<i>Alfa-4, pridi.</i>

51
00:07:17,575 --> 00:07:20,536
<i>Poi��ite telo.

52
00:07:40,648 --> 00:07:48,147
<i>Subjekt 2 je pobegnil, 
ogro�ena je varnost!</i>

53
00:08:00,837 --> 00:08:02,727
<i>Odmrzovanje se je za�elo.</i>

54
00:08:02,761 --> 00:08:06,529
<i>Zmanj�ujem pritisk 
v krio-komori subjekta 1.</i>

55
00:08:11,913 --> 00:08:15,836
<i>Odmrzovanje kon�ano.</i>

56
00:09:16,520 --> 00:09:18,789
<i>Kr�itev varnosti na stopnji 6.</i>

57
00:09:18,827 --> 00:09:23,058
<i>Vse osebje 
naj gre na stopnjo 6. </i>

58
00:09:40,517 --> 00:09:44,977
Jacob ... -Subjekt 2 je pobegnil, 
�e vedno je nekje v zgradbi. 

59
00:09:46,592 --> 00:09:49,443
�elim subjekt 1 
pod popolnim varovanjem. 

60
00:09:49,476 --> 00:09:53,322
Kak�na je situacija? -Subjekt 1 se je
osvobodil, takoj ga moramo se�gati. 

61
00:09:53,356 --> 00:09:56,129
Ne, naj vidim. 

62
00:10:06,128 --> 00:10:09,742
Ponovno ga uspavajte. 

63
00:10:10,358 --> 00:10:12,935
In pohitite. 

64
00:10:14,204 --> 00:10:17,664
<i>Oddajam najvi�ji 
odmerek Fentanyla. </i>

65
00:10:38,047 --> 00:10:42,431
Kaj je ta kraj?
Kje je Michael?!

66
00:11:12,926 --> 00:11:17,041
<i>Vidim ga!</i>
Stoj, ne premikaj se!

67
00:11:26,193 --> 00:11:28,276
Ve� �rtev na stopnji 6!

68
00:11:28,309 --> 00:11:30,481
Medicinsko osebje 
v opozorilno stanje!

69
00:11:30,482 --> 00:11:32,654
Pripravite se na popolno
omejitev na stopnji 6!

70
00:11:32,688 --> 00:11:36,270
<i>Ponavljam, popolna
omejitev na stopnji 6!</i>

71
00:11:55,036 --> 00:11:57,117
Na vzhodnem hodniku je,
po�ljite zavarovanje.

72
00:11:57,150 --> 00:12:00,227
<i>Pustite jo, mogo�e nas 
lahko pripelje do subjekta 2. </i>

73
00:12:00,261 --> 00:12:04,342
Ne streljajte! 
Pustite jo. 

74
00:12:33,415 --> 00:12:36,952
<i>Subjekt 1 in subjekt 2 
sta pobegnila ... </i>

75
00:14:18,169 --> 00:14:22,900
Kako si pri�la tja?! 
Ne sme� biti tu. 

76
00:14:22,934 --> 00:14:26,938
Podro�je je zaprto.
-Od kdaj?

77
00:14:26,971 --> 00:14:30,860
Dolgo nazaj. Pridi. 

78
00:14:30,898 --> 00:14:34,018
Kako dolgo?

79
00:14:34,052 --> 00:14:39,436
Ve� kot 12 let, 
od �i��enja. Zakaj?

80
00:14:39,820 --> 00:14:42,012
Ste v redu, gospa?
<i>-Pozor vsem enotam,</i>

81
00:14:42,051 --> 00:14:46,242
<i>�ifra 3, imamo poro�ilo 
o napadu, izhod 39, podhod.</i>

82
00:14:46,281 --> 00:14:49,401
<i>Mogo�e so ne�love�ki napadalci, 
bodite previdni.</i>

83
00:14:49,434 --> 00:14:53,473
<i>Ekipe za nalezljive bolezni so 
�e na poti. Vse enote, javite se. </i>

84
00:14:54,703 --> 00:14:58,395
Gospa?!

85
00:15:48,656 --> 00:15:51,431
Kdorkoli je bil,
ni mogel priti dale�.

86
00:15:51,464 --> 00:15:54,771
Obvestil bom CZB, 
naj bodo pozorni. 

87
00:15:54,805 --> 00:15:58,460
Ne verjamem, da je to 
naredil �lovek. -Ne ve� �e. 

88
00:15:58,494 --> 00:16:02,082
Ne vem �esa? �lovek je mrtev, 
grlo ima tam v jarku.

89
00:16:02,116 --> 00:16:06,346
Pusti mi, da re�im to. -Kaj boste
re�ili? Vidi se, da je bil volkodlak. 

90
00:16:06,379 --> 00:16:11,615
Detektiv, kdaj si nazadnje 
sli�al o napadih volkodlakov?

91
00:16:11,654 --> 00:16:16,153
Pred ve� leti?
-In koliko �asa si v tem oddelku?

92
00:16:16,186 --> 00:16:19,399
Kako mislite? 
Tukaj sem tri mesece. -To�no. 

93
00:16:19,432 --> 00:16:22,614
Spra�ujte me, 
potem ko bo� vedel kaj ve�.

94
00:16:22,647 --> 00:16:25,843
Jasno?
-Kristalno. 

95
00:16:27,998 --> 00:16:30,959
Detektiv Sebastian.
<i>-Pravkar smo prejeli poro�ilo</i>

96
00:16:30,993 --> 00:16:33,616
<i>o neredu v Antigen Labsu.</i>

97
00:16:33,650 --> 00:16:35,689
<i>O�ividec trdi, 
da je mogo�e nekdo pobegnil,</i>

98
00:16:35,722 --> 00:16:37,654
<i>da je sko�il skozi okno.</i>

99
00:16:37,687 --> 00:16:41,918
<i>To je prednostna preiskava,
�im prej vas potrebujemo tam. </i>

100
00:16:41,952 --> 00:16:44,961
Pridemo. Gremo.

101
00:16:44,995 --> 00:16:47,533
Johnson, ti si glavni.

102
00:17:03,261 --> 00:17:05,953
Kdo je glavni?

103
00:17:06,338 --> 00:17:10,375
Ja? - Detektiv Sebastian 
vas ho�e videti, gospod. 

104
00:17:12,030 --> 00:17:16,606
Kje je?
-V preddverju. 

105
00:17:19,605 --> 00:17:22,758
Rad bi vam povedal,
da imamo situacijo pod nadzorom.

106
00:17:22,797 --> 00:17:24,797
Kaj se je zgodilo?
-Ponavljam, vse imamo pod nadzorom. 

107
00:17:24,831 --> 00:17:28,528
Ponavljam, kaj se je zgodilo?

108
00:17:28,566 --> 00:17:31,223
Moram predlo�iti poro�ila. 

109
00:17:31,257 --> 00:17:33,527
Eden od na�ih testnih subjektov 
je posku�al pobegniti.

110
00:17:33,565 --> 00:17:37,487
Morali smo ga ubiti.
-Potem ni ni� pri�lo ven?

111
00:17:37,521 --> 00:17:41,415
Samo izstreljeni metek.
-Testni subjekt? -Se�gan. 

112
00:17:41,449 --> 00:17:45,337
Je to vse? Moram se vrniti 
nazaj na delo, saj veste. 

113
00:17:45,371 --> 00:17:49,793
Doktor, prejeli smo anonimno prijavo,
da je nekaj drugega u�lo ven. 

114
00:17:49,827 --> 00:17:53,905
Detektiv, na�a naloga tukaj
v Antigenu je razvijanje na�ina

115
00:17:53,939 --> 00:17:57,984
za prepre�evanje oku�be, 
ne sprostitve med prebivalstvom.

116
00:17:58,018 --> 00:18:00,719
Zaradi te bolezni 
sem izgubil sina. 

117
00:18:00,753 --> 00:18:04,907
Zame ni napake, 
ko gre za varnost javnosti.

118
00:18:04,941 --> 00:18:08,444
�e je res nekaj pobegnilo,
potem bi bile oblasti obve��ene. 

119
00:18:08,478 --> 00:18:11,713
Vi ste del oblasti, kajne?

120
00:18:11,747 --> 00:18:14,982
Zdaj pa mi oprostite.

121
00:18:36,633 --> 00:18:39,978
Glavni pravi, 
da ni ni�. Gremo. 

122
00:18:40,012 --> 00:18:43,517
Ja, sranje. 
Poglejte to. 

123
00:18:44,209 --> 00:18:48,900
�elite, da prijavim? -Ne, jaz bom.
-Ne moti me, lahko namesto vas.

124
00:18:51,899 --> 00:18:54,669
V redu, gospod. 

125
00:19:16,051 --> 00:19:20,665
Zapornik, ki mi je pomagal pobegniti, 
tisti ki mu pravite subjekt 2 ... 

126
00:19:20,935 --> 00:19:24,587
Je bil hibrid?
-Ja!

127
00:19:24,621 --> 00:19:28,708
Kaj ste naredili?

128
00:19:28,741 --> 00:19:31,778
Lahko pomagamo vladi 
pri prepoznavanju oku�be.

129
00:19:31,817 --> 00:19:34,664
Uporabili smo va� DNK,
da najdemo zdravilo.

130
00:19:34,702 --> 00:19:37,137
Da bi prepre�ili 
novo epidemijo.

131
00:19:37,170 --> 00:19:41,201
Naro�eno vam je bilo, da vas 
jaz peljem do hibrida. Zakaj?

132
00:19:41,239 --> 00:19:46,046
Informacije o subjektu 2 so
za��itene, nimam dovoljenja. 

133
00:19:46,080 --> 00:19:48,893
La�ete!

134
00:19:50,930 --> 00:19:52,820
Ne, prosim te!

135
00:19:52,853 --> 00:19:55,627
Vem samo, da obstaja 
nek odnos med vama!

136
00:19:55,660 --> 00:19:59,313
Va�i mo�ganski valovi so sinhronizirani,
vsaki� ko sta si blizu.

137
00:19:59,347 --> 00:20:02,467
Lahko vidi�, 
kar vidi on!

138
00:20:02,852 --> 00:20:05,736
Prosim te, 
jaz sem te izpustil!

139
00:20:05,770 --> 00:20:08,428
Zdaj sva si bot. 

140
00:20:39,077 --> 00:20:42,731
Kaj za vraga je bilo to?
-Grozljivo.

141
00:20:42,769 --> 00:20:45,346
Prijavil bom.
-Gremo. 

142
00:21:42,530 --> 00:21:44,953
Ne streljaj.
-Zakaj ne?

143
00:21:44,986 --> 00:21:47,721
Enaka sva. 

144
00:21:49,645 --> 00:21:53,336
Zakaj mi sledi�?
-Sledimo policijskim frekvencam. 

145
00:21:53,413 --> 00:21:58,220
Mi? -Ve�, kaj je napadlo 
tistega �lovek pod mostom?

146
00:21:58,254 --> 00:22:00,643
Ne. 

147
00:22:03,028 --> 00:22:06,565
Volkodlaki.
-Kar jih je �e ostalo. 

148
00:22:08,334 --> 00:22:11,295
Skrivajo se vse 
od �i��enja podgan.

149
00:22:11,329 --> 00:22:14,838
Bolani in la�ni, 
le redko pridejo na povr�je. 

150
00:22:14,872 --> 00:22:18,601
Kar pomeni, 
da jih je nekaj prebudilo.

151
00:22:18,635 --> 00:22:22,063
Nekaj, kar mi ne pove�.

152
00:22:34,714 --> 00:22:36,799
Michael!

153
00:23:17,440 --> 00:23:21,400
Jo pozna�?
-Ne. 

154
00:23:26,938 --> 00:23:32,554
Vrnili sem bodo nazaj.
-Ne moremo je pustiti tukaj. 

155
00:23:48,274 --> 00:23:50,319
Vozi kombi. 

156
00:23:55,396 --> 00:23:57,550
Spravi se, hitro!

157
00:24:13,539 --> 00:24:16,240
Trije gredo za nama.

158
00:25:38,689 --> 00:25:41,650
Prekleto!

159
00:26:21,668 --> 00:26:23,759
Ti vozi. 

160
00:27:09,215 --> 00:27:12,791
Ne zaceli se.
-Imamo zdravnika, ki bo pomagal. 

161
00:27:12,824 --> 00:27:17,905
V na�em klanu.
-Kako si me na�la?

162
00:27:19,059 --> 00:27:22,564
Kako ti je ime?

163
00:27:22,598 --> 00:27:26,135
Subjekt 2, 
in ti si subjekt 1. 

164
00:27:26,443 --> 00:27:29,712
Osvobodila sem te. 

165
00:27:30,596 --> 00:27:33,673
Zakaj ne ve�, 
kdo sem jaz?

166
00:27:33,750 --> 00:27:39,595
Rekli so mi, da nimam matere,
da je umrla ko sem se rodil. 

167
00:27:40,480 --> 00:27:45,060
Nikoli nisem verjela v to,
pomagala sem ti ... 

168
00:27:45,094 --> 00:27:51,440
Videla sem slike v o�eh.
In potem je bil nekdo pred vrati. 

169
00:27:51,473 --> 00:27:56,631
Rekli so mi, da nisem ve� otrok
in da je �akanje kon�ano. 

170
00:27:57,284 --> 00:28:02,591
Skoraj bi umrla, vendar moja mati, 
ki je bila zraven mene,

171
00:28:02,625 --> 00:28:06,437
nikoli ni vedela, 
da sem obstajala.

172
00:28:06,822 --> 00:28:10,206
Kaj so ti povedali o o�etu?

173
00:28:10,283 --> 00:28:14,129
Nekaj so ti gotovo povedali.
Si videl kak�no njegovo sliko?

174
00:28:14,162 --> 00:28:18,167
Ne, samo tvojo. 

175
00:28:34,433 --> 00:28:38,279
Nikoli ni stopila ven iz laboratorija.
Kako naj bi sploh pre�ivela?

176
00:28:38,313 --> 00:28:41,014
Razumem tveganje, �e povemo 
oblastem, da je izginila.

177
00:28:41,048 --> 00:28:44,586
Ni podatkov o njenem rojstvu,
zato se bodo pojavljala vpra�anja.

178
00:28:44,625 --> 00:28:47,277
�e pa umre, 
vsa leta na�ih raziskav ...

179
00:28:47,316 --> 00:28:50,893
Ne govori mi, 
kako naj opravljam svoje delo.

180
00:28:51,231 --> 00:28:54,676
Va�a stalna potreba za ustvarjanje 
njenih �love�kih lastnosti,

181
00:28:54,710 --> 00:28:57,813
je privedla do pomanjkljivosti
pri varnostnem protokolu.

182
00:28:57,846 --> 00:29:02,076
Ona ni �lovek! 
Ni tvoj otrok, ni ljubka!

183
00:29:02,110 --> 00:29:05,352
Ona je kuga narave.

184
00:29:07,575 --> 00:29:10,020
Nehaj skrbeti, 
vrnila se bo.

185
00:30:39,031 --> 00:30:41,993
Takoj pokli�ite Olivio!

186
00:30:44,414 --> 00:30:48,297
<i>Znova si prekr�il pravila.
Zakaj? -Bila je napadena.</i>

186
00:30:48,814 --> 00:30:52,297
<i>Ne celi se. -In v zameno 
si ji ponudil na�e varstvo.</i>

186
00:30:52,414 --> 00:30:55,897
<i>Ni potrebe, 
da se nas bojite.</i>

186
00:30:58,514 --> 00:31:00,997
Misli�, da sem tako neumen,
da ti bom verjel na besedo?

187
00:31:03,836 --> 00:31:07,724
Ni razloga, da bi se bal vampirke,
ki se je zaljubila v likana?

188
00:31:07,758 --> 00:31:14,295
In ubila dve na�i stare�ini?
In na vsakem koraku izdaja svojo vrsto.

189
00:31:15,334 --> 00:31:17,839
Ve�, kdo je to?
-Vem. 

190
00:31:17,873 --> 00:31:21,218
Vendar smo mi 
izdali njenega o�eta. 

191
00:31:21,795 --> 00:31:24,948
Spustite jo. 

192
00:31:24,987 --> 00:31:28,640
Ne celi se. 

193
00:31:37,177 --> 00:31:42,369
Napadel jo je nizki volkodlak, 
pred dvema urama. 

194
00:31:43,523 --> 00:31:49,790
Obnavljanje njenega telesa
je aktivno, toda oslabljeno. 

195
00:31:51,598 --> 00:31:58,251
Nima sledov ugriza, 
nobenih brazgotin.

196
00:31:58,904 --> 00:32:02,448
Nikoli �e nisem 
videla tak�nega otroka.

197
00:32:02,482 --> 00:32:06,712
Kdaj si se nazadnje hranila?
-Hranila?

198
00:32:07,096 --> 00:32:10,173
Daj ji kri. 

199
00:32:17,171 --> 00:32:21,324
Otrok, mora�. 

200
00:32:38,784 --> 00:32:42,475
Po�asi ... 

201
00:32:42,860 --> 00:32:47,629
Tako ... 

202
00:32:50,436 --> 00:32:56,171
Celi se in to hitro.
-Dobro. 

203
00:32:56,204 --> 00:33:00,166
Peljite jo v mojo sobo,
mora se spo�iti. -Ne. 

204
00:33:00,200 --> 00:33:03,896
Poglej njene o�i. 

205
00:33:11,317 --> 00:33:15,932
Ne morem verjeti.
Kljub vsemu. 

206
00:33:16,471 --> 00:33:21,701
To ni eden od nas.
-Ne, nekaj ve� je, o�e. 

207
00:33:23,700 --> 00:33:27,084
Kaj ti ve� o njej?

208
00:33:28,008 --> 00:33:31,392
Bila sem ujeta v stavbi 
pod imenom Antigen. 

209
00:33:31,426 --> 00:33:34,292
Eden od drugih zapornikov 
me je osvobodil.

210
00:33:34,325 --> 00:33:37,126
Mislila sem,
da je to Michael Corvin, 

211
00:33:37,160 --> 00:33:39,545
edini hibrid, 
za katerega sem vedela. 

212
00:33:39,583 --> 00:33:44,737
Tvoj ljubljeni volkodlak je �e
davno mrtev. Kaj je potem to tukaj?

213
00:33:44,775 --> 00:33:47,317
To se te ne ti�e. 

214
00:33:47,351 --> 00:33:51,466
Takoj, ko se vrne 
njena mo�, bova od�li.

215
00:34:26,691 --> 00:34:30,538
Sli�al sem govorice 
o trgovcih smrti. 

216
00:34:30,572 --> 00:34:33,957
Da so stvar preteklosti.

217
00:34:33,991 --> 00:34:37,311
Zdaj pa si se vrnila.

218
00:34:37,343 --> 00:34:40,420
Vesel sem, 
da sem se motil.

219
00:34:40,454 --> 00:34:43,997
Videl si, kdo sem. 

220
00:34:44,150 --> 00:34:47,728
Vseeno si me pripeljal sem.

221
00:34:48,611 --> 00:34:50,577
Hvala ti. 

222
00:34:50,611 --> 00:34:55,303
Ti si borka, 
radi bi, da nas nau�i�.

223
00:34:55,342 --> 00:34:59,264
Nisem prepri�ana, da bi se tvoj o�e
strinjal. -Bili so �asi, ko se je, 

224
00:34:59,297 --> 00:35:03,114
zato te tudi tako sovra�i.
Mislim, da ga spominja� na dni,

225
00:35:03,147 --> 00:35:07,070
ko nas je branil 
z ognjem in �elezom. 

226
00:35:07,104 --> 00:35:10,032
Ve, kako zelo te potrebujemo.

227
00:35:10,416 --> 00:35:15,684
Jaz nisem vodja.
-Toda bori� se za tisto, kar verjame�. 

228
00:35:15,722 --> 00:35:20,145
Ne vem ve�, kaj naj verjamem.
Sploh ne poznam ve� tega sveta. 

229
00:35:20,179 --> 00:35:23,073
Nisi se spremenila.

230
00:35:23,107 --> 00:35:26,299
Videl se tvoje o�i,
kako si pogledala otroka. 

231
00:35:26,332 --> 00:35:32,529
Za��itila jo bo�.
To ima� v krvi, to si ti.

232
00:35:33,297 --> 00:35:37,528
Ko bo� od�la, 
vzemi me s sabo.

233
00:36:37,443 --> 00:36:41,442
Ne zaupa� mi. 

234
00:36:45,710 --> 00:36:48,980
Seveda ne,
ker ti nisem za�elel dobrodo�lice.

235
00:36:49,014 --> 00:36:52,440
�al mi je zaradi tega. 

236
00:36:55,401 --> 00:37:01,439
Te o�i. 
Posebne so. 

237
00:37:01,478 --> 00:37:06,362
To�no vem, kaj si. 

238
00:37:16,475 --> 00:37:19,980
Moj o�e pravi, �e pre�ivimo 
kot vrsta, bodo ti ostanki

239
00:37:20,014 --> 00:37:23,590
pomagali na�im potomcem izvedeti,
kdo smo bili in kako smo �iveli.

240
00:37:23,623 --> 00:37:27,337
Kaj pa ti pravi�?
-�e bomo �e vedno �iveli tako, 

241
00:37:27,370 --> 00:37:31,050
sramotimo preteklost 
in si ne zaslu�imo prihodnosti.

242
00:37:31,088 --> 00:37:35,781
Moramo se upreti ljudem,
kadarkoli in kjerkoli je mogo�e.

243
00:37:35,814 --> 00:37:38,439
Med prvim in drugim �i��enjem,

244
00:37:38,472 --> 00:37:43,279
mladi volkodlaki so se zbrali na 
enak na�in, kljub nesoglasju stare�in.

245
00:37:43,312 --> 00:37:46,664
Njihovi kriki po nasilju 
so odmevali dan za dnem.

246
00:37:46,698 --> 00:37:51,931
Rezultat. 
Volkodlaki so skoraj izumrli.

247
00:37:51,965 --> 00:37:55,662
To nam �eli�? 
Izumrtje?

248
00:37:55,701 --> 00:37:58,436
In vendar si pripeljal 
njihovo potomko v na� dom.

249
00:37:58,469 --> 00:38:02,199
�e vedno verjame�, da bodo ljudje 
dovolili, da ostane svobodna?

250
00:38:02,238 --> 00:38:05,584
Lovili jo bodo na vsakem koraku.
-Potem jo lahko za��itimo. 

251
00:38:05,617 --> 00:38:09,890
Za��itimo?! 
Zve�er odide�. 

252
00:38:09,929 --> 00:38:14,236
To ni zdrav razum, 
ampak navadna strahopetnost.

253
00:38:14,813 --> 00:38:19,928
Kako si drzne�?
-Kako si ti drzne�?

254
00:38:20,197 --> 00:38:23,658
Tako malo razmi�lja�.

255
00:38:51,347 --> 00:38:55,192
Sanjala sem o dnevu, 
ko te bom spoznala. 

256
00:38:56,461 --> 00:39:00,191
Kot neumna deklica.

257
00:39:04,499 --> 00:39:10,113
Hladna si, 
kot da si �e mrtva.

258
00:39:11,729 --> 00:39:17,805
V�eraj sem bila
s tvojim o�etom. 

259
00:39:18,842 --> 00:39:23,381
Bil je samo malce bolj stran, 
ko si ti zdaj. 

260
00:39:24,343 --> 00:39:27,846
Zaspala sem ... 

261
00:39:27,880 --> 00:39:31,230
In ko sem se naslednji 
dan prebudila ... 

262
00:39:31,263 --> 00:39:37,572
Spoznala sem, 
da je �ez no� minilo 12 let. 

263
00:39:37,878 --> 00:39:42,494
In namesto edinega �loveka,
ki sem ga kdajkoli ljubila, 

264
00:39:42,528 --> 00:39:46,339
je tam deklica 
z njegovimi o�mi. 

265
00:39:49,031 --> 00:39:54,415
Moje srce ni hladno, 
ampak zlomljeno. 

266
00:39:57,030 --> 00:40:01,530
Tako mi je �al. 

267
00:40:08,644 --> 00:40:11,431
Kaj je?

268
00:40:11,465 --> 00:40:14,187
Tukaj so. 

269
00:40:14,220 --> 00:40:19,186
Ostani tu, po�akaj me.
-Vzemi me s sabo, prosim te. 

270
00:40:19,219 --> 00:40:24,910
Ne bom dovolila, da te kdo rani,
obljubim. Zdaj pa zapri vrata.

271
00:40:25,564 --> 00:40:28,755
Vsi v zato�i��e, takoj!

272
00:40:29,101 --> 00:40:32,333
Ne! Pripravite oro�je!

273
00:40:32,367 --> 00:40:35,529
Naredite, kot sem rekel. 
Gremo!

274
00:40:35,562 --> 00:40:42,100
Mene poslu�ajo!
-Ne, ostali bomo in se borili!

275
00:40:56,329 --> 00:40:59,713
Ne! Ste znoreli?!

276
00:41:02,290 --> 00:41:05,140
Ustavite se!

277
00:41:05,174 --> 00:41:08,558
Ve� let smo v miru �iveli tukaj,
zdaj pa si pripeljala ljudi.

278
00:41:08,592 --> 00:41:11,520
�elijo deklico 
in ni� jih ne bo ustavilo!

279
00:41:11,553 --> 00:41:14,095
Na ta dan smo �akali,
z vami ali brez.

280
00:41:14,134 --> 00:41:18,249
Gremo! Strelci, gremo!
-Si znorel?!

281
00:41:18,283 --> 00:41:22,363
Imamo samo pe��ico vojakov.
Uni�ili nas bodo!

282
00:41:22,402 --> 00:41:26,632
Tak�en je tvoj na�rt, o�e?
Gremo, na svoja mesta!

283
00:41:27,093 --> 00:41:29,291
Kam gre�?
-Odpeljala jo bom od tukaj. 

284
00:41:29,325 --> 00:41:31,439
Tvoj o�e ima prav, 
ne bi smela priti. 

285
00:41:31,473 --> 00:41:33,554
On ne razume ...
-Ljudje nama bodo sledili, 

286
00:41:33,588 --> 00:41:36,097
morate jih samo 
toliko zadr�ati. 

287
00:41:36,131 --> 00:41:42,669
Pri�li so samo po njo. Mo�nej�a je od nas. 
Bojim se dneva, ko bo to izvedela.

288
00:41:42,703 --> 00:41:46,399
Moram jo odpeljati. 

289
00:42:21,125 --> 00:42:24,124
Volkodlaki!

290
00:42:26,393 --> 00:42:29,201
Srebrno strelivo! 
Takoj!

291
00:42:29,234 --> 00:42:32,008
Branite drugo Rotondo! 
Gremo!

292
00:43:15,310 --> 00:43:17,617
Odpri vrata. 

292
00:43:18,310 --> 00:43:20,617
Greva.

293
00:44:22,069 --> 00:44:24,454
Morava iti. 

294
00:44:47,759 --> 00:44:51,604
Kaj je bilo to?

295
00:44:52,258 --> 00:44:56,488
Pelji jo na varno.
-Ne. -Pelji jo!

296
00:47:31,350 --> 00:47:35,966
Kje je?
-Odpeljali so jo. 

297
00:47:36,004 --> 00:47:42,426
Sprejeli so jo, 
kot moje darilo njim.

298
00:48:19,421 --> 00:48:22,996
Sploh ve�, kaj si naredil?
Ima� kaj pojma?!

299
00:48:23,030 --> 00:48:26,574
Kaj si hotela, da naj naredim?
Da se borimo do zadnjega,

300
00:48:26,608 --> 00:48:30,001
da za��itimo nekoga, 
ki je svarilo vsem nam?

301
00:48:30,035 --> 00:48:35,495
Pora�eni smo. Na�a krinka 
je uni�ena, moj sin je mrtev. 

302
00:48:35,529 --> 00:48:38,108
Zaradi tebe. 

303
00:48:38,141 --> 00:48:40,653
Zakaj?

304
00:48:40,686 --> 00:48:45,878
Zato, ker je pomagal tvojemu dekletcu.
Prav bo, �e jo ubijejo.

305
00:48:45,912 --> 00:48:49,971
Tvoj sin je umrl,
ker se je boril za vse nas. 

306
00:48:50,004 --> 00:48:54,030
Vedel je, 
da beg ne prinese pre�ivetja. 

307
00:48:54,069 --> 00:48:59,800
Bili so volkodlaki, ki niso izumrli.
Bili so mo�nej�i, kot kadarkoli.

308
00:48:59,838 --> 00:49:04,337
Ta stvar je bila dvakrat ve�ja od 
volkodlaka, ki sem ga kdajkoli videla. 

309
00:49:04,371 --> 00:49:09,337
Tvegajo svojo pojavo, 
po ve�ih letih skrivanja. Zaradi nje. 

310
00:49:09,370 --> 00:49:13,259
In ti si jim jo predal. 

311
00:49:13,297 --> 00:49:18,951
Ona je zadnja neposredna
potomka Alexandra Corvinusa.

312
00:49:18,985 --> 00:49:23,566
Edini otro�ki hibrid, 
ki je kdajkoli obstajal.

313
00:49:24,027 --> 00:49:27,762
Kar so volkodlaki 
hoteli od nje, 

314
00:49:27,796 --> 00:49:31,334
tvoj sin je vedel, 
da je zaradi tega vredno umreti. 

315
00:49:36,179 --> 00:49:39,024
Kaj po�ne�?

316
00:50:06,444 --> 00:50:10,020
David?

317
00:51:18,857 --> 00:51:24,395
Dvajset volkodlakov nas je napadlo 
v�eraj in uni�ilo eno od na�ih zavetij.

318
00:51:25,472 --> 00:51:29,009
Nisi presene�en. 

319
00:51:29,048 --> 00:51:32,168
Vedel si, da volkodlaki 
niso izumrli. Kako?

320
00:51:32,201 --> 00:51:36,489
Samo slutil sem.
-Nisem dobra v slutnjah. 

321
00:51:36,523 --> 00:51:40,776
�elim vedeti, kako.
-To ni prostor za to. 

322
00:51:40,810 --> 00:51:44,589
In ne bo� me ubila.
-Res?

323
00:51:44,623 --> 00:51:48,621
Zato, ker za to ne 
potrebuje� preklete pi�tole. 

324
00:51:49,776 --> 00:51:53,851
Pred nekaj leti je vlada objavila,
da volkodlaki niso na�a tar�a. 

325
00:51:53,885 --> 00:51:58,352
Federalci so objavili, 
da se osredoto�imo samo na tvojo vrsto. 

326
00:51:58,385 --> 00:52:02,004
Pred tremi meseci 
mi je prijatelj poslal to. 

327
00:52:02,312 --> 00:52:07,080
Prispel je dva dni prepozno.
Mrtev je visel iz ventilatorja.

328
00:52:08,081 --> 00:52:11,584
Dvesto sumov volkodlakov.
Vsak primer je bil preiskan,

329
00:52:11,618 --> 00:52:15,464
vsaka krvna slika je bila negativna.
Moj prijatelj se je za�el spra�evati.

330
00:52:15,498 --> 00:52:19,771
S vsemi temi la�nimi stvarmi, 
nekdo posku�a zavarovati volkodlake. 

331
00:52:20,079 --> 00:52:22,464
Pomagal izpolniti delo,
jim omogo�i, da se zdru�ijo.

332
00:52:22,502 --> 00:52:26,155
In tvoj oddelek?

333
00:52:26,540 --> 00:52:31,616
Imajo mojo h�er. 
Ho�em samo, da jo vrnejo. 

334
00:52:32,193 --> 00:52:37,077
�e �eli� koga za��ititi, 
ni mi mar. Samo njo �elim.

335
00:52:37,115 --> 00:52:39,004
Ne. 

336
00:52:39,038 --> 00:52:43,076
Tudi �e bi jih imeli v mojem
oddelku, ne bi imel pooblastila.

337
00:52:43,110 --> 00:52:48,345
To pelje veliko vi�je,
mislim da vse do vrha. 

338
00:52:49,114 --> 00:52:52,382
Ali pa do dna.
-Kako misli�?

339
00:52:52,416 --> 00:52:55,617
Kdo vodi testiranja?

340
00:52:55,651 --> 00:52:59,497
Mudi se nam, 
zato si prosim poglejte to. 

341
00:52:59,612 --> 00:53:02,650
Ta razpored vas bo vodil 
od konca dana�njega sestanka,

342
00:53:02,688 --> 00:53:05,731
pa vse do kon�ne 
proizvodnje cepiva. 

343
00:53:05,765 --> 00:53:09,879
�e se dr�imo tega na�rta, 
bi se lahko cepljenje za�elo �ez tri mesece.

344
00:53:09,913 --> 00:53:11,961
�e ima kdo vpra�anje, 
zdaj je pravi �as. 

345
00:53:11,995 --> 00:53:15,149
Od dana�nje operacije naprej
ne bo nobene komunikacije med nami,

346
00:53:15,187 --> 00:53:17,379
dokler se ponovne ne sestanemo
v centru za cepljenje.

347
00:53:17,413 --> 00:53:20,456
Doktor, kar se ti�e 
testnega subjekta. 

348
00:53:20,489 --> 00:53:23,036
�e vedno dokazuje 
odpornost proti srebru?

349
00:53:23,070 --> 00:53:29,223
Seveda. In dodatna cepiva,
uporabljena le iz previdnosti,

350
00:53:29,257 --> 00:53:31,530
so spro�ila stranske u�inke,
ki si jih nismo niti predstavljali.

351
00:53:31,564 --> 00:53:35,046
Ogromno pove�anje mo�i, 
mi�i�ne mase,

352
00:53:35,080 --> 00:53:38,530
samozaceljivost, vzdr�ljivost ... 

353
00:53:39,684 --> 00:53:41,913
Rekel si, da je 
nujna za na�e raziskave!

354
00:53:41,947 --> 00:53:45,226
Ja, in �e naprej bo.
-Kako?

355
00:53:45,259 --> 00:53:48,989
Tako, da jo bo� seciral?
-Ne bi smela dobiti tega. 

356
00:53:49,027 --> 00:53:52,240
Borim se za to deklico,
vsa ta leta,

357
00:53:52,273 --> 00:53:55,450
in ti misli�, da bom samo 
stali zadaj, dokler ti ... 

358
00:53:55,484 --> 00:53:58,334
Zbiram njen
genetski material?

359
00:53:58,368 --> 00:54:01,296
Ni� ne pri�akujem od tebe. 

360
00:54:01,334 --> 00:54:06,603
Zaposlil sem te, da pazi� nanjo,
in to si v ve�ini naredila odli�no.

361
00:54:06,636 --> 00:54:11,525
Re�ila si tiso�e ljudi.
-O �em govori�?

362
00:54:11,558 --> 00:54:16,216
Cela vrsta 
re�ena pred izumrtjem.

363
00:55:03,980 --> 00:55:08,517
Od danes naprej ti ne bo treba
�akati tako dolgo za injekcijo. 

364
00:55:08,551 --> 00:55:10,713
Si prepri�an, 
da je pripravljena?

365
00:55:10,747 --> 00:55:15,516
Raven estrogena je visoka, 
�asovna razlika med obdobji je stabilna ...

366
00:55:15,555 --> 00:55:22,054
Dozorela je. Vse se je razpletlo 
tako, kot smo predvidevali.

367
00:55:25,130 --> 00:55:28,976
Ti si prvi dokaz. 

368
00:55:29,014 --> 00:55:33,206
Ponosen sem nate, sin. 

369
00:55:34,668 --> 00:55:37,455
Zdaj ko je ponovno z nama,
imava ves potreben

370
00:55:37,488 --> 00:55:40,210
genetski material,
pripravljen za kloniranje. 

371
00:55:40,244 --> 00:55:44,281
Dovolj za proizvodnjo
neskon�ne zaloge cepiva.

372
00:55:44,358 --> 00:55:48,862
Zamisli si, 
na�a vrsta je odporna na srebro. 

373
00:55:48,896 --> 00:55:54,857
Ni� ve� skrivanja. Vsak volkodlak pod 
na�o oblastjo, bo tako mo�en, kot si ti. 

374
00:55:54,890 --> 00:55:58,142
Tudi sebi se ga za�el dajati.

375
00:55:58,176 --> 00:56:01,765
Samo �elim si, da bi bila 
mati tu in bi videla vse to. 

376
00:56:01,799 --> 00:56:05,355
Tvoja mati je
hotela ostati �lovek. 

377
00:56:05,389 --> 00:56:08,700
Ona je zapustila nas. 

378
00:56:09,354 --> 00:56:12,277
Zapomni si to. 

379
00:56:24,584 --> 00:56:27,659
Seveda, o�e. 

380
00:56:33,121 --> 00:56:37,428
�e ima� prav o Antigeni, samo bog ve, 
koliko volkodlakov se skriva notri. 

381
00:56:38,313 --> 00:56:43,312
Takoj ko pride� noter, 
bo� obkoljena. -Upam tako. 

382
00:56:43,580 --> 00:56:46,811
Vem, da jo �eli� nazaj.
Lahko ti pomagam. 

383
00:56:46,844 --> 00:56:50,041
Zakaj? -Samo povej mi, 
kak�en na�rt ima�. 

384
00:57:12,922 --> 00:57:18,306
Slab�e bo, 
�e se bo� upirala. 

385
00:57:20,036 --> 00:57:25,343
Kako to,
da lahko pre�ivi� na svetlobi?

386
00:57:25,613 --> 00:57:31,112
To je dar, od �loveka po
imenu Alexander Corvinus. 

387
00:57:31,146 --> 00:57:34,573
O�e ustanovitelj.

388
00:57:35,881 --> 00:57:39,726
Bil sem poro�en 
z medicinsko sestro. 

389
00:57:40,419 --> 00:57:43,846
Enega dne,
v slu�bi je bila ugrizena. 

390
00:57:43,879 --> 00:57:49,187
Nih�e ni vedel, razen nas.
Nekaj let smo �iveli tako.

391
00:57:49,726 --> 00:57:52,532
In potem 
je pri�lo �i��enje. 

392
00:57:52,801 --> 00:57:57,647
Federalci so �li od vrata do vrata, 
in ko so potrkali na na�a ... 

393
00:58:02,031 --> 00:58:06,261
Rekla je, da me ljubi ... 

394
00:58:06,300 --> 00:58:10,188
Posijalo je sonce.

395
00:58:10,222 --> 00:58:13,491
Moral sem gledati,
kako gori. 

396
00:58:36,102 --> 00:58:40,333
Me sli�i�?
-Ja. 

397
00:58:40,487 --> 00:58:42,794
Sre�no. 

398
00:58:55,985 --> 00:58:58,792
Sranje! To je ona, 
na svetlobi!

399
00:59:29,557 --> 00:59:32,633
Na moj znak.
-Pripravljen sem. 

400
00:59:37,403 --> 00:59:40,560
�ifra A, ponavljam �ifra A.
Vlom na prvi stopnji.

401
00:59:40,594 --> 00:59:45,363
Vsiljivec je v dvigalu 4. 
<i>�elim, da so vsi takoj pri tem dvigalu!</i>

402
00:59:49,170 --> 00:59:51,938
Anestezija pripravljena?
-Da, gospod. -Dobro. 

403
01:00:11,551 --> 01:00:16,359
<i>Zavarovanje 
v polni pripravljenosti!</i>

404
01:00:17,396 --> 01:00:19,627
<i>Vsiljivec se 
vzpenja z dvigalom!</i>

405
01:00:19,660 --> 01:00:23,396
Dajmo, gremo. 

406
01:00:58,276 --> 01:01:01,622
<i>To je tvoj znak.</i>
-Razumem. 

407
01:01:29,156 --> 01:01:33,464
Vzemite plin srebrnega nitrata,
bombe na vseh nadstropjih. 

408
01:01:33,497 --> 01:01:37,306
To je subjekt 1, 
moramo se evakuirati. 

409
01:01:37,339 --> 01:01:41,169
Dobro. 
Za�ni z infuzijo propofola, 

410
01:01:41,203 --> 01:01:45,000
ne odpovedujemo operacije, 
samo prestavljamo jo. 

411
01:01:45,461 --> 01:01:48,595
Najdi jo 
in uni�ijo jo.

412
01:01:48,628 --> 01:01:51,730
Z veseljem.

413
01:02:03,767 --> 01:02:08,766
Tu sem, vidim premikanje kombija
na ravni 3. <i>Ni sledu o deklici.</i>

414
01:02:08,799 --> 01:02:12,765
Na poti je. 
Samo ne izgubi je. 

415
01:02:16,611 --> 01:02:19,686
V storitvenem dvigalu so.

416
01:02:19,725 --> 01:02:22,687
Primite jo.
-Prihajam dol. 

417
01:02:43,761 --> 01:02:47,375
Selene?

418
01:02:47,414 --> 01:02:50,759
Kombi je na parkiri��u,
na ravni 2. 

419
01:02:53,452 --> 01:02:56,528
<i>Selene moram vedeti, 
kje si!</i>

420
01:03:22,485 --> 01:03:25,293
Selene, kje si?

421
01:03:52,751 --> 01:03:58,250
Vidim jo, raven 2,
tvojo h�er dajejo v kombi. 

422
01:03:58,284 --> 01:04:01,519
Selene, kje si?

423
01:04:02,364 --> 01:04:04,788
Jebi ga. 

424
01:04:21,593 --> 01:04:23,824
Michael ... 

425
01:04:34,591 --> 01:04:38,629
<i>Dr�ijo jo v kombiju 
in so na poti. Kje si?</i>

426
01:04:38,663 --> 01:04:40,744
<i>Odpeljali jo bodo!</i>

427
01:04:42,667 --> 01:04:44,898
Sem na poti. 

428
01:06:09,000 --> 01:06:11,577
Dajmo, hitreje. 

429
01:06:28,498 --> 01:06:32,075
Ne ustavi. 
Zadeni ga!

430
01:06:56,109 --> 01:06:59,571
Selene, gredo na raven 1. 

431
01:07:00,455 --> 01:07:03,878
<i>Selene, be�ijo na raven 1.
Me sli�i�?</i>

432
01:07:03,917 --> 01:07:06,954
Prihajam. 

433
01:07:27,566 --> 01:07:32,335
Je vse v redu?
-Stabilno je, vitalni ... 

434
01:09:09,822 --> 01:09:11,937
Jebi se!

435
01:09:52,238 --> 01:09:55,238
Pojdi stran od nje!

436
01:11:48,299 --> 01:11:52,030
Slab�e je, 
�e se upira�.

437
01:11:53,453 --> 01:11:55,683
Verjemi mi. 

438
01:12:50,214 --> 01:12:55,982
Hitro se celim.
-Ra�unam na to. 

439
01:13:50,474 --> 01:13:54,051
Vrnila si se pome. 

440
01:13:56,742 --> 01:13:59,627
Rekla sem, 
da se bom. 

441
01:13:59,661 --> 01:14:02,280
Selene. 

442
01:14:04,203 --> 01:14:07,319
Morate iti. 

443
01:14:08,279 --> 01:14:10,938
Na voljo je varna 
hi�a v bli�ini. -Pojdi.

444
01:14:10,971 --> 01:14:14,893
Poslal jih bom drugam.
Kupil vam bo nekaj �asa.

445
01:14:14,927 --> 01:14:18,932
Hvala, pridi. Gremo. 

446
01:14:20,585 --> 01:14:24,431
Se vra�amo nazaj noter?
-Vra�amo se. 

447
01:14:41,121 --> 01:14:44,891
Kaj je? Michael?

448
01:14:46,312 --> 01:14:48,813
Streha. 

449
01:14:53,658 --> 01:14:58,509
<i>Svet se je spremenil, 
na� sovra�nik je ostal isti.</i>

450
01:14:58,543 --> 01:15:05,003
<i>Volkodlaki se bodo obnovili.
Lovili bodo o�eta, tako kot so lovili njo.</i>

451
01:15:05,540 --> 01:15:09,963
<i>Dokler se oni krepijo, 
se tudi mi.</i>

452
01:15:10,771 --> 01:15:14,693
<i>Vampirski klan ne bo 
samo pre�ivel tega sveta ...</i>

453
01:15:14,727 --> 01:15:19,000
<i>Ponovno ga bomo osvojili.</i>

454
01:15:19,727 --> 01:15:25,000
Prevod: Marinko

